/// WelfareGettingType
/// OnePeople 一人一次
/// OneDay 一天一次
enum WelfareGettingTypeEnum { Error, OnePeople, OneDay }

/// WelfareReferenceTypeEnumMap
/// WelfareGettingType 的 Map 映射
const WelfareReferenceTypeEnumMap = {
  WelfareGettingTypeEnum.Error: 0,
  WelfareGettingTypeEnum.OnePeople: 1,
  WelfareGettingTypeEnum.OneDay: 2
};
