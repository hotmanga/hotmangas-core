enum RecommendType {
  /// H5_首页_Banner 2100101
  @deprecated
  WebH5HomeBanners,

  /// H5_首页_漫画推荐 2200202
  WebH5HomeEditors,

  /// H5_首页_动画推荐推荐 2200203
  WebH5HomeEditorsCartoons,

  /// H5_搜索_漫画推荐 2200301
  @deprecated
  WebH5SearchEditors,

  /// H5_发现_免费漫画_Banner 2101101
  @deprecated
  WebH5DiscoverComicFreeBanners,

  /// H5_发现_免费漫画_编辑漫画推荐 2201202
  WebH5DiscoverComicFreeEditors,

  /// H5_发现_付费漫画_Banner 2101301
  @deprecated
  WebH5DiscoverComicChargeBanners,

  /// H5_发现_付费漫画_编辑漫画推荐 2201402
  WebH5DiscoverComicChargeEditors,

  /// H5_发现_动画_Banner 2101501
  @deprecated
  WebH5DiscoverCartoonBanners,

  /// H5_发现_动画_编辑动画推荐 2201602
  WebH5DiscoverCartoonEditors,

  /// H5_全部_免费漫画 2200001
  WebH5FreeComics,

  /// H5_全部_付费漫画 2200002
  WebH5ChargeComics,

  /// H5_全部_动画 2200003
  WebH5ChargeCartoons,

  /// Copy_H5_首页_漫画推荐 3200102
  CopyWebH5HomeEditors,
}

const RecommendTypeEnumMap = {
  // RecommendType.WebH5HomeBanners: 2100101,
  RecommendType.WebH5HomeEditors: 2200202,
  RecommendType.WebH5HomeEditorsCartoons: 2200203,
  // RecommendType.WebH5SearchEditors: 2200301,
  // RecommendType.WebH5DiscoverComicFreeBanners: 2101101,
  // RecommendType.WebH5DiscoverComicChargeBanners: 2101301,
  // RecommendType.WebH5DiscoverCartoonBanners: 2101501,
  RecommendType.WebH5DiscoverComicFreeEditors: 2201202,
  RecommendType.WebH5DiscoverComicChargeEditors: 2201402,
  RecommendType.WebH5DiscoverCartoonEditors: 2201602,
  RecommendType.WebH5FreeComics: 2200001,
  RecommendType.WebH5ChargeComics: 2200002,
  RecommendType.WebH5ChargeCartoons: 2200003,
  RecommendType.CopyWebH5HomeEditors: 3200102,
};
