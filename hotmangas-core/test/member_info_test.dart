// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/member/member_info_entity.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试获取用户info信息', () async {
    final userName = 'kbceshi004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberInfoEntity? entity;
    await member.info.getInfo(
        resolve: (data) {
          entity = data;
        },
        reject: (error) {});

    expect(entity, isA<MemberInfoEntity>());
    expect(entity?.code, equals(200));
    expect(entity?.results?.userName, equals(userName));
    expect(entity?.results?.dayDownloads, greaterThanOrEqualTo(0));
  });
}
