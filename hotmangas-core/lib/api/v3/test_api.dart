import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;

class TestApi extends HttpCore {
  Future<void> getPathNotExists({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/path_not_exists',
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }

  Future<void> postPathNotExists({Function? resolve, Function? reject}) async {
    await postCore(
        path: '/api/v3/path_not_exists',
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }

  /// 需要修改 [hot/option.dart] 里的代码
  Future<void> getConnectionTimeout(
      {Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/connection_timeout',
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }

  /// 需要修改 [hot/option.dart] 里的代码
  Future<void> getReceivedTimeout({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/received_timeout',
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }
}

final testApi = TestApi();
