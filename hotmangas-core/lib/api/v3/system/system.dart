import 'package:hotmangasCore/api/v3/system/appversion.dart'
    show SystemAppVersion;
import 'package:hotmangasCore/api/v3/system/config.dart' show SystemConfig;
import 'package:hotmangasCore/api/v3/system/network.dart' show SystemNetwork;

class System extends Object {
  final appversion = SystemAppVersion();

  final config = SystemConfig();

  final network = SystemNetwork();
}

final system = System();
