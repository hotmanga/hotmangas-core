import './rank_item.dart' show RankListItemEntity;
import '../core/common/common_list_results.dart' show CommonListResults;
import '../core/enmus/content_type.dart' show ContentTypeEnumMap;
import '../core/enmus/decode.dart' show enumDecodeNullable;
import '../core/results_entity.dart' show ResultsEntity;

class RankListEntity extends ResultsEntity {
  Results? results;

  RankListEntity(
    code,
    message,
    this.results,
  ) : super(code, message);

  factory RankListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$RankListEntityFromJson(srcJson);
}

class Results extends CommonListResults {
  List<RankListItemEntity?>? list;

  @override
  bool? get hasWrapperItem {
    return true;
  }

  Results(this.list, total, limit, offset, type)
      : super(total, limit, offset, type);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

RankListEntity _$RankListEntityFromJson(Map<String, dynamic> json) {
  return RankListEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      (json['list'] as List)
          .map((e) => e == null
              ? null
              : RankListItemEntity.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['total'],
      json['limit'],
      json['offset'],
      enumDecodeNullable(ContentTypeEnumMap, json['type']));
}
