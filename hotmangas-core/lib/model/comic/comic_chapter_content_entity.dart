import 'package:hotmangasCore/model/comic/comic_chapter_content.dart'
    show ChapterContent;
import 'package:hotmangasCore/model/core/common/inner_item.dart' show InnerItem;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class ComicChapterContentEntity extends ResultsEntity {
  Results? results;

  ComicChapterContentEntity(int code, String message, this.results)
      : super(code, message);

  factory ComicChapterContentEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$ComicChapterContentEntityFromJson(srcJson);
}

ComicChapterContentEntity _$ComicChapterContentEntityFromJson(
    Map<String, dynamic> json) {
  return ComicChapterContentEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends Object {
  bool? isLock;

  bool? isLogin;

  bool? isMobileBind;

  bool? isVip;

  InnerItem? comic;

  Chapter? chapter;

  Results(this.isLock, this.isLogin, this.isMobileBind, this.isVip, this.comic,
      this.chapter);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['is_lock'],
      json['is_login'],
      json['is_mobile_bind'],
      json['is_vip'],
      InnerItem.fromJson(json['comic']),
      Chapter.fromJson(json['chapter']));
}

class Chapter extends Object {
  int? index;

  String? uuid;

  int? count;

  int? size;

  String? name;

  String? comicId;

  String? comicPathWord;

  String? groupId;

  String? groupPathWord;

  int? type;

  int? imgType;

  String? datetimeCreated;

  String? prev;

  String? next;

  bool? isLong;

  dynamic ordered;

  double get orderedDouble {
    if (ordered == null) {
      return 0.0;
    }
    return ordered.toDouble();
  }

  List<ChapterContent?>? contents;

  Chapter(
      this.index,
      this.uuid,
      this.count,
      this.size,
      this.name,
      this.comicId,
      this.comicPathWord,
      this.groupId,
      this.groupPathWord,
      this.type,
      this.imgType,
      this.datetimeCreated,
      this.prev,
      this.next,
      this.isLong,
      this.ordered,
      this.contents);

  factory Chapter.fromJson(Map<String, dynamic> srcJson) =>
      _$ChapterFromJson(srcJson);
}

Chapter _$ChapterFromJson(Map<String, dynamic> json) {
  return Chapter(
    json['index'],
    json['uuid'],
    json['count'],
    json['size'],
    json['name'],
    json['comic_id'],
    json['comic_path_word'],
    json['group_id'],
    json['group_path_word'],
    json['type'],
    json['img_type'],
    json['datetime_created'],
    json['prev'],
    json['next'],
    json['is_long'] ?? false,
    json['ordered'],
    (json['contents'] as List).map((e) => ChapterContent.fromJson(e)).toList(),
  );
}
