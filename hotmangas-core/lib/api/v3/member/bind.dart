import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show MemberInfoEntity;
import 'package:meta/meta.dart' show required;

class Bind extends HttpCore {
  Future<void> bindInviteCode(
      {@required String? inviteCode,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/bind/inviteCode',
        formData: {'invite_code': inviteCode},
        queryParameters: null,
        resolve: (data) {
          resolve?.call(MemberInfoEntity.fromJson(data));
        },
        reject: reject);
  }
}
