/// 返回类的父类, 只从返回 json 中获取 code 和 message.
class ResultsEntity extends Object {
  int? code;

  String? message;

  bool get isSuccess {
    return code == 200;
  }

  String? get failedMessge {
    return message!;
  }

  @override
  String toString() {
    return 'Results: ${code!}, ${message!}';
  }

  ResultsEntity(this.code, this.message);

  factory ResultsEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsEntityFromJson(srcJson);
}

ResultsEntity _$ResultsEntityFromJson(Map<String, dynamic> json) {
  return ResultsEntity(
    json['code'],
    json['message'],
  );
}
