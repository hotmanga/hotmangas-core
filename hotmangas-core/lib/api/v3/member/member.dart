import './account.dart' show Account;
import './bind.dart' show Bind;
import './browser.dart' show Browser;
import './collect.dart' show Collect;
import './comments.dart' show Comments;
import './feed_back.dart' show FeedBack;
import './info.dart' show Info;
import './login.dart' show Login;
import './order.dart' show Order;
import './report.dart' show Report;
import './reward.dart' show Reward;
import './roasts.dart' show Roasts;
import './safety.dart' show Safety;
import './update.dart' show Update;
import './upload.dart' show Upload;
import './welfare.dart' show Welfare;

class Member {
  final login = Login();
  final feedBack = FeedBack();
  final order = Order();
  final collect = Collect();
  final browser = Browser();
  final welfare = Welfare();
  final safety = Safety();
  final info = Info();
  final update = Update();
  final upload = Upload();
  final bind = Bind();
  final account = Account();
  final reward = Reward();
  final comments = Comments();
  final report = Report();
  final roasts = Roasts();
}

final member = Member();
