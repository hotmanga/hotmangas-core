import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试Filter comic type=1', () async {
    FilterListEntity? response;
    await filterIndex.getFreeComicFilterIndex(resolve: (entity) {
      response = entity;
    }, reject: (error) {
      print(error);
    });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.theme, isNotNull);
    expect(response?.results?.female, isNull);
    expect(response?.results?.male, isNull);
    expect(response?.results?.ordering, isNotNull);
  });

  test('测试Filter comic type=2', () async {
    FilterListEntity? response;
    await filterIndex.getChargeComicFilterIndex(resolve: (entity) {
      response = entity;
    }, reject: (error) {
      print(error);
    });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.theme, isNull);
    expect(response?.results?.female, isNotNull);
    expect(response?.results?.male, isNotNull);
    expect(response?.results?.ordering, isNotNull);
  });

  test('测试Filter cartoon type=1', () async {
    FilterListEntity? response;
    await filterIndex.getChargeCartoonFilterIndex(resolve: (entity) {
      response = entity;
    }, reject: (error) {
      print(error);
    });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.theme, isNull);
    expect(response?.results?.company, isNotNull);
    expect(response?.results?.year, isNotNull);
    expect(response?.results?.cartoonType, isNotNull);
    expect(response?.results?.female, isNotNull);
    expect(response?.results?.male, isNotNull);
    expect(response?.results?.ordering, isNotNull);
  });

  test('测试Filter cartoon type=3', () async {
    FilterListEntity? response;
    await filterIndex.getHomeIndexCartoon(resolve: (entity) {
      response = entity;
    }, reject: (error) {
      print(error);
    });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.theme, isNull);
    expect(response?.results?.company, isNull);
    expect(response?.results?.cartoonType, isNull);
    expect(response?.results?.female, isNotNull);
    expect(response?.results?.male, isNotNull);
    expect(response?.results?.ordering, isNotNull);
    expect(response?.results?.year, isNotNull);
  });

  test('测试Filter book 无 type 参数', () async {
    FilterListEntity? response;
    await filterIndex.getBookFilterIndex(resolve: (entity) {
      response = entity;
    }, reject: (error) {
      print(error);
    });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.theme, isNotNull);
    expect(response?.results?.theme?.length, equals(20));
    expect(response?.results?.top, isNotNull);
    expect(response?.results?.top?.length, equals(5));
    expect(response?.results?.ordering, isNotNull);
    expect(response?.results?.ordering?.length, equals(2));
  });
}
