import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/common/inner_item.dart' show InnerItem;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class FilterBookListEntity extends ResultsEntity {
  Results? results;

  FilterBookListEntity(code, message, this.results) : super(code, message);

  factory FilterBookListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$FilterBookListEntity(srcJson);
}

FilterBookListEntity _$FilterBookListEntity(Map<String, dynamic> json) {
  return FilterBookListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<FilterBookListItem?>? list;

  /// 列表是否包含信息.
  bool? get hasList {
    return list?.isNotEmpty;
  }

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) => _$Results(srcJson);
}

Results _$Results(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List).map((e) => FilterBookListItem.fromJson(e)).toList(),
  );
}

class FilterBookListItem extends Object {
  InnerItem? book;

  FilterBookListItem(this.book);

  factory FilterBookListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$FilterBookListItem(srcJson);
}

FilterBookListItem _$FilterBookListItem(Map<String, dynamic> json) {
  return FilterBookListItem(InnerItem.fromJson(json));
}
