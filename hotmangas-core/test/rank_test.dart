// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show rank;
import 'package:hotmangasCore/model/model.dart'
    show
        ContentRankDateType,
        ContentRankDateTypeEnumMap,
        ContentRankType,
        ContentRankTypeEnumMap,
        ContentType,
        RankListEntity;
import 'package:test/test.dart';

void main() {
  test('测试获取排行榜 免费漫画 日排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.FreeComic],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Day],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Day,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Comic,
        reason: '4');
  });

  test('测试获取排行榜 免费漫画 周排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.FreeComic],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Week],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Week,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Comic,
        reason: '4');
  });

  test('测试获取排行榜 免费漫画 月排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.FreeComic],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Month],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Month,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Comic,
        reason: '4');
  });
  test('测试获取排行榜 免费漫画 总排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.FreeComic],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Total],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Total,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Comic,
        reason: '4');
  });

  test('测试获取排行榜 付费漫画 日排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.ChargeComic],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Day],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Day,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Comic,
        reason: '4');
  });

  test('测试获取排行榜 付费漫画 周排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.ChargeComic],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Week],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Week,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Comic,
        reason: '4');
  });

  test('测试获取排行榜 付费漫画 月排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.ChargeComic],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Month],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Month,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Comic,
        reason: '4');
  });

  test('测试获取排行榜 付费漫画 总排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.ChargeComic],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Total],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Total,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Comic,
        reason: '4');
  });

  test('测试获取排行榜 付费动画 日排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.ChargeCartoon],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Day],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Day,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Cartoon,
        reason: '4');
  });

  test('测试获取排行榜 付费动画 周排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.ChargeCartoon],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Week],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Week,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Cartoon,
        reason: '4');
  });

  test('测试获取排行榜 付费动画 月排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.ChargeCartoon],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Month],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Month,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Cartoon,
        reason: '4');
  });

  test('测试获取排行榜 付费动画 总排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.ChargeCartoon],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Total],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.type, isNull);
    expect(response?.results?.limit, equals(1), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.list?.first?.dateType, ContentRankDateType.Total,
        reason: '3');
    expect(response?.results?.list?.first?.currentType, ContentType.Cartoon,
        reason: '4');
  });

  test('测试获取排行榜 免费动画 错误', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.FreeCartoon],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Day],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(210));
  });

  test('测试获取排行榜 错误的 date_type', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.FreeCartoon],
          'date_type': ContentRankDateTypeEnumMap[ContentRankDateType.Error],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(210));
  });
}
