import 'package:hotmangasCore/model/model.dart'
    show ComicQueryBrowseEntity, ResultsEntity;

class MemberCollectEntity extends ResultsEntity {
  MemberCollectedItem? results;

  MemberCollectEntity(code, message, this.results) : super(code, message);

  factory MemberCollectEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberComicCollectEntityFromJson(srcJson);
}

MemberCollectEntity _$MemberComicCollectEntityFromJson(
    Map<String, dynamic> json) {
  return MemberCollectEntity(
    json['code'],
    json['message'],
    MemberCollectedItem.fromJson(json['results']),
  );
}

class MemberCollectedItem extends Object {
  int? collect;

  bool? isLock;

  bool? isLogin;

  bool? isMobileBind;

  bool? isVip;

  bool? get hasCollect {
    return collect != -1;
  }

  ComicQueryBrowseEntity? browse;

  MemberCollectedItem(this.collect, this.isLock, this.isLogin,
      this.isMobileBind, this.isVip, this.browse);

  factory MemberCollectedItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberComicCollectedItemFromJson(srcJson);
}

MemberCollectedItem _$MemberComicCollectedItemFromJson(
    Map<String, dynamic> json) {
  return MemberCollectedItem(
    json['collect'] ?? -1,
    json['is_lock'],
    json['is_login'],
    json['is_mobile_bind'],
    json['is_vip'],
    json['browse'] != null
        ? ComicQueryBrowseEntity.fromJson(json['browse'])
        : null,
  );
}
