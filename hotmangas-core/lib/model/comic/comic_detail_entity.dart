import 'package:hotmangasCore/api/utils/datetime_format.dart'
    show datetimeFormat;
import 'package:hotmangasCore/model/model.dart'
    show
        FreeTypeItem,
        TagOrThemeItem,
        StatusItem,
        RestrictItem,
        RegionItem,
        Parodies,
        LastChapter,
        AuthorOrCompany,
        ClubItem,
        GroupWrapper;

import '../core/results_entity.dart' show ResultsEntity;

class ComicDetailEntity extends ResultsEntity {
  Results? results;

  ComicDetailEntity(code, message, this.results) : super(code, message);

  factory ComicDetailEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$ComicDetailEntityFromJson(srcJson);
}

ComicDetailEntity _$ComicDetailEntityFromJson(Map<String, dynamic> json) {
  return ComicDetailEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends Object {
  bool? isLock;

  bool? isLogin;

  bool? isMobileBind;

  bool? isVip;

  ///是否因为版权问题下架
  bool? isBanned;

  ComicDetailItem? comic;

  int? popular;

  GroupWrapper? groups;

  Results(this.isLock, this.isLogin, this.isMobileBind, this.isVip,
      this.isBanned, this.comic, this.popular, this.groups);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['is_lock'],
      json['is_login'],
      json['is_mobile_bind'],
      json['is_vip'],
      json['is_banned'] ?? false,
      json['comic'] == null
          ? null
          : ComicDetailItem.fromJson(json['comic'] as Map<String, dynamic>),
      json['popular'],
      GroupWrapper(json['groups']));
}

class ComicDetailItem extends Object {
  String? uuid;

  String? name;

  String? alias;

  String? pathWord;

  FreeTypeItem? freeType;

  List<TagOrThemeItem?>? females;

  List<TagOrThemeItem?>? males;

  List<ClubItem?>? clubs;

  RegionItem? region;

  RestrictItem? restrict;

  StatusItem? status;

  List<AuthorOrCompany?>? _author;

  List<TagOrThemeItem?>? theme;

  List<Parodies?>? parodies;

  String? brief;

  DateTime? _datetimeUpdated;

  String? cover;

  LastChapter? lastChapter;

  int? popular;

  bool? closeComment;

  ///因版权问题下架
  bool? isBanned;

  List<AuthorOrCompany?>? get author {
    return _author;
  }

  String? get displayParodiesName {
    if (parodies!.isEmpty) {
      return '--';
    }
    return parodies!.join(',');
  }

  String? get datetimeUpdated {
    return datetimeFormat(_datetimeUpdated);
  }

  ComicDetailItem(
    this.uuid,
    this.name,
    this.alias,
    this.pathWord,
    this.freeType,
    this.females,
    this.males,
    this.clubs,
    this.region,
    this.restrict,
    this.status,
    this._author,
    this.theme,
    this.parodies,
    this.brief,
    this._datetimeUpdated,
    this.cover,
    this.lastChapter,
    this.popular,
    this.closeComment,
    this.isBanned,
  );

  factory ComicDetailItem.fromJson(Map<String, dynamic> srcJson) =>
      _$ComicDetailItemFromJson(srcJson);
}

ComicDetailItem _$ComicDetailItemFromJson(Map<String, dynamic> json) {
  return ComicDetailItem(
    json['uuid'],
    json['name'],
    json['alias'],
    json['path_word'],
    FreeTypeItem.fromJson(json['free_type']),
    (json['females'] as List)
        .map((e) => e == null
            ? null
            : TagOrThemeItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['males'] as List)
        .map((e) => e == null
            ? null
            : TagOrThemeItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['clubs'] as List)
        .map((e) =>
            e == null ? null : ClubItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    RegionItem.fromJson(json['region']),
    RestrictItem.fromJson(json['restrict']),
    StatusItem.fromJson(json['status']),
    (json['author'] as List)
        .map((e) => e == null
            ? null
            : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['theme'] as List)
        .map((e) => e == null
            ? null
            : TagOrThemeItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['parodies'] as List)
        .map((e) =>
            e == null ? null : Parodies.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['brief'],
    json['datetime_updated'] == null
        ? null
        : DateTime.parse(json['datetime_updated']),
    json['cover'],
    json['last_chapter'] == null
        ? null
        : LastChapter.fromJson(json['last_chapter']),
    json['popular'],
    json['close_comment'] ?? false,
    json['is_banned'] ?? false,
  );
}
