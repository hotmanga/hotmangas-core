import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;
import 'package:hotmangasCore/model/model.dart'
    show BookContentType, TxtEncodingType;

class BookVolumeEntity extends ResultsEntity {
  Results? results;

  BookVolumeEntity(code, message, this.results) : super(code, message);

  factory BookVolumeEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$BookVolumeEntityFromJson(srcJson);
}

BookVolumeEntity _$BookVolumeEntityFromJson(Map<String, dynamic> json) {
  return BookVolumeEntity(json['code'], json['message'],
      json['results'] == null ? null : Results.fromJson(json['results']));
}

class Results extends Object {
  /// 未登录是 true,  不允许浏览当前内容
  /// 已登录是 false, 可以浏览当前内容
  bool? isLock;

  /// 是否登录
  bool? isLogin;

  /// 是否绑定手机
  bool? isMobileBind;

  /// 是否是 VIP
  bool? isVip;

  BookInnerItem? book;

  VolumeInnerItem? volume;

  Results(this.isLock, this.isLogin, this.isMobileBind, this.isVip, this.book,
      this.volume);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['is_lock'],
    json['is_login'],
    json['is_mobile_bind'],
    json['is_vip'],
    json['book'] == null ? null : BookInnerItem.fromJson(json['book']),
    json['volume'] == null ? null : VolumeInnerItem.fromJson(json['volume']),
  );
}

class BookInnerItem extends Object {
  String? name;

  String? uuid;

  String? pathWord;

  BookInnerItem(this.uuid, this.name, this.pathWord);

  factory BookInnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$BookInnerItemFromJson(srcJson);
}

BookInnerItem _$BookInnerItemFromJson(Map<String, dynamic> json) {
  return BookInnerItem(
    json['uuid'],
    json['name'],
    json['path_word'],
  );
}

class VolumeInnerItem extends Object {
  int? index;

  /// 轻小说章的 ID
  String? id;

  int? count;

  /// 轻小说章的排序 sort
  int? sort;

  /// 轻小说章的名称
  String? name;

  /// 轻小说的 txt 地址.
  String? textAddress;

  /// 轻小说该章对应的轻小说的 ID
  String? bookId;

  /// 轻小说该章对应的轻小说的 pathword
  String? bookPathWord;

  /// 轻小说对应章节的目录列表.
  List<VolumeContent?>? contents;

  /// 是否包含目录
  bool? get hasContents {
    return contents?.isNotEmpty;
  }

  /// 轻小说章的前一章, 没有则为 null
  String? prev;

  bool get hasPrev {
    return prev != null;
  }

  /// 轻小说章的下一章, 没有则为 null
  String? next;

  bool get hasNext {
    return next != null;
  }

  /// 文件的编码格式.
  String? _txtEncoding;

  TxtEncodingType? get txtEncoding {
    switch (_txtEncoding) {
      case 'GBK':
        return TxtEncodingType.GBK;
      case 'GB18030':
        return TxtEncodingType.GB18030;
      case 'UTF-8':
        return TxtEncodingType.UTF8;
      case 'UTF-16':
        return TxtEncodingType.UTF16;
      case 'UTF-32':
        return TxtEncodingType.UTF32;
      default:
        return TxtEncodingType.None;
    }
  }

  VolumeInnerItem(
      this.index,
      this.id,
      this.count,
      this.sort,
      this.name,
      this.textAddress,
      this.bookId,
      this.bookPathWord,
      this.contents,
      this.prev,
      this.next,
      this._txtEncoding);

  factory VolumeInnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$VolumeInnerItemFromJson(srcJson);
}

VolumeInnerItem _$VolumeInnerItemFromJson(Map<String, dynamic> json) {
  return VolumeInnerItem(
      json['index'],
      json['id'],
      json['count'],
      json['sort'],
      json['name'],
      json['txt_addr'],
      json['book_id'],
      json['book_path_word'],
      (json['contents'] as List)
          .map((e) => e == null
              ? null
              : VolumeContent.fromList(e as Map<String, dynamic>))
          .toList(),
      json['prev'],
      json['next'],
      json['txt_encoding'] ?? '');
}

/// 轻小说章里的话
class VolumeContent extends Object {
  /// 话的名称
  String? name;

  /// 话的起始行
  int? startLine;

  /// 话的结束行
  int? endLine;

  ///是否是图片  1为默认 2为图片
  int? _contentType;

  BookContentType? get contentType {
    switch (_contentType) {
      case 1:
        return BookContentType.Text;
      case 2:
        return BookContentType.Image;
      default:
        return BookContentType.None;
    }
  }

  ///图片地址
  String? content;

  VolumeContent(
      this.name, this.startLine, this.endLine, this._contentType, this.content);

  factory VolumeContent.fromList(Map<String, dynamic> srcJson) =>
      _$VolumeContentFromList(srcJson);
}

VolumeContent _$VolumeContentFromList(Map<String, dynamic> json) {
  return VolumeContent(
    json['name'] ?? '',
    json['start_lines'] ?? 0,
    json['end_lines'] ?? 0,
    json['content_type'] ?? 0,
    json['content'] ?? '',
  );
}
