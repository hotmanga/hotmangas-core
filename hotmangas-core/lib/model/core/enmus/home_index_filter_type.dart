enum HomeIndexFilterCartoonType { Uncensored, Cartoon3D }

enum HomeIndexFilterComicType { Color, Korea, Volume, Finish, Yaoi }

class HomeIndexFilter {
  String? type;
  String? name;
  String? displayName;

  HomeIndexFilter(this.type, this.name, this.displayName);
}

final HomeIndexFilterCartoonTypeEnumMap = {
  HomeIndexFilterCartoonType.Uncensored:
      HomeIndexFilter('cartoon', 'uncensored', '無碼動畫'),
  HomeIndexFilterCartoonType.Cartoon3D:
      HomeIndexFilter('cartoon', '3d', '3D動畫'),
};

final HomeIndexFilterComicTypeEnumMap = {
  HomeIndexFilterComicType.Color: HomeIndexFilter('comic', 'color', '全彩漫畫'),
  HomeIndexFilterComicType.Korea: HomeIndexFilter('comic', 'korea', '韓漫'),
  HomeIndexFilterComicType.Volume: HomeIndexFilter('comic', 'volume', '單行本'),
  HomeIndexFilterComicType.Finish: HomeIndexFilter('comic', 'finish', '已完結'),
  HomeIndexFilterComicType.Yaoi: HomeIndexFilter('comic', 'yaoi', '同志')
};
