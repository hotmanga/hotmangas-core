class ClubItem extends Object {
  String? pathWord;

  String? name;

  ClubItem(this.pathWord, this.name);

  factory ClubItem.fromJson(Map<String, dynamic> srcJson) =>
      _$ClubItemFromJson(srcJson);
}

ClubItem _$ClubItemFromJson(Map<String, dynamic> json) {
  return ClubItem(
    json['path_word'],
    json['name'],
  );
}
