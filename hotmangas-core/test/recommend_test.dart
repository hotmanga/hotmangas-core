// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/core/common/inner_item.dart';
import 'package:hotmangasCore/model/core/enmus/recommend_type.dart';
import 'package:hotmangasCore/model/recommend/recommend_comic_list_entity.dart';
import 'package:test/test.dart';

void main() async {
  test('推荐列表页，首页推荐漫画 接口基本信息', () async {
    RecommendComicListEntity? entity;
    await recommend.getList(
        position: RecommendType.WebH5HomeEditors,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
          print(data);
        },
        reject: (error) {
          print(error);
        });
    expect(entity!.code, equals(200));
    expect(entity!.results?.limit, equals(1));
    expect(entity!.results?.offset, equals(0));
    expect(entity!.results?.list, isList);
    expect(entity!.results?.list?.first, isA<RecommendComicListItem>());
    expect(entity!.results?.list?.first?.type, equals(1));
    expect(entity!.results?.list?.first?.comic, isA<InnerItem>());
    expect(entity!.results?.list?.first?.comic?.author, isNotEmpty);
  });

  test('推荐列表页，首页推荐动画 接口基本信息', () async {
    RecommendComicListEntity? entity;
    await recommend.getList(
        position: RecommendType.WebH5HomeEditorsCartoons,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });
    expect(entity?.code, equals(200), reason: '1');
    expect(entity?.results?.limit, equals(1), reason: '2');
    expect(entity?.results?.offset, equals(0), reason: '3');
    expect(entity?.results?.list, isList, reason: '4');
    expect(entity?.results?.list?.first, isA<RecommendComicListItem>(),
        reason: '5');
    expect(entity?.results?.list?.first?.type, equals(5), reason: '6');
    expect(entity?.results?.list?.first?.comic, isA<InnerItem>(), reason: '7');
    expect(entity?.results?.list?.first?.comic?.author, isNotEmpty,
        reason: '8');
  });

  test('推荐列表页，付费漫画推荐 接口基本信息', () async {
    RecommendComicListEntity? entity;
    await recommend.getList(
        position: RecommendType.WebH5ChargeComics,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });
    expect(entity?.code, equals(200));
    expect(entity?.results?.limit, equals(1));
    expect(entity?.results?.offset, equals(0));
    expect(entity?.results?.list, isList);
    expect(entity?.results?.list?.first, isA<RecommendComicListItem>());
    expect(entity?.results?.list?.first?.type, equals(1));
    expect(entity?.results?.list?.first?.comic, isA<InnerItem>());
    expect(entity?.results?.list?.first?.comic?.author, isNotEmpty);
  });

  test('推荐列表页，付费动画推荐 接口基本信息', () async {
    RecommendComicListEntity? entity;
    await recommend.getList(
        position: RecommendType.WebH5ChargeCartoons,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });
    expect(entity?.code, equals(200));
    expect(entity?.results?.limit, equals(1));
    expect(entity?.results?.offset, equals(0));
    expect(entity?.results?.list, isList);
    expect(entity?.results?.list?.first, isA<RecommendComicListItem>());
    expect(entity?.results?.list?.first?.type, equals(5));
    expect(entity?.results?.list?.first?.comic, isA<InnerItem>());
    expect(entity?.results?.list?.first?.comic?.author, isNotEmpty);
  });

  test('推荐列表页，发现页付费漫画编辑推荐 接口基本信息WebH5DiscoverComicChargeEditors', () async {
    RecommendComicListEntity? entity;
    await recommend.getList(
        position: RecommendType.WebH5DiscoverComicChargeEditors,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });
    expect(entity?.code, equals(200));
    expect(entity?.results?.limit, equals(1));
    expect(entity?.results?.offset, equals(0));
    expect(entity?.results?.list, isList);
    expect(entity?.results?.list?.first, isA<RecommendComicListItem>());
    expect(entity?.results?.list?.first?.type, equals(1));
    expect(entity?.results?.list?.first?.comic, isA<InnerItem>());
    expect(entity?.results?.list?.first?.comic?.author, isNotEmpty);
  });

  test('推荐列表页，发现页付费漫画编辑推荐 接口基本信息WebH5DiscoverCartoonEditors', () async {
    RecommendComicListEntity? entity;
    await recommend.getList(
        position: RecommendType.WebH5DiscoverCartoonEditors,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });
    expect(entity?.code, equals(200));
    expect(entity?.results?.limit, equals(1));
    expect(entity?.results?.offset, equals(0));
    expect(entity?.results?.list, isList);
    expect(entity?.results?.list?.first, isA<RecommendComicListItem>());
    expect(entity?.results?.list?.first?.type, equals(5));
    expect(entity?.results?.list?.first?.comic, isA<InnerItem>());
    expect(entity?.results?.list?.first?.comic?.author, isNotEmpty);
  });
}
