class TagOrThemeItem extends Object {
  String? pathWord;

  String? name;

  TagOrThemeItem(this.pathWord, this.name);

  factory TagOrThemeItem.fromJson(Map<String, dynamic> srcJson) =>
      _$TagItemFromJson(srcJson);
}

TagOrThemeItem _$TagItemFromJson(Map<String, dynamic> json) {
  return TagOrThemeItem(
    json['path_word'],
    json['name'],
  );
}
