import 'package:hotmangasCore/model/model.dart'
    show MemberWelfareItem, ResultsEntity;

class MemberWelfareEntity extends ResultsEntity {
  Results? results;

  MemberWelfareEntity(code, message, this.results) : super(code, message);

  factory MemberWelfareEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberWelfareEntityFromJson(srcJson);
}

MemberWelfareEntity _$MemberWelfareEntityFromJson(Map<String, dynamic> json) {
  return MemberWelfareEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends Object {
  MemberWelfareItem? welfare;

  /// 表示用户是否可以领取
  bool? show;

  /// 不能领取时的原因。
  String? message;

  Results(this.show, this.message, this.welfare);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['show'],
    json['message'],
    MemberWelfareItem.fromJson(json['welfare']),
  );
}
