// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';

void main() {
  test('测试福利列表', () async {
    MemberWelfareListEntity? entity;
    await member.welfare.getList(
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isList);
    expect(entity?.results, isNotEmpty);

    var welfareItem = entity?.results?.first;
    expect(welfareItem!.isSupportedCurrentPlatfrom(3), isTrue);
    expect(welfareItem.isSupportedCurrentPlatfrom(1), isFalse);
  });

  test('测试福利详情 -  未登录获取 - 失败', () async {
    await member.welfare.getDetails(
        welfareUuid: '30de86d2-bdb7-11e9-90be-024352452ce0',
        resolve: (data) {
          print(data);
        },
        reject: (error) {
          print(error);
        });
  });

  test('测试福利详情 - 登录，获取某个福利详情', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    await member.welfare.getDetails(
        welfareUuid: '30de86d2-bdb7-11e9-90be-024352452ce0',
        resolve: (data) {
          print(data);
        },
        reject: (error) {
          print(error);
        });
  });
}
