import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show MemberUpdateInfoEntity, MemberUpdatePostEntity;
import 'package:meta/meta.dart' show required;

class Update extends HttpCore {
  Future<void> getInfo({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/member/update/info',
        queryParameters: null,
        resolve: (data) {
          resolve?.call(MemberUpdateInfoEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> postInfo(
      {@required Map<String, dynamic>? fromData,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/update/info',
        formData: fromData,
        queryParameters: null,
        resolve: (data) {
          resolve?.call(MemberUpdatePostEntity.fromJson(data));
        },
        reject: reject);
  }
}

/// 1 头像，2 反馈
