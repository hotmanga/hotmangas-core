import 'package:hotmangasCore/model/model.dart' show CommonListItemPortrait;

class CommonListPortrait extends Object {
  List<CommonListItemPortrait?>? list;

  int? total;

  int? limit;

  int? offset;

  CommonListPortrait(
    this.list,
    this.total,
    this.limit,
    this.offset,
  );

  factory CommonListPortrait.fromJson(Map<String, dynamic> srcJson) =>
      _$CommonListComicFromJson(srcJson);
}

CommonListPortrait _$CommonListComicFromJson(Map<String, dynamic> json) {
  return CommonListPortrait(
    (json['list'] as List)
        .map((e) => e == null
            ? null
            : CommonListItemPortrait.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['total'],
    json['limit'],
    json['offset'],
  );
}
