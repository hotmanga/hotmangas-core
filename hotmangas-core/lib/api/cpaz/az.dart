import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/comic/comic_detail_entity.dart';
import 'package:hotmangasCore/model/core/common/common_list_entity.dart';
import 'package:hotmangasCore/model/core/enmus/rank_type.dart';
import 'package:hotmangasCore/model/core/enmus/recommend_type.dart';
import 'package:hotmangasCore/model/model.dart'
    show CopyHomeIndexEntity, MemberCollectComicListEntity;
import 'package:hotmangasCore/model/rank/rank_list_entity.dart';
import 'package:hotmangasCore/model/recommend/recommend_comic_list_entity.dart';

final Map<String, dynamic> _rankQueryParameters = {
  'type': RankTypeEnumMap[RankType.Error],
  'date_type': RankDateTypeEnumMap[RankDateType.Error],
  'limit': 21,
  'offset': 0,
};

final Map<String, dynamic> _recQueryParameters = {
  'pos': null,
  'limit': 20,
  'offset': 0,
};

final Map<String, dynamic> _queryParameters = {
  'limit': 21,
  'offset': 0,
};

class AnZhiApi extends HttpCore {
  Future<void> getHomeIndex({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/cpaz/h5/homeIndex',
        resolve: (data) {
          resolve?.call(CopyHomeIndexEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getRankList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_rankQueryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/cpaz/ranks',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(RankListEntity.fromJson(data));
        },
        reject: reject!);
  }

  Future<void> getRecsList(
      {RecommendType? position,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_recQueryParameters);
    _.addAll({'pos': RecommendTypeEnumMap[position]});
    await getCore(
        path: '/api/cpaz/recs',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(RecommendComicListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getComicQueryList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/cpaz/comics',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CommonListEntity.fromJson(data));
        },
        reject: reject);
  }

  void getComicDetail(
      {String? pathWord, Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/cpaz/comic2/$pathWord',
        resolve: (data) {
          resolve?.call(ComicDetailEntity.fromJson(data));
        },
        reject: reject);
  }

  void getCollectComicList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/cpaz/member/collect/comics',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberCollectComicListEntity.fromJson(data));
        },
        reject: reject);
  }
}

final azApi = AnZhiApi();
