class SystemNetworkEntity extends Object {
  int? code;

  String? message;

  Results? results;

  SystemNetworkEntity(
    this.code,
    this.message,
    this.results,
  );

  factory SystemNetworkEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$SystemNetworkEntityFromJson(srcJson);
}

class Results extends Object {
  String? static;

  String? image;

  List<String>? share;

  List<List<String>?>? api;

  // 每隔几次展示一个插屏广告
  int? adsScreenShow = 0;

  // 每日一共多少次下載免費
  int? dayDownloadsTotal = 0;

  // 看一次激勵廣告獲得多少下載次數
  int? adsRewardDownloads = 0;

  // 用戶每日下載次數
  int? dayDownloads = 0;

  // 用戶贊助次數
  int? vipDownloads = 0;

  // 用戶激勵視頻次數
  int? rewardDownloads = 0;

  /// 当天只能展示几次激励广告
  int? adsRewardDayShow = 0;

  ///首页广告每隔多少秒弹一次
  int? adsHomeSeconds = 0;

  ///漫画阅读页每隔几次请求一次信息流广告
  int? adsReadExpressShow = 0;

  ///热启动广告每隔多少秒请求一次
  int? adsRestartSeconds = 60;

  ///是否是国内ip 默认是空
  bool? inMainland;

  ///是否发送记录广告数据的接口  true 发送  false 不发送 默认为不发送
  bool? adsStatSwitch2 = false;

  /// 免广告 VIP 到期时间
  @Deprecated('废弃, 由 memberinfo 里同名值代替')
  String? adsVipEnd;

  Results(
    this.static,
    this.image,
    this.share,
    this.api,
    this.dayDownloadsTotal,
    this.adsRewardDownloads,
    this.dayDownloads,
    this.vipDownloads,
    this.rewardDownloads,
    this.adsScreenShow,
    this.adsRewardDayShow,
    this.adsVipEnd,
    this.adsHomeSeconds,
    this.adsReadExpressShow,
    this.adsRestartSeconds,
    this.inMainland,
    this.adsStatSwitch2,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

SystemNetworkEntity _$SystemNetworkEntityFromJson(Map<String, dynamic> json) {
  return SystemNetworkEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['static'],
    json['image'],
    (json['share'] as List).map((e) => e as String).toList(),
    (json['api'] as List)
        .map((e) => (e as List).map((e) => e as String).toList())
        .toList(),
    json['day_downloads_total'],
    json['ads_reward_downloads'],
    json['day_downloads'],
    json['vip_downloads'],
    json['reward_downloads'],
    json['ads_screen_show'],
    json['ads_reward_day_show'],
    json['ads_vip_end'] ?? '',
    json['ads_home_seconds'] ?? 0,
    json['ads_read_express_show'] ?? 0,
    json['ads_restart_seconds'] ?? 60,
    json['in_mainland'] ?? true,
    json['ads_stat_switch2'] ?? false,
  );
}
