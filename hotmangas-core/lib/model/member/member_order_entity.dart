import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberOrderEntity extends ResultsEntity {
  MemberOrderPayUrlItem? results;

  MemberOrderEntity(code, message, this.results) : super(code, message);

  factory MemberOrderEntity.fromJson(Map<String, dynamic> fromJson) =>
      _$MemberOrderEntityFromJson(fromJson);
}

MemberOrderEntity _$MemberOrderEntityFromJson(Map<String, dynamic> json) {
  return MemberOrderEntity(json['code'], json['message'],
      MemberOrderPayUrlItem.fromJson(json['results']));
}

class MemberOrderPayUrlItem extends Object {
  // String goodsName;

  // /// 数据库订单主键
  // String uuid;

  // /// 订单编号
  // String no;

  // int pricePay;

  // int channel;

  // int platform;

  // int way;

  // String username;

  // String pay;

  String? payUrl;

  // int redirectWay;

  MemberOrderPayUrlItem(this.payUrl);

  factory MemberOrderPayUrlItem.fromJson(Map<String, dynamic> fromJson) =>
      _$MemberOrderItemFromJson(fromJson);
}

MemberOrderPayUrlItem _$MemberOrderItemFromJson(Map<String, dynamic> json) {
  return MemberOrderPayUrlItem(
    json['pay_url'],
  );
}
