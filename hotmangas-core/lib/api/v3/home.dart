import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        HomeIndexEntity,
        HomeIndexFilterListEntity,
        OrderingType,
        OrderingTypeEnumMap,
        HomeIndexFilterCartoonType,
        HomeIndexFilterCartoonTypeEnumMap,
        HomeIndexFilterComicType,
        HomeIndexFilterComicTypeEnumMap;

final Map<String, dynamic> _queryCartoonParameters = {
  'top':
      HomeIndexFilterCartoonTypeEnumMap[HomeIndexFilterCartoonType.Uncensored]
          ?.name,
  'ordering': OrderingTypeEnumMap[OrderingType.DatetimeUpdatedDown],
  'limit': 20,
  'offset': 0,
  'year': '',
  'male': '',
  'female': ''
};

final Map<String, dynamic> _queryComicParameters = {
  'top': HomeIndexFilterComicTypeEnumMap[HomeIndexFilterComicType.Color]?.name,
  'ordering': OrderingTypeEnumMap[OrderingType.DatetimeUpdatedDown],
  'limit': 20,
  'offset': 0
};

class HomeIndex extends HttpCore {
  Future<void> getHomeIndex({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/h5/homeIndex',
        resolve: (data) {
          var homeIndex = HomeIndexEntity.fromJson(data);
          resolve?.call(homeIndex);
        },
        reject: reject);
  }

  Future<void> getHomeIndexCartoon(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryCartoonParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/h5/homeIndex/cartoons',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(HomeIndexFilterListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getHomeIndexComic(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryComicParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/h5/homeIndex/comics',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(HomeIndexFilterListEntity.fromJson(data));
        },
        reject: reject);
  }
}

final homeIndex = HomeIndex();
