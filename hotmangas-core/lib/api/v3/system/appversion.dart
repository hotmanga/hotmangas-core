import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show SystemAppVersionInfoEntity;

class SystemAppVersion extends HttpCore {
  Future<void> getList({Function? resolve, Function? reject}) async {
    //写死请求版本的接口域名
    await getCore(
        path: '/api/v3/system/appVersion/last',
        resolve: (data) {
          resolve?.call(SystemAppVersionInfoEntity.fromJson(data));
        },
        reject: reject);
  }
}
