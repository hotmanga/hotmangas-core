import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show MemberCommentCollectListEntity, ResultsEntity;

final Map<String, dynamic>? _queryParameters = {
  'comic_id': '', //漫画ID
  'comment': '', //评论内容
  'reply_id': '', //可以不带 带的话就是回复某一个评论
};
final Map<String, dynamic>? _getCommentListMap = {
  'comic_id': '', //漫画ID
  'reply_id': '', //可以不带 某一个评论
};
final Map<String, dynamic>? _delComment = {
  'comic_id': '', //漫画ID
  'comment_id': 0, //评论ID
};
final Map<String, dynamic>? _queryNovelParameters = {
  'book_id': '', //轻小说ID
  'comment': '', //评论内容
  'reply_id': '', //可以不带 带的话就是回复某一个评论
};
final Map<String, dynamic>? _getNovelCommentListMap = {
  'book_id': '', //轻小说ID
  'reply_id': '', //可以不带 某一个评论
};
final Map<String, dynamic>? _delNovelComment = {
  'book_id': '', //轻小说ID
  'comment_id': 0, //评论ID
};

class Comments extends HttpCore {
  /// 用户在漫画下发表评论
  Future<void> createComment(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters!);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v3/member/comment',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  ///获取用户的留言列表
  Future<void> getCommentCollectList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_getCommentListMap!);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/comments',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberCommentCollectListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> delComment(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_delComment!);
    _.addAll(queryParameters!);
    await deleteCore(
        path: '/api/v3/member/comment',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 用户在轻小说下发表评论
  Future<void> createNovelComment(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryNovelParameters!);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v3/member/bookcomment',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 用户轻小说的评论列表
  Future<void> getNovelCommentCollectList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_getNovelCommentListMap!);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/bookcomments',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberCommentCollectListEntity.fromJson(data));
        },
        reject: reject);
  }

  ///用户删除轻小说评论
  Future<void> delNovelComment(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_delNovelComment!);
    _.addAll(queryParameters!);
    await deleteCore(
        path: '/api/v3/member/bookcomment',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }
}
