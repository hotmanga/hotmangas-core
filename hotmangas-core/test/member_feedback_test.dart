// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/member/member.dart';
import 'package:hotmangasCore/model/model.dart';

void main() async {
  test('测试留言列表 接口基本信息', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';
    // '2dfc366e80ba5a71b4f311fcaec8ce7efbc136be'

    MemberFeedBackListEntity? entity;
    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await member.feedBack.getList(
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });
    expect(entity?.code, equals(200));
    expect(entity?.results, isNotNull);
    expect(entity?.results?.list, isList);
    expect(entity?.results?.limit, equals(1));
    expect(entity?.results?.offset, equals(0));
    expect(entity?.results?.list?.first, isA<MemberFeedBackItem>());
    expect(entity?.results?.list?.first.type?.value, FeedBackType.Advise);
    expect(entity?.results?.list?.first.isReplied, isFalse);
    expect(entity?.results?.list?.first.reply, isNotNull);
    expect(entity?.results?.list?.first.dateTimeReply, isNotNull);
  });

  test('测试留言功能，发送留言', () async {
    final userName = 'copy002';
    final passWord = '111111';

    MemberFeedBackEntity? entity;
    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    await member.feedBack.postFeedBack(
        message: '测试API发送留言，本留言由程序发出，无图，待remark信息',
        type: FeedBackType.SomethingWrong,
        remark: FeedBackRemark('deviceInfo', 'appVersion', 'utmSource'),
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });

    expect(entity?.code, equals(200));
    expect(entity?.isSuccess, isTrue);
    expect(entity?.failedMessge, isNotNull);
    expect(entity?.failedMessge, isNotEmpty);
  });
}
