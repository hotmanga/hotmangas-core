import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;

import '../core/results_entity.dart' show ResultsEntity;

class PortraitChapterEntity extends ResultsEntity {
  Results? results;

  PortraitChapterEntity(code, message, this.results) : super(code, message);

  factory PortraitChapterEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitChapterEntity(srcJson);
}

PortraitChapterEntity _$PortraitChapterEntity(Map<String, dynamic> json) {
  return PortraitChapterEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends CommonListResults {
  List<PortraitChapterItem?>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['total'],
      json['limit'],
      json['offset'],
      (json['list'] as List)
          .map((e) => PortraitChapterItem.fromJson(e))
          .toList());
}

class PortraitChapterItem extends Object {
  int? index;

  String? id;

  int? count;

  int? sort;

  String? name;

  String? postId;

  String? prev;

  String? next;

  int? size;

  PortraitChapterItem(this.index, this.id, this.count, this.sort, this.name,
      this.postId, this.prev, this.next, this.size);

  factory PortraitChapterItem.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitChapterItem(srcJson);
}

PortraitChapterItem _$PortraitChapterItem(Map<String, dynamic> json) {
  return PortraitChapterItem(
    json['index'],
    json['id'],
    json['count'],
    json['sort'],
    json['name'],
    json['post_id'],
    json['prev'],
    json['next'],
    json['size'] ?? 0,
  );
}
