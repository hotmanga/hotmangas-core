import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show CopyAdsEntity, CopyHomeIndexEntity, PortraitHomeIndexEntity;

class CopyApi extends HttpCore {
  Future<void> getHomeIndex({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/h5/homeIndex2',
        resolve: (data) {
          resolve?.call(CopyHomeIndexEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getAdsShow({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/system/ads',
        resolve: (data) {
          resolve?.call(CopyAdsEntity.fromJson(data));
        },
        reject: reject);
  }

  ///写真集首页
  Future<void> getPortraitHomeIndex(
      {Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/h5/postIndex',
        resolve: (data) {
          resolve?.call(PortraitHomeIndexEntity.fromJson(data));
        },
        reject: reject);
  }
}

final copyApi = CopyApi();
