enum UpdateType {
  Error,
  WeeklyCartoon,
  WeeklyComicFree,
  WeeklyComicCharge,
}

const UpdateTypeEnumMap = {
  UpdateType.Error: '',
  UpdateType.WeeklyCartoon: 'weekly-cartoon',
  UpdateType.WeeklyComicFree: 'weekly-comic-free',
  UpdateType.WeeklyComicCharge: 'weekly-comic-charge',
};
