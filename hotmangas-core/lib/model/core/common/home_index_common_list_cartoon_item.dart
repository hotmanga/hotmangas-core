import 'inner_item.dart' show InnerItem;

class CommonListCartoonItem extends Object {
  String? name;

  int? sort;

  InnerItem? cartoon;

  CommonListCartoonItem(
    this.name,
    this.sort,
    this.cartoon,
  );

  factory CommonListCartoonItem.fromJson(Map<String, dynamic> srcJson) =>
      _$CommonListCartoonItemFromJson(srcJson);
}

CommonListCartoonItem _$CommonListCartoonItemFromJson(
    Map<String, dynamic> json) {
  return CommonListCartoonItem(
    json['name'],
    json['sort'],
    json['cartoon'] == null
        ? null
        : InnerItem.fromJson(json['cartoon'] as Map<String, dynamic>),
  );
}
