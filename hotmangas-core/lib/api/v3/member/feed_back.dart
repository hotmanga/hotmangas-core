import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        FeedBackRemark,
        FeedBackType,
        FeedBackTypeEnumMap,
        MemberFeedBackEntity,
        MemberFeedBackListEntity;
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'limit': 21,
  'offset': 0,
};

class FeedBack extends HttpCore {
  Future<void> getList({
    Map<String, dynamic>? queryParameters,
    Function? resolve,
    Function? reject,
  }) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/feedbacks',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberFeedBackListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> postFeedBack({
    @required String? message,
    @required FeedBackType? type,
    String? imgUrl,
    @required FeedBackRemark? remark,
    Function? resolve,
    Function? reject,
  }) async {
    await postCore(
        path: '/api/v3/member/feedback',
        formData: {
          'message': message,
          'type': FeedBackTypeEnumMap[type],
          'image': imgUrl,
          'remark': remark.toString(),
        },
        resolve: (data) {
          resolve?.call(MemberFeedBackEntity.fromJson(data));
        },
        reject: reject);
  }
}
