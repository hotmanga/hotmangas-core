// Dart imports:
import 'dart:io';

// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' as api_v3;
import 'package:hotmangasCore/model/model.dart';

void main() {
  test('测试上传头像', () async {
    final userName = 'copy002';
    final passWord = '111111';

    await api_v3.member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var file = File('C:\\Users\\Administrator\\Desktop\\jps.jpg');

    await api_v3.member.upload.uploadAvator(
        file: file,
        resolve: (UploadEntity data) {
          print(data.results);
        },
        reject: (error) {
          print(error);
        });
  });

  test('测试留言图片', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await api_v3.member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var file = File('C:\\Users\\Administrator\\Desktop\\jps.jpg');

    await api_v3.member.upload.uploadFeedBackImage(
        file: file,
        resolve: (data) {
          print(data);
        },
        reject: (error) {
          print(error);
        });
  });
}
