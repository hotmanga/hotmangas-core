import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;
import 'package:hotmangasCore/model/member/member_reward_show_total.dart'
    show MemberRewardShowTotal;

class MemberRewardShowTotalEntity extends ResultsEntity {
  MemberRewardShowTotal? results;

  MemberRewardShowTotalEntity(code, message, this.results)
      : super(code, message);

  factory MemberRewardShowTotalEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberRewardEntityFromJson(srcJson);
}

MemberRewardShowTotalEntity _$MemberRewardEntityFromJson(
    Map<String, dynamic> json) {
  return MemberRewardShowTotalEntity(json['code'], json['message'],
      MemberRewardShowTotal.fromJson(json['results']));
}
