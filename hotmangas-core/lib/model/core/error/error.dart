// 200, 401, 403  210, 404,
// status_code 200, 401, 403  210
// code 200, 401, 403, 210

import 'package:hotmangasCore/model/core/enmus/decode.dart';

enum HotmangasErrorType {
  /// 内容不存在 404
  NotFind,

  /// 缺少TOKEN 401
  Unauthorized,

  /// TOKEN过期 403
  Forbidden,

  /// 自定义错误 210
  CustomerError,

  /// 下载次数不足 211
  DownloadLimited,

  /// 连接超时 ConnectionTimeout408
  RequestTimeout,

  /// 收取超时 ConnectionTimeout408
  ReceiveTimeout,

  /// ads 报错时是400暂定
  AdsError,

  /// 502错误
  badGateway,

  ///寫真幣不足
  partraitPay,

  ///内容不存在
  ObjectNotExist,

  ///内容为空
  noContent,

  /// 不是以上几种的都是default
  Default,
}

final HotmangasErrorTypeMap = {
  HotmangasErrorType.NotFind: 404,
  HotmangasErrorType.Unauthorized: 401,
  HotmangasErrorType.Forbidden: 403,
  HotmangasErrorType.CustomerError: 210,
  HotmangasErrorType.DownloadLimited: 211,
  HotmangasErrorType.RequestTimeout: 408,
  HotmangasErrorType.ReceiveTimeout: 499,
  HotmangasErrorType.AdsError: 400,
  HotmangasErrorType.partraitPay: 221,
  HotmangasErrorType.badGateway: 502,
  HotmangasErrorType.Default: 999,
  HotmangasErrorType.noContent: 231,
  HotmangasErrorType.ObjectNotExist: 232,
};

/// 封装统一的错误类型
class HotmangasError implements Exception {
  final int? _code;

  final String? _message;

  HotmangasErrorType? _errorType;

  HotmangasError(this._code, this._message) {
    _errorType = enumDecodeNullable<HotmangasErrorType>(
        HotmangasErrorTypeMap, _code,
        unknownValue: HotmangasErrorType.Default);
  }

  String? get message {
    return _message!;
  }

  HotmangasErrorType? get errorType {
    return _errorType!;
  }

  @override
  String toString() {
    return '$_code : $message';
  }
}
