class RegionItem extends Object {
  int? value;

  String? display;

  RegionItem(this.value, this.display);

  factory RegionItem.fromJson(Map<String, dynamic> srcJson) =>
      _$RegionItemFromJson(srcJson);
}

RegionItem _$RegionItemFromJson(Map<String, dynamic> json) {
  return RegionItem(
    json['value'],
    json['display'],
  );
}
