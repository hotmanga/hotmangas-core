/// 排序枚举类型
enum OrderingType {
  /// 修改时间降序
  DatetimeUpdatedDown,

  /// 修改时间升序
  DatetimeUpdatedUp,

  /// 热度降序
  PopularDown,

  /// 热度升序
  PopularUp,

  /// 加到书架时间升序
  DatetimeModifierUp,

  /// 加到书架时间降序
  DatetimeModifierDown,
}

/// 排序枚举类型对应的值
const OrderingTypeEnumMap = {
  /// 修改时间降序
  OrderingType.DatetimeUpdatedDown: '-datetime_updated',

  /// 修改时间升序
  OrderingType.DatetimeUpdatedUp: 'datetime_updated',

  /// 热度降序
  OrderingType.PopularDown: '-popular',

  /// 热度升序
  OrderingType.PopularUp: 'popular',

  /// 加到书架时间升序
  OrderingType.DatetimeModifierDown: '-datetime_modifier',

  /// 加到书架时间降序
  OrderingType.DatetimeModifierUp: 'datetime_modifier',
};