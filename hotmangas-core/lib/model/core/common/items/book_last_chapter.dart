class BookLastChapter extends Object {
  String? id;

  String? name;

  bool get isEmpty {
    return id!.isEmpty && name!.isEmpty;
  }

  BookLastChapter(
    this.id,
    this.name,
  );

  factory BookLastChapter.fromJson(Map<String, dynamic> srcJson) =>
      _$BookLastChapterFromJson(srcJson);
}

BookLastChapter _$BookLastChapterFromJson(Map<String, dynamic> json) {
  return BookLastChapter(
    json['id'],
    json['name'],
  );
}
