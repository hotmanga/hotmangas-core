import 'package:hotmangasCore/model/core/common/portrait_item.dart'
    show PortraitItemEntity;

import '../core/common/inner_item.dart' show InnerItem;
import '../core/enmus/content_rank_type.dart'
    show ContentRankDateType, ContentRankDateTypeEnumMapInt;
import '../core/enmus/content_type.dart' show ContentType;
import '../core/enmus/decode.dart' show enumDecodeNullable;

class RankListItemEntity extends Object {
  int? sort;

  int? sortLast;

  int? riseSort;

  int? riseNum;

  ContentRankDateType? dateType;

  int? popular;

  InnerItem? _comic;

  InnerItem? _cartoon;

  InnerItem? _book;

  PortraitItemEntity? post;

  InnerItem? get wrapperItem {
    if (_comic != null) {
      return _comic;
    } else if (_cartoon != null) {
      return _cartoon;
    } else if (_book != null) {
      return _book;
    } else {
      return null;
    }
  }

  ContentType? get currentType {
    if (_comic != null) {
      return ContentType.Comic;
    } else if (_cartoon != null) {
      return ContentType.Cartoon;
    } else {
      return ContentType.None;
    }
  }

  RankListItemEntity(
    this.sort,
    this.sortLast,
    this.riseSort,
    this.riseNum,
    this.dateType,
    this.popular,
    this._comic,
    this._cartoon,
    this._book,
    this.post,
  );

  factory RankListItemEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$RankListItemFromJson(srcJson);
}

RankListItemEntity _$RankListItemFromJson(Map<String, dynamic> json) {
  return RankListItemEntity(
    json['sort'],
    json['sort_last'],
    json['rise_sort'],
    json['rise_num'],
    enumDecodeNullable(ContentRankDateTypeEnumMapInt, json['date_type']),
    json['popular'],
    json['comic'] == null
        ? null
        : InnerItem.fromJson(json['comic'] as Map<String, dynamic>),
    json['cartoon'] == null
        ? null
        : InnerItem.fromJson(json['cartoon'] as Map<String, dynamic>),
    json['book'] == null
        ? null
        : InnerItem.fromJson(json['book'] as Map<String, dynamic>),
    json['post'] == null
        ? null
        : PortraitItemEntity.fromJson(json['post'] as Map<String, dynamic>),
  );
}
