import 'package:hotmangasCore/model/core/common/author_or_company.dart'
    show AuthorOrCompany;
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class SearchBookListEntity extends ResultsEntity {
  Results? results;

  SearchBookListEntity(code, message, this.results) : super(code, message);

  factory SearchBookListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$SearchBookListEntityFromJson(srcJson);
}

SearchBookListEntity _$SearchBookListEntityFromJson(Map<String, dynamic> json) {
  return SearchBookListEntity(
      json['code'], json['message'], Results.fromJson(json['results']));
}

class Results extends CommonListResults {
  List<SearchBookListItem?>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['total'],
      json['limit'],
      json['offset'],
      (json['list'] as List)
          .map((e) => SearchBookListItem.fromJson(e as Map<String, dynamic>))
          .toList());
}

class SearchBookListItem extends Object {
  /// 轻小说名称
  String? name;

  /// 轻小说 Pathword
  String? pathWord;

  /// 轻小说封面
  String? cover;

  /// 轻小说作者
  List<AuthorOrCompany?>? _author;

  List<AuthorOrCompany?>? get author {
    return _author;
  }

  /// 轻小说热度
  int? popular;

  SearchBookListItem(
      this.name, this.pathWord, this.cover, this._author, this.popular);

  factory SearchBookListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$SearchBookListItemFromJson(srcJson);
}

SearchBookListItem _$SearchBookListItemFromJson(Map<String, dynamic> json) {
  return SearchBookListItem(
    json['name'],
    json['path_word'],
    json['cover'],
    json['author'] == null
        ? null
        : (json['author'] as List)
            .map((e) => e == null
                ? null
                : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
            .toList(),
    json['popular'],
  );
}
