import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;
import 'package:hotmangasCore/model/member/member_reward_create.dart';

class MemberRewardCreateEntity extends ResultsEntity {
  MemberRewardCreate? results;

  MemberRewardCreateEntity(code, message, this.results) : super(code, message);

  factory MemberRewardCreateEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberRewardEntityFromJson(srcJson);
}

MemberRewardCreateEntity _$MemberRewardEntityFromJson(
    Map<String, dynamic> json) {
  return MemberRewardCreateEntity(json['code'], json['message'],
      MemberRewardCreate.fromJson(json['results']));
}
