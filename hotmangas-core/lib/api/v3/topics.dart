import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        ContentType,
        PortraitPuyEntity,
        TopicListEntity,
        TopicsDetailEntity,
        TopicsDetailListEntity;
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'type': ContentType.None.index,
  'limit': 20,
  'offset': 0,
};

final Map<String, dynamic> _puyParameters = {
  'path_word': "",
};

class Topic extends HttpCore {
  Future<void> getList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/topics',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(TopicListEntity.fromJson(data));
          // resolve(data);
        },
        reject: reject);
  }

  Future<void> getDetail(
      {@required String? topicPathWord,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/topic/$topicPathWord',
        resolve: (data) {
          resolve?.call(TopicsDetailEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getDetailList(
      {@required String? topicPathWord,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/topic/$topicPathWord/contents',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(TopicsDetailListEntity.fromJson(data));
        },
        reject: reject);
  }

  ///购买专题
  Future<void> pushDetailListPuy(
      {@required String? topicPathWord,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/topic/buy',
        formData: {'path_word': topicPathWord},
        resolve: (data) {
          resolve?.call(PortraitPuyEntity.fromJson(data));
        },
        reject: reject);
  }
}

final topics = Topic();
