// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/core/error/error.dart';

void main() {
  test('401 错误', () async {
    await member.feedBack.getList(
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          print('resolve: $data');
        },
        reject: (HotmangasError error) {
          print(
              'reject: errorType: ${error.runtimeType}, ${error.errorType}, ${error.message}');
        });
  });

  test('403 错误', () async {
    member.login.addToken(token: 'error token');
    await member.feedBack.getList(
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          print('resolve: $data');
        },
        reject: (HotmangasError error) {
          print(
              'reject: errorType: ${error.runtimeType}, ${error.errorType}, ${error.message}');
        });
  });

  test('210 错误，登录', () async {
    await member.login.login(
        userName: 'errorUsername',
        passWord: 'errorPathword',
        resolve: (data) {
          print('resolve: $data');
        },
        reject: (HotmangasError error) {
          print(
              'reject: errorType: ${error.runtimeType}, ${error.errorType}, ${error.message}');
        });
  });

  test('210 错误，漫画不存在', () async {
    await comic.getDetail(
        pathWord: 'notExistsComicPathword',
        resolve: (data) {
          print(data);
        },
        reject: (HotmangasError error) {
          print(
              'reject: errorType: ${error.runtimeType}, ${error.errorType}, ${error.message}');
        });
  });

  test('404 GET 请求路径错误错误', () async {
    await testApi.getPathNotExists(resolve: (data) {
      print(data);
    }, reject: (HotmangasError error) {
      print(
          'reject: errorType: ${error.runtimeType}, ${error.errorType}, ${error.message}');
    });
  });

  test('404 POST 请求路径错误错误', () async {
    await testApi.postPathNotExists(resolve: (data) {
      print(data);
    }, reject: (HotmangasError error) {
      print(
          'reject: errorType: ${error.runtimeType}, ${error.errorType}, ${error.message}');
    });
  });

  test('CONNECTION TIMEOUT', () async {
    await testApi.getConnectionTimeout(resolve: (data) {
      print(data);
    }, reject: (HotmangasError error) {
      print(
          'reject: errorType: ${error.runtimeType}, ${error.errorType}, ${error.message}');
    });
  });

  test('RECEIVE TIMEOUT', () async {
    await testApi.getReceivedTimeout(resolve: (data) {
      print(data);
    }, reject: (HotmangasError error) {
      print(
          'reject: errorType: ${error.runtimeType}, ${error.errorType}, ${error.message}');
    });
  });
}
