import 'package:hotmangasCore/model/core/common/portrait_item.dart';

import '../core/common/common_list_results.dart' show CommonListResults;
import '../core/enmus/content_type.dart' show ContentType, ContentTypeEnumMap;
import '../core/enmus/decode.dart' show enumDecodeNullable;
import '../core/results_entity.dart' show ResultsEntity;

class UpdatePortraitListEntity extends ResultsEntity {
  Results? results;

  UpdatePortraitListEntity(
    code,
    message,
    this.results,
  ) : super(code, message);

  factory UpdatePortraitListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$UpdatePortraitListEntityFromJson(srcJson);
}

class Results extends CommonListResults {
  List<UpdatePortraitListItem?>? list;

  @override
  bool get hasWrapperItem {
    return true;
  }

  Results(this.list, total, limit, offset, type)
      : super(total, limit, offset, type);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

class UpdatePortraitListItem extends Object {
  String? name;
  String? datetimeCreated;
  PortraitItemEntity? post;

  UpdatePortraitListItem(this.datetimeCreated, this.name, this.post);

  factory UpdatePortraitListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$UpdateListItemFromJson(srcJson);
}

UpdatePortraitListEntity _$UpdatePortraitListEntityFromJson(
    Map<String, dynamic> json) {
  return UpdatePortraitListEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      (json['list'] as List)
          .map((e) => e == null
              ? null
              : UpdatePortraitListItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['total'],
      json['limit'],
      json['offset'],
      enumDecodeNullable(ContentTypeEnumMap, json['type']));
}

UpdatePortraitListItem _$UpdateListItemFromJson(Map<String, dynamic> json) {
  return UpdatePortraitListItem(
    json['datetime_created'] ?? '',
    json['name'] ?? '',
    json['post'] == null
        ? null
        : PortraitItemEntity.fromJson(json['post'] as Map<String, dynamic>),
  );
}
