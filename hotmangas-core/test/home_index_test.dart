// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show homeIndex;
import 'package:hotmangasCore/model/model.dart' show HomeIndexEntity;
import 'package:test/test.dart';

void main() {
  test('首页', () async {
    await homeIndex.getHomeIndex(resolve: (HomeIndexEntity data) {
      print(data.results?.banners?.length);
      for (var item in data.results!.banners!) {
        if (item?.item == null) {
          print('${item?.type} | ${item?.brief}');
        } else {
          print(
              '${item?.type} | ${item?.brief} | ${item?.item?.pathWord} | ${item?.item?.name == null}');
        }
      }

      print(data.results?.topics?.list?.length);
      for (var item in data.results!.topics!.list!) {
        print('${item?.pathWord} | ${item?.title}');
      }
      print(data.results?.recComics?.list?.length);
      for (var item in data.results!.recComics!.list!) {
        print(
            'recComic | ${item?.comic?.name} | ${item?.comic?.pathWord} | ${item?.comic?.author}');
      }
      print(data.results?.recCartoons?.list?.length);
      for (var item in data.results!.recCartoons!.list!) {
        print(
            'recCartoon | ${item?.comic?.name} | ${item?.comic?.pathWord} | ${item?.comic?.author}');
      }

      print(data.results?.updateWeeklyFreeComics?.list?.length);
      for (var item in data.results!.updateWeeklyFreeComics!.list!) {
        print(
            'updateWeeklyFreeComics: ${item?.comic?.name} | ${item?.comic?.pathWord} | ${item?.comic?.author}');
      }
      print(data.results?.updateWeeklyChargeComics?.list?.length);
      for (var item in data.results!.updateWeeklyChargeComics!.list!) {
        print(
            'updateWeeklyChargeComics: ${item?.comic?.name} | ${item?.comic?.pathWord} | ${item?.comic?.author}');
      }
      print(data.results?.updateWeeklyChargeCartoons?.list?.length);
      for (var item in data.results!.updateWeeklyChargeCartoons!.list!) {
        print(
            'updateWeeklyChargeCartoons: ${item?.cartoon?.name} | ${item?.cartoon?.pathWord} | ${item?.cartoon?.company}');
      }

      print(data.results?.rankWeeklyFreeComics?.list?.length);
      for (var item in data.results!.rankWeeklyFreeComics!.list!) {
        print(
            'rankWeeklyFreeComics: ${item?.comic?.name} | ${item?.comic?.pathWord} | ${item?.comic?.author}');
      }
      print(data.results?.rankWeeklyChargeComics?.list?.length);
      for (var item in data.results!.rankWeeklyChargeComics!.list!) {
        print(
            'rankWeeklyChargeComics: ${item?.comic?.name} | ${item?.comic?.pathWord} | ${item?.comic?.author}');
      }
      print(data.results?.rankWeeklyChargeCartoons?.list?.length);
      for (var item in data.results!.rankWeeklyChargeCartoons!.list!) {
        print(item?.cartoon?.company);
      }
      print(data.results!.openOrders);
    }, reject: (error) {
      print(error);
    });
  });
}
