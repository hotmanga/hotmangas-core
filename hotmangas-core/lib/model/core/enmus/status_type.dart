enum StatusType {
  /// 连载
  Serializing,

  /// 已完结
  Finished,

  /// 短篇
  Short
}

const StatusTypeEnumMap = {
  StatusType.Serializing: 0,
  StatusType.Finished: 1,
  StatusType.Short: 2
};
