import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show MemberWelfareListEntity;
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'limit': 21,
  'offset': 0,
};

class Welfare extends HttpCore {
  Future<void> getList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/welfares',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberWelfareListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getDetails(
      {@required String? welfareUuid,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/member/welfare/$welfareUuid',
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }
}
