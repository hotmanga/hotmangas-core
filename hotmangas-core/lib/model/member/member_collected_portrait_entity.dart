import 'package:hotmangasCore/model/model.dart' show ResultsEntity;

class MemberCollectPortraitEntity extends ResultsEntity {
  MemberCollectedItem? results;

  MemberCollectPortraitEntity(code, message, this.results)
      : super(code, message);

  factory MemberCollectPortraitEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberCollectPortraitEntityFromJson(srcJson);
}

MemberCollectPortraitEntity _$MemberCollectPortraitEntityFromJson(
    Map<String, dynamic> json) {
  return MemberCollectPortraitEntity(
    json['code'],
    json['message'],
    MemberCollectedItem.fromJson(json['results']),
  );
}

class MemberCollectedItem extends Object {
  int? collect;

  bool? isLock;

  bool? isLogin;

  bool? isMobileBind;

  bool? isVip;

  bool? isBuy;

  ///内容不存在已下架 仅在写真详情页需要
  bool? isDelete;

  bool? get hasCollect {
    return collect != -1;
  }

  ///打赏模特列表
  List<RewardMannequinsList?>? mannequins;

  /// 模特所对应的打赏金额
  Map<String?, String?>? get mannequinsRewardMap {
    var _rewardMap = <String?, String?>{};

    for (var i = 0; i < mannequins!.length; i++) {
      _rewardMap.putIfAbsent(mannequins![i]!.pathWord, () {
        return mannequins![i]!.rewards;
      });
    }
    return _rewardMap;
  }

  dynamic browse;

  MemberCollectedItem(
    this.collect,
    this.isLock,
    this.isLogin,
    this.isMobileBind,
    this.isVip,
    this.isBuy,
    this.browse,
    this.isDelete,
    this.mannequins,
  );

  factory MemberCollectedItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberCollectedItemFromJson(srcJson);
}

MemberCollectedItem _$MemberCollectedItemFromJson(Map<String, dynamic> json) {
  return MemberCollectedItem(
    json['collect'] ?? -1,
    json['is_lock'],
    json['is_login'],
    json['is_mobile_bind'],
    json['is_vip'],
    json['is_buy'],
    json['browse'],
    json['is_delete'],
    json['mannequins'] == null
        ? null
        : (json['mannequins'] as List)
            .map((e) => e == null
                ? null
                : RewardMannequinsList.fromJson(e as Map<String, dynamic>))
            .toList(),
  );
}

class QueryBrowseEntity extends Object {
  QueryBrowseEntity();

  factory QueryBrowseEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$QueryBrowseEntityFromJson(srcJson);
}

QueryBrowseEntity _$QueryBrowseEntityFromJson(Map<String, dynamic> json) {
  return QueryBrowseEntity();
}

class RewardMannequinsList extends Object {
  /// 模特名称
  String? name;

  ///模特pathword
  String? pathWord;

  ///打赏了多少钱
  String? rewards;

  RewardMannequinsList(
    this.name,
    this.pathWord,
    this.rewards,
  );

  factory RewardMannequinsList.fromJson(Map<String, dynamic> srcJson) =>
      _$RewardMannequinsListFromJson(srcJson);
}

RewardMannequinsList _$RewardMannequinsListFromJson(Map<String, dynamic> json) {
  return RewardMannequinsList(
    json['name'] ?? '',
    json['path_word'] ?? '',
    json['rewards'] ?? '',
  );
}
