class ComicQueryBrowseEntity extends Object {
  String? comicUuid;
  String? pathWord;
  String? chapterUuid;
  String? chapterName;

  ComicQueryBrowseEntity(
      this.comicUuid, this.pathWord, this.chapterUuid, this.chapterName);

  factory ComicQueryBrowseEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$ChapterContentFromJson(srcJson);
}

ComicQueryBrowseEntity _$ChapterContentFromJson(Map<String, dynamic> json) {
  return ComicQueryBrowseEntity(json['comic_uuid'], json['path_word'],
      json['chapter_uuid'], json['chapter_name']);
}
