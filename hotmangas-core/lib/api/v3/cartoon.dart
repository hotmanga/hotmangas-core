import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        MemberCollectEntity,
        CartoonDetailEntity,
        CartoonListEntity,
        CommonListEntity,
        FreeType,
        FreeTypeEnumMap,
        PlayInfoEntity,
        PlayLineType,
        PlayLineTypeEnumMap;
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'free_type': FreeTypeEnumMap[FreeType.None],
  'limit': 21,
  'offset': 0,
};

class Cartoon extends HttpCore {
  Future<void> getDetail(
      {@required String? cartoonPathWord,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/cartoon2/$cartoonPathWord',
        resolve: (data) {
          resolve?.call(CartoonDetailEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getChapters(
      {@required String? cartoonPathWord,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/cartoon/$cartoonPathWord/chapters',
        resolve: (data) {
          resolve?.call(CartoonListEntity.fromJson(data));
        },
        reject: reject);
  }

  /*
   * @example
   * /api/v3/cartoons?free_type=1&limit=21&offset=0&ordering=-datetime_updated&_update=true
   */
  Future<void> getQueryList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/cartoons',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CommonListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getCurrentCartoonCollect(
      {@required String? pathWord, Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/cartoon2/$pathWord/query',
        resolve: (data) {
          resolve?.call(MemberCollectEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getPlayInfo(
      {@required PlayLineType? playLineType,
      @required String? cartoonPathword,
      @required String? cartoonChapterId,
      Function? resolve,
      Function? reject}) async {
    final line = PlayLineTypeEnumMap[playLineType];
    await getCore(
        path: '/api/v3/cartoon/$cartoonPathword/chapter2/$cartoonChapterId',
        queryParameters: {'line': line},
        resolve: (data) {
          resolve?.call(PlayInfoEntity.fromJson(data));
        },
        reject: reject);
  }
}

final cartoon = Cartoon();
