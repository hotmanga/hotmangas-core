import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        MemberForgetAnswerEntity,
        MemberSafetyListEntity,
        MemberSafetyMyListEntity,
        MemberSecurityAnswerEntity;
import 'package:meta/meta.dart' show required;

class Safety extends HttpCore {
  Future<void> getList({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/member/securityquestionall/',
        resolve: (data) {
          resolve?.call(MemberSafetyListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getMyList({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/member/securityanwserall/',
        resolve: (data) {
          resolve?.call(MemberSafetyMyListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> postSecurityAnwser(
      {int id = 0,
      @required String? question,
      @required String? answer,
      Function? resolve,
      Function? reject}) async {
    var _id = id == 0 ? '' : id.toString();
    await postCore(
        path: '/api/v3/member/securityanwser/',
        formData: {
          'id': _id,
          'question': question,
          'answer': answer,
        },
        resolve: (data) {
          // resolve.call(data);
          resolve?.call(MemberSecurityAnswerEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> postAnswerStep1(
      {@required String? userName, Function? resolve, Function? reject}) async {
    await postCore(
        path: '/api/v3/forget/answer/step1',
        formData: {
          'username': userName,
        },
        resolve: (data) {
          resolve?.call(MemberForgetAnswerEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> postAnswerStep2(
      {@required int? answerId,
      @required String? token,
      @required String? answer,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/forget/answer/step2',
        formData: {
          'token': token,
          'answer_id': answerId,
          'answer': answer,
        },
        resolve: (data) {
          resolve?.call(MemberForgetAnswerEntity.fromJson(data));
          // resolve.call(data);
        },
        reject: reject);
  }

  Future<void> postAnswerStep3(
      {@required String? token,
      @required String? password,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/forget/answer/step3',
        formData: {
          'token': token,
          'password': password,
        },
        resolve: (data) {
          // resolve.call(MemberForgetAnswerEntity.fromJson(data));
          resolve?.call(data);
        },
        reject: reject);
  }
}
