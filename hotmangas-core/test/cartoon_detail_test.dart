// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show cartoon;
import 'package:hotmangasCore/model/model.dart' show CartoonDetailEntity;

void main() {
  test('测试获取动画详情，测试的动画PathWord为luminousart', () async {
    CartoonDetailEntity? response;
    await cartoon.getDetail(
        cartoonPathWord: 'luminousart',
        resolve: (CartoonDetailEntity data) {
          response = data;
        });
    expect(response?.runtimeType, equals(CartoonDetailEntity));
    expect(response?.code, equals(200), reason: '1');
    expect(response?.message, equals('请求成功'), reason: '2');
    expect(response?.results?.cartoon, isNotNull, reason: '3');
    expect(response?.results?.cartoon?.parodies, isNull, reason: '4');
    expect(response?.results?.collect, isNull, reason: '5');
    expect(response?.results?.isLock, isTrue, reason: '6');
    expect(response?.results?.isLogin, isFalse, reason: '7');
    expect(response?.results?.isMobileBind, isFalse, reason: '8');
    expect(response?.results?.isVip, isFalse, reason: '9');
  });
}
