// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show cartoon;
import 'package:hotmangasCore/model/model.dart' show CartoonListEntity, Line;

void main() {
  test('测试获取动画章节，测试的动画PathWord为luminousart', () async {
    CartoonListEntity? response;
    await cartoon.getChapters(
        cartoonPathWord: 'luminousart',
        resolve: (CartoonListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response?.runtimeType, equals(CartoonListEntity));
    expect(response?.code, equals(200));
    expect(response?.results?.list?.length, greaterThan(0));
    expect(response?.results?.list?.first?.lines?.line1, isA<Line>());
    expect(response?.results?.list?.first?.lines?.line2, isA<Line>());
    expect(response?.results?.list?.first?.lines?.line3, isA<Line>());
  });
}
