import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        RankListEntity,
        RankType,
        RankTypeEnumMap,
        RankDateType,
        RankDateTypeEnumMap;

final Map<String, dynamic> _queryParameters = {
  'type': RankTypeEnumMap[RankType.Error],
  'date_type': RankDateTypeEnumMap[RankDateType.Error],
  'limit': 21,
  'offset': 0,
  'audience_type': 'male',
};

class Rank extends HttpCore {
  Future<void> getList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/ranks',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(RankListEntity.fromJson(data));
        },
        reject: reject);
  }
}

final rank = Rank();
