import 'package:hotmangasCore/model/member/member_comment_collect_lists.dart';

class MemberCommentCollectList extends Object {
  // 总共有多少条留言
  int? total;

  // 留言列表
  List<MemberCommentCollectLists>? list;

  // 每一页多少个
  int? limit;

  // 每一页的偏移量
  int? offset;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'total': total,
      'list': list,
      'limit': limit,
      'offset': offset,
    };
  }

  MemberCommentCollectList(
    this.total,
    this.list,
    this.limit,
    this.offset,
  );

  factory MemberCommentCollectList.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberCommentCollectListFromJson(srcJson);
}

MemberCommentCollectList _$MemberCommentCollectListFromJson(
    Map<String, dynamic> json) {
  return MemberCommentCollectList(
    json['total'],
    (json['list'] as List)
        .map((e) => MemberCommentCollectLists.fromJson(e))
        .toList(),
    json['limit'],
    json['offset'],
  );
}
