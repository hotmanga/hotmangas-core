import 'package:hotmangasCore/api/v3/api.dart' show rank;
import 'package:hotmangasCore/model/model.dart'
    show
        RankListEntity,
        ContentRankTypeEnumMap,
        ContentRankType,
        ContentRankDateType,
        InnerItem;
import 'package:test/test.dart';

void main() {
  test('测试获取排行榜 轻小说 日排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.Book],
          'date_type': ContentRankTypeEnumMap[ContentRankDateType.Day],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list?.length, equals(1));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list?[0]?.wrapperItem, isA<InnerItem>());
  });

  test('测试获取排行榜 轻小说 周排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.Book],
          'date_type': ContentRankTypeEnumMap[ContentRankDateType.Week],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list?.length, equals(1));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list?[0]?.wrapperItem, isA<InnerItem>());
  });

  test('测试获取排行榜 轻小说 月排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.Book],
          'date_type': ContentRankTypeEnumMap[ContentRankDateType.Month],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list?.length, equals(1));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list?[0]?.wrapperItem, isA<InnerItem>());
  });

  test('测试获取排行榜 轻小说 总排行榜', () async {
    RankListEntity? response;
    await rank.getList(
        queryParameters: {
          'type': ContentRankTypeEnumMap[ContentRankType.Book],
          'date_type': ContentRankTypeEnumMap[ContentRankDateType.Total],
          'limit': 1,
          'offset': 0
        },
        resolve: (RankListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<RankListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list?.length, equals(1));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list?[0]?.wrapperItem, isA<InnerItem>());
  });
}
