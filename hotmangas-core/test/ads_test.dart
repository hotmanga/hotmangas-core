import 'package:hotmangasCore/api/v3/api.dart' show ads;
import 'package:hotmangasCore/model/ads/ads_all_entity.dart';
import 'package:hotmangasCore/model/model.dart'
    show AdsAllEntity, AdsDetailEntity, ResultsEntity;
import 'package:test/test.dart';

void main() {
  test('调用获取所有广告信息', () async {
    AdsAllEntity? response;
    Map<String, IdentItem>? adsAll = {};
    await ads.getAllAds(
        queryParameters: {
          'ident': 1720001,
          'channels': '2001',
        },
        resolve: (AdsAllEntity _response) {
          response = _response;
          // List<IdentItem?>? list = _response.results!.list!;
          // list.forEach((val) {
          //   adsAll.putIfAbsent(val!.ident!, () => val);
          // });
          print(adsAll);
        });

    expect(response, isA<AdsAllEntity>());
    // expect(response!.results!.list!.first!.ident, equals('200100001'));
    // expect(response!.results!.list!.first!.name, equals('2.0.5-安卓冷启开屏'));
    expect(response?.code, equals(200));
  });
  test('调用请求广告详细信息', () async {
    AdsDetailEntity? response;

    await ads.postAdsDetail(
        queryParameters: {
          'ident': 200100001,
          'channels': '2001,2002',
        },
        resolve: (AdsDetailEntity? _response) {
          response = _response;
        });
    expect(response, isA<AdsDetailEntity>());
    expect(response!.results!.channel_ident, equals('2002'));
    expect(response?.code, equals(200));
  });
  test('调用广告错误重新请求新广告', () async {
    AdsDetailEntity? response;

    await ads.postErrorAds(
        queryParameters: {
          'error': "{'ss':'22'}",
          'params':
              'eyJyaWQiOiAiNjRhZmE2NmIwZmUyMDQ0OTg2M2VhMGY3IiwgInBpZCI6IDIsICJjaWQiOiAyLCAiY2dpZCI6IDYsICJhaWQiOiAxLCAiZXh0IjogbnVsbCwgInRzIjogMTY4OTIzMzAwMywgIm5leHQiOiBbMV19',
        },
        resolve: (AdsDetailEntity _response) {
          response = _response;
        });
    expect(response, isA<AdsDetailEntity>());
    expect(response!.results!.channel_ident, equals('2002'));
    expect(response?.code, equals(200));
  });

  test('调用广告点击接口', () async {
    ResultsEntity? response;

    await ads.postClickAds(
        queryParameters: {
          'params':
              'eyJyaWQiOiAiNjRhZmE2NmIwZmUyMDQ0OTg2M2VhMGY3IiwgInBpZCI6IDIsICJjaWQiOiAyLCAiY2dpZCI6IDYsICJhaWQiOiAxLCAiZXh0IjogbnVsbCwgInRzIjogMTY4OTIzMzAwMywgIm5leHQiOiBbMV19',
        },
        resolve: (_response) {
          response = _response;
        });
    expect(response?.code, equals(200));
  });

  test('调用广告曝光接口', () async {
    ResultsEntity? response;

    await ads.postAdsExposure(
        queryParameters: {
          'params':
              'eyJyaWQiOiAiNjRhZmE2NmIwZmUyMDQ0OTg2M2VhMGY3IiwgInBpZCI6IDIsICJjaWQiOiAyLCAiY2dpZCI6IDYsICJhaWQiOiAxLCAiZXh0IjogbnVsbCwgInRzIjogMTY4OTIzMzAwMywgIm5leHQiOiBbMV19',
        },
        resolve: (_response) {
          response = _response;
        });
    expect(response?.code, equals(200));
  });

  test('调用广告点击接口', () async {
    ResultsEntity? response;

    await ads.postClickAds(
        queryParameters: {
          'params':
              'eyJyaWQiOiAiNjRhZmE2NmIwZmUyMDQ0OTg2M2VhMGY3IiwgInBpZCI6IDIsICJjaWQiOiAyLCAiY2dpZCI6IDYsICJhaWQiOiAxLCAiZXh0IjogbnVsbCwgInRzIjogMTY4OTIzMzAwMywgIm5leHQiOiBbMV19',
        },
        resolve: (_response) {
          response = _response;
        });
    expect(response?.code, equals(200));
  });
}
