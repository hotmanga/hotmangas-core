import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show RecommendType, RecommendTypeEnumMap, RecommendComicListEntity;
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'pos': null,
  'limit': 20,
  'offset': 0,
};

class Recommend extends HttpCore {
  Future<void> _getList(
      {@required String? path,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: path, queryParameters: _, resolve: resolve, reject: reject);
  }

  Future<void> getList(
      {@required RecommendType? position,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    queryParameters?.addAll({'pos': RecommendTypeEnumMap[position]});
    await _getList(
        path: '/api/v3/recs',
        queryParameters: queryParameters,
        resolve: (data) {
          resolve?.call(RecommendComicListEntity.fromJson(data));
        },
        reject: reject);
  }
}

final recommend = Recommend();
