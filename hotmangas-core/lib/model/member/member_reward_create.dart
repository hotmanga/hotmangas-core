class MemberRewardCreate extends Object {
  //激励广告showId
  int? showId;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'showId': showId,
    };
  }

  MemberRewardCreate(
    this.showId,
  );

  factory MemberRewardCreate.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberInfoFromJson(srcJson);
}

MemberRewardCreate _$MemberInfoFromJson(Map<String, dynamic> json) {
  return MemberRewardCreate(
    json['show_id'],
  );
}
