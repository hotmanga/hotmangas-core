import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberCartoonDeleteEntity extends ResultsEntity {
  MemberCartoonDeleteEntity(int code, String message) : super(code, message);

  factory MemberCartoonDeleteEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberComicDeleteEntityFromJson(srcJson);
}

MemberCartoonDeleteEntity _$MemberComicDeleteEntityFromJson(
    Map<String, dynamic> json) {
  return MemberCartoonDeleteEntity(
    json['code'],
    json['message'],
  );
}
