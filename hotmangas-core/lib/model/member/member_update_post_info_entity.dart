import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberUpdatePostEntity extends ResultsEntity {
  MemberUpdatePostEntity(code, message) : super(code, message);

  factory MemberUpdatePostEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberFeedBackEntity(srcJson);
}

MemberUpdatePostEntity _$MemberFeedBackEntity(Map<String, dynamic> json) {
  return MemberUpdatePostEntity(
    json['code'],
    json['message'],
  );
}
