import 'package:hotmangasCore/model/core/common/author_or_company.dart'
    show AuthorOrCompany;
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/common/inner_item.dart' show InnerItem;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberCollectCartoonListEntity extends ResultsEntity {
  Results? results;

  MemberCollectCartoonListEntity(code, message, this.results)
      : super(code, message);

  factory MemberCollectCartoonListEntity.fromJson(
          Map<String, dynamic> srcJson) =>
      _$MemberCollectCartoonListEntityFromJson(srcJson);
}

MemberCollectCartoonListEntity _$MemberCollectCartoonListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberCollectCartoonListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<MemberCollectCartoonItem>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List)
        .map((e) => MemberCollectCartoonItem.fromJson(e))
        .toList(),
  );
}

class MemberCollectCartoonItem extends Object {
  int? uuid;

  String? name;

  bool? bFolder;

  CollectCartoonInnerItem? cartoon;

  int? folderId;

  MemberCollectCartoonItem(
      this.uuid, this.name, this.bFolder, this.folderId, this.cartoon);

  factory MemberCollectCartoonItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberCollectCartoonItemFromJson(srcJson);
}

MemberCollectCartoonItem _$MemberCollectCartoonItemFromJson(
    Map<String, dynamic> json) {
  return MemberCollectCartoonItem(
    json['uuid'],
    json['name'],
    json['b_folder'],
    json['folder_id'],
    CollectCartoonInnerItem.fromJson(json['cartoon']),
  );
}

class CollectCartoonInnerItem extends InnerItem {
  bool? bDisplay;

  // int status;

  // String lastChapterId;

  String? lastChapterName;

  String? years;

  int? count;

  bool? bSubtitle;

  CollectCartoonInnerItem(
    name,
    pathWord,
    company,
    cover,
    popular,
    mannequins,
    this.bDisplay,
    this.lastChapterName,
    this.years,
    this.count,
    this.bSubtitle,
  ) : super(
          name,
          pathWord,
          null,
          company,
          cover,
          popular,
          '-',
          mannequins,
          null,
          null,
          null,
        );

  factory CollectCartoonInnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$CollectInnerItemFromJson(srcJson);
}

CollectCartoonInnerItem _$CollectInnerItemFromJson(Map<String, dynamic> json) {
  return CollectCartoonInnerItem(
    json['name'],
    json['path_word'],
    json['company'] == null ? null : AuthorOrCompany.fromJson(json['company']),
    json['cover'],
    json['popular'],
    json['mannequins'] == null
        ? null
        : (json['mannequins'] as List)
            .map((e) => e == null
                ? null
                : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
            .toList(),
    json['b_display'] ?? false,
    json['last_chapter_name'] ?? '',
    json['years'],
    json['count'],
    json['b_subtitle'],
  );
}
