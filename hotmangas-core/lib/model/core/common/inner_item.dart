import 'package:hotmangasCore/api/utils/export.dart' show datetimeFormat;
import 'package:hotmangasCore/model/core/enmus/decode.dart';
import 'package:hotmangasCore/model/model.dart'
    show AuthorOrCompany, ContentType, ContentTypeEnumMap;

/*
 * Comic 和  Cartoon 复用的元素类。
 */

class InnerItem extends Object {
  /// 名称
  String? name;

  /// pathword
  String? pathWord;

  /// 只有comic 和 book 有
  List<AuthorOrCompany?>? _author;

  /// 只有cartoon有
  AuthorOrCompany? _company;

  /// 只有portrait 有
  List<AuthorOrCompany?>? _mannequins;

  // 封面
  String? cover;

  /// 热度
  int? popular;

  /// 最后更新时间
  String? _datetimeUpdated;

  ///该type仅在推荐使用 用于区分漫画和写真集
  ContentType? type;

  String? get company {
    return _company?.name;
  }

  String? ticket;

  dynamic _freeType;

  int? get freeType {
    if (_freeType is Map) {
      return int.parse(_freeType['value']);
    }
    return _freeType;
  }

  List<AuthorOrCompany?>? get author {
    return _author;
  }

  List<AuthorOrCompany?>? get mannequins {
    return _mannequins;
  }

  /// 如果时间没有, 则会返回 "-"
  String? get datetimeUpdated {
    if (_datetimeUpdated != null && _datetimeUpdated!.isNotEmpty) {
      return datetimeFormat(DateTime.parse(_datetimeUpdated!));
    }
    return '-';
  }

  InnerItem(
    this.name,
    this.pathWord,
    this._author,
    this._company,
    this.cover,
    this.popular,
    this._datetimeUpdated,
    this._mannequins,
    this.type,
    this.ticket,
    this._freeType,
  );

  factory InnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$InnerItemFromJson(srcJson);
}

InnerItem _$InnerItemFromJson(Map<String, dynamic> json) {
  return InnerItem(
    json['name'],
    json['path_word'],
    json['author'] == null
        ? null
        : (json['author'] as List)
            .map((e) => e == null
                ? null
                : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
            .toList(),
    json['company'] == null
        ? null
        : AuthorOrCompany.fromJson(json['company'] as Map<String, dynamic>),
    json['cover'],
    json['popular'],
    json['datetime_updated'] ?? '',
    json['mannequins'] == null
        ? null
        : (json['mannequins'] as List)
            .map((e) => e == null
                ? null
                : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
            .toList(),
    enumDecodeNullable(ContentTypeEnumMap, json['type']),
    json['ticket'] ?? '',
    json['free_type'],
  );
}
