import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;
import 'package:hotmangasCore/model/model.dart' show MemberWelfareItem;

class MemberWelfareListEntity extends ResultsEntity {
  List<MemberWelfareItem>? results;

  MemberWelfareListEntity(code, message, this.results) : super(code, message);

  factory MemberWelfareListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberWelfareListEntityFromJson(srcJson);
}

MemberWelfareListEntity _$MemberWelfareListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberWelfareListEntity(
    json['code'],
    json['message'],
    (json['results'] as List)
        .map((e) => MemberWelfareItem.fromJson(e))
        .toList(),
  );
}
