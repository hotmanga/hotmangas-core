class MemberRewardShowTotal extends Object {
  /// 写真激励视频每天可以看的最大次数
  int? total;

  /// 写真激励视频当前看了多少次
  int? show;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'total': total,
      'show': show,
    };
  }

  MemberRewardShowTotal(
    this.total,
    this.show,
  );

  factory MemberRewardShowTotal.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberInfoFromJson(srcJson);
}

MemberRewardShowTotal _$MemberInfoFromJson(Map<String, dynamic> json) {
  return MemberRewardShowTotal(
    json['total'] ?? 0,
    json['show'] ?? 0,
  );
}
