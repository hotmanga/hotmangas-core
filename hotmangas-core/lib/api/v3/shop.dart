import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show GoodEntity, GoodListEntity;
import 'package:meta/meta.dart' show required;

class Shop extends HttpCore {
  Future<void> getGoodList({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/goods',
        resolve: (data) {
          resolve?.call(GoodListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getCurrentGoodChannels(
      {@required String? goodUuid, Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/goods/$goodUuid',
        resolve: (data) {
          resolve?.call(GoodEntity.fromJson(data));
        },
        reject: reject);
  }
}

final shop = Shop();
