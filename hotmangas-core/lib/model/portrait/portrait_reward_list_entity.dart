import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/common/reward_list_entity.dart'
    show RewardList;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class PortraitRewardListEntity extends ResultsEntity {
  Results results;

  PortraitRewardListEntity(code, message, this.results) : super(code, message);

  factory PortraitRewardListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitRewardListEntityFromJson(srcJson);
}

PortraitRewardListEntity _$PortraitRewardListEntityFromJson(
    Map<String, dynamic> json) {
  return PortraitRewardListEntity(
      json['code'], json['message'], Results.fromJson(json['results']));
}

class Results extends CommonListResults {
  List<RewardList?>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    json['list'] == null
        ? null
        : (json['list'] as List)
            .map((e) => e == null
                ? null
                : RewardList.fromJson(e as Map<String, dynamic>))
            .toList(),
  );
}
