enum TxtEncodingType {
  /// 无
  None,

  /// GBK
  GBK,

  /// GB18030
  GB18030,

  /// UTF8
  UTF8,

  /// UTF16
  UTF16,

  /// UTF32
  UTF32,
}

@deprecated
const TxtEncodingTypeMap = {
  TxtEncodingType.None: '',
  TxtEncodingType.GBK: 'GBK',
  TxtEncodingType.GB18030: 'GB18030',
  TxtEncodingType.UTF8: 'UTF-8',
  TxtEncodingType.UTF16: 'UTF-16',
  TxtEncodingType.UTF32: 'UTF-32',
};
