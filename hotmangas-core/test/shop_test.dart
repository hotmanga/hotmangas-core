// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';

void main() async {
  GoodListEntity? goodListEntity;
  await shop.getGoodList(resolve: (GoodListEntity? data) {
    goodListEntity = data;
  }, reject: (error) {
    print(error);
  });

  GoodEntity? goodEntity;
  var currentSelectGoodItem = goodListEntity?.results?.goods?.vip60?.first;
  await shop.getCurrentGoodChannels(
      goodUuid: currentSelectGoodItem?.uuid,
      resolve: (GoodEntity data) {
        goodEntity = data;
      },
      reject: (error) {
        print(error);
      });

  test('获取商品列表 接口基本信息', () {
    expect(goodListEntity, isA<GoodListEntity>(), reason: '1');
    expect(goodListEntity?.code, equals(200), reason: '2');
    expect(goodListEntity?.results?.goods, isA<Goods>(), reason: '3');
    expect(goodListEntity?.results?.show, isFalse, reason: '4');
    expect(goodListEntity?.results?.code, isEmpty, reason: '5');
  });

  test('获取商品列表 商品列表', () {
    expect(goodListEntity, isA<GoodListEntity>());
    expect(goodListEntity!.code, equals(200));
    expect(goodListEntity!.results?.goods?.vip10?.first.vipType, equals(10));
    expect(goodListEntity!.results?.goods?.vip20?.first.vipType, equals(20));
    expect(goodListEntity!.results?.goods?.vip30?.first.vipType, equals(30));
    expect(goodListEntity!.results?.goods?.vip30?.first.brief, isNull);
  });

  test('获取单个商品详情 接口基本信息', () {
    expect(goodEntity!.code, equals(200));
    expect(goodEntity, isA<GoodEntity>());
  });

  test('获取单个商品详情 基本信息', () {
    expect(goodEntity!.results?.uuid, equals(currentSelectGoodItem?.uuid));
    expect(goodEntity!.results?.name, equals(currentSelectGoodItem?.name));
    expect(goodEntity!.results?.priceOrig,
        equals(currentSelectGoodItem?.priceOrig));
    expect(goodEntity!.results?.price, equals(currentSelectGoodItem?.price));
    expect(goodEntity!.results?.channels?.first.channelItem, isNotNull);
    expect(goodEntity!.results?.channels?.first.wayItem, isNotNull);
    expect(goodEntity!.results?.channels?.first.uuid, isNotEmpty);
  });
}
