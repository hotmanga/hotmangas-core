import 'package:hotmangasCore/hot/dio_warpper.dart';
import 'package:hotmangasCore/hot/options.dart';
import 'package:meta/meta.dart' show required;

class HttpCoreExtra extends HttpCore {
  /// 退出登录时，清空Token，或者外层判断403时，清空token
  void clearToken() {
    mergeDioOptionExtra({'authorization': 'Token '});
  }

  /// 添加token，一般是用来在程序启动时添加。
  void addToken({@required String? token}) {
    mergeDioOptionExtra({'authorization': 'Token $token'});
  }

  void addSource({@required String? source}) {
    mergeDioOptionExtra({'source': '$source'});
  }

  void addWebP({@required String? webp}) {
    mergeDioOptionExtra({'webp': '$webp'});
  }

  void addRegion({@required String? region}) {
    mergeDioOptionExtra({'region': '$region'});
  }

  // 添加32位小写MD5
  void addUMString({@required String? umString}) {
    mergeDioOptionExtra({'umString': '$umString'});
  }

  //添加接口请求referer
  void addReferer({@required String? referer}) {
    mergeDioOptionExtra({'referer': '$referer'});
  }

  //添加接口请求的安卓id
  void addAndroidId({@required String? androidId}) {
    mergeDioOptionExtra({'androidId': '$androidId'});
  }

  //添加接口请求的手机型号
  void addDeviceInfo({@required String? deviceInfo}) {
    mergeDioOptionExtra({'deviceInfo': '$deviceInfo'});
  }

  ///添加是否是谷歌渠道
  void addisGoogle({@required bool? isGoogle}) {
    mergeDioOptionExtra({'isGoogle': isGoogle});
  }

  ///添加簡體繁體英文  英文en   繁體zh-hant  簡體zh-hans
  void addLanguage({@required String? language}) {
    mergeDioOptionExtra({'Accept-Language': language});
    SystemNetworkCache.setLang(language);
  }
}

final httpCoreExtra = HttpCoreExtra();
