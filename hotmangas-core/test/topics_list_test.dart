// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/topics.dart' show topics;
import 'package:hotmangasCore/model/model.dart' show ContentType;
import 'package:hotmangasCore/model/model.dart' show Topic;
import 'package:hotmangasCore/model/model.dart' show TopicListEntity;

void main() {
  test('测试获取专题列表-默认参数', () async {
    TopicListEntity? response;
    await topics.getList(
        queryParameters: {},
        resolve: (TopicListEntity data) {
          response = data;
        });
    expect(response, isA<TopicListEntity>());
    expect(response!.code, equals(200));
    expect(response!.results?.list, isList);
    expect(response!.results?.list, isEmpty);
  });

  test('测试获取专题列表-获取空列表', () async {
    TopicListEntity? response;
    await topics.getList(
        queryParameters: {'type': ContentType.None.index},
        resolve: (TopicListEntity data) {
          response = data;
        });
    expect(response, isA<TopicListEntity>());
    expect(response!.code, equals(200));
    expect(response!.results?.list, isList);
    expect(response!.results?.list, isEmpty);
  });

  test('测试获取专题列表-获取漫画 type=1', () async {
    TopicListEntity? response;
    await topics.getList(
        queryParameters: {
          'type': ContentType.Comic.index,
          'limit': 1,
          'offset': 1,
        },
        resolve: (TopicListEntity data) {
          response = data;
        });
    expect(response, isA<TopicListEntity>(), reason: '1');
    expect(response!.code, equals(200), reason: '2');
    expect(response!.results?.list, isList, reason: '3');
    expect(response!.results?.list?.length, equals(1), reason: '4');
    expect(response!.results?.list?.first, isA<Topic>(), reason: '5');
    expect(response!.results?.list?.first?.type, equals(ContentType.Comic),
        reason: '6');
    expect(response!.results?.limit, equals(1), reason: '7');
    expect(response!.results?.offset, equals(1), reason: '8');
  });

  test('测试获取专题列表-获取动画 type=2', () async {
    TopicListEntity? response;
    await topics.getList(
        queryParameters: {
          'type': ContentType.Cartoon.index,
          'limit': 2,
          'offset': 0,
        },
        resolve: (TopicListEntity data) {
          response = data;
        });
    expect(response, isA<TopicListEntity>());
    expect(response!.code, equals(200));
    expect(response!.results?.list, isList);
    expect(response!.results?.list?.length, equals(2));
    expect(response!.results?.list?.first, isA<Topic>());
    expect(response!.results?.list?.first?.type, equals(ContentType.Cartoon));
    expect(response!.results?.limit, equals(2));
    expect(response!.results?.offset, equals(0));
  });

  test('测试获取专题详情页下方的内容列表 ', () async {
    await topics.getDetailList(
        topicPathWord: 'donghua202009',
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          print(data);
        },
        reject: (error) {
          print(error);
        });
  });

  test('测试获取专题详情页本身的内容', () async {
    await topics.getDetail(
        topicPathWord: 'donghua202009',
        resolve: (data) {
          print(data);
        },
        reject: (error) {
          print(error);
        });
  });
}
