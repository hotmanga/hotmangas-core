class FeedBackRemark extends Object {
  String? deviceInfo;

  String? appVersion;

  String? utmSource;

  String? oaid;

  @override
  String toString() {
    return '$deviceInfo/$appVersion/$utmSource/$oaid';
  }

  FeedBackRemark(this.deviceInfo, this.appVersion, this.utmSource, this.oaid);
}
