import 'package:hotmangasCore/model/core/enmus/decode.dart'
    show enumDecodeNullable;
import 'package:hotmangasCore/model/core/enmus/home_index_banner_type.dart'
    show BannerType, BannerTypeEnumMap;
import 'package:hotmangasCore/model/model.dart' show InnerItem;

class Banners extends Object {
  BannerType? type;

  String? cover;

  String? brief;

  String? outUuid;

  InnerItem? item;

  Banners(
    this.type,
    this.cover,
    this.brief,
    this.outUuid,
    this.item,
  );

  factory Banners.fromJson(Map<String, dynamic> srcJson) =>
      _$BannersFromJson(srcJson);
}

Banners _$BannersFromJson(Map<String, dynamic> json) {
  return Banners(
    enumDecodeNullable(BannerTypeEnumMap, json['type']),
    json['cover'],
    json['brief'],
    json['out_uuid'] ?? '',
    json['comic'] == null
        ? null
        : InnerItem.fromJson(json['comic'] as Map<String, dynamic>),
  );
}
