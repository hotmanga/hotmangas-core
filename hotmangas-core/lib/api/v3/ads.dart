import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show AdsAllEntity, AdsDetailEntity, ResultsEntity;

final Map<String, dynamic> _queryParameters = {
  'ident': '',
  'channels': '',
};

final Map<String, dynamic> _adsPorint = {
  'params': '',
  'union_id': '',
  'type': '', // 1初始化   2加載的時候
};

final Map<String, dynamic> _errorParameters = {
  'errors': '',
  'params': '',
  'request_id': '',
};
final Map<String, dynamic> _queryV2Parameters = {
  'params': '',
};
final Map<String, dynamic> _clickParameters = {
  'params': '',
  'request_id': '',
};

final Map<String, dynamic> _verifyParameters = {
  'params': '',
  'result': '',
  'request_id': '',
};

class AdsApi extends HttpCore {
  /// 获取所有广告信息
  Future<void> getAllAds({
    Map<String, dynamic>? queryParameters,
    Function? resolve,
    Function? reject,
  }) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v2/adopr/query/',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(AdsAllEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 测试信息
  Future<void> getSelect({
    Map<String, dynamic>? queryParameters,
    Function? resolve,
    Function? reject,
  }) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/adopr/select/',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }

  /// 请求广告信息
  Future<void> postAdsDetail(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryV2Parameters);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v2/adopr/request/',
        formData: _,
        resolve: (data) {
          resolve?.call(AdsDetailEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 广告错误
  Future<void> postErrorAds(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_errorParameters);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v2/adopr/error/',
        formData: _,
        resolve: (data) {
          resolve?.call(AdsDetailEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 广告点击
  Future<void> postClickAds(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_clickParameters);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v2/adopr/click/',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data)); //没有返回值
        },
        reject: reject);
  }

  /// 广告曝光
  Future<void> postAdsExposure({
    Map<String, dynamic>? queryParameters,
    Function? resolve,
    Function? reject,
  }) async {
    var _ = Map<String, dynamic>.from(_clickParameters);
    _.addAll(queryParameters!);
    await postCore(
      path: '/api/v2/adopr/exposure/',
      formData: _,
      resolve: (data) {
        resolve?.call(ResultsEntity.fromJson(data)); //没有返回值
      },
      reject: reject,
    );
  }

  /// 广告加载完成
  Future<void> postAdsLoad({
    Map<String, dynamic>? queryParameters,
    Function? resolve,
    Function? reject,
  }) async {
    var _ = Map<String, dynamic>.from(_clickParameters);
    _.addAll(queryParameters!);
    await postCore(
      path: '/api/v2/adopr/show/',
      formData: _,
      resolve: (data) {
        resolve?.call(ResultsEntity.fromJson(data)); //没有返回值
      },
      reject: reject,
    );
  }

  /// 激励广告观看完毕
  Future<void> postAdsVerify({
    Map<String, dynamic>? queryParameters,
    Function? resolve,
    Function? reject,
  }) async {
    var _ = Map<String, dynamic>.from(_verifyParameters);
    _.addAll(queryParameters!);
    await postCore(
      path: '/api/v2/adopr/reward/verify/',
      formData: _,
      resolve: (data) {
        resolve?.call(ResultsEntity.fromJson(data)); //没有返回值
      },
      reject: reject,
    );
  }

  /// 記錄廣告數據
  Future<void> postAdsPoint({
    Map<String, dynamic>? queryParameters,
    Function? resolve,
    Function? reject,
  }) async {
    var _ = Map<String, dynamic>.from(_adsPorint);
    _.addAll(queryParameters!);
    await postCore(
      path: '/api/v2/adopr/point2/',
      formData: _,
      resolve: (data) {
        resolve?.call(ResultsEntity.fromJson(data)); //没有返回值
      },
      reject: reject,
    );
  }
}

final ads = AdsApi();
