import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberComicDeleteEntity extends ResultsEntity {
  MemberComicDeleteEntity(int code, String message) : super(code, message);

  factory MemberComicDeleteEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberComicDeleteEntityFromJson(srcJson);
}

MemberComicDeleteEntity _$MemberComicDeleteEntityFromJson(
    Map<String, dynamic> json) {
  return MemberComicDeleteEntity(
    json['code'],
    json['message'],
  );
}
