import 'package:hotmangasCore/model/model.dart'
    show Banners, CommonListItemPortrait, CommonListPortrait, ResultsEntity;

class PortraitHomeIndexEntity extends ResultsEntity {
  Results? results;

  PortraitHomeIndexEntity(code, message, this.results) : super(code, message);

  factory PortraitHomeIndexEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitHomeIndexEntityFromJson(srcJson);
}

class Results extends Object {
  List<Banners?>? banners;

  List<CommonListItemPortrait?>? newPosts;

  CommonListPortrait? rankWeekPosts;

  CommonListPortrait? rankMonthPosts;

  Results(
    this.banners,
    this.newPosts,
    this.rankWeekPosts,
    this.rankMonthPosts,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

PortraitHomeIndexEntity _$PortraitHomeIndexEntityFromJson(
    Map<String, dynamic> json) {
  return PortraitHomeIndexEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    (json['banners'] as List)
        .map((e) =>
            e == null ? null : Banners.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['newPosts'] as List)
        .map((e) => e == null
            ? null
            : CommonListItemPortrait.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['rankWeekPosts'] == null
        ? null
        : CommonListPortrait.fromJson(
            json['rankWeekPosts'] as Map<String, dynamic>),
    json['rankMonthPosts'] == null
        ? null
        : CommonListPortrait.fromJson(
            json['rankMonthPosts'] as Map<String, dynamic>),
  );
}
