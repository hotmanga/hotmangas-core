class FilterItem extends Object {
  int? initials;

  String? name;

  /// 仅 theme
  String? logo;

  /// 仅 theme
  String? colorH5;

  /// 仅 female/male
  int? gender;

  String? pathWord;

  int? count;

  FilterItem(this.initials, this.name, this.logo, this.colorH5, this.pathWord,
      this.count, this.gender);

  factory FilterItem.fromJson(Map<String, dynamic> srcJson) =>
      $_FilterItemFromJson(srcJson);
}

FilterItem $_FilterItemFromJson(Map<String, dynamic> json) {
  return FilterItem(
    json['initials'] ?? -1,
    json['name'],
    json['logo'],
    json['color_h5'],
    json['path_word'].toString(),
    json['count'] ?? -1,
    json['gender'] ?? -1,
  );
}
