enum RankType {
  Error,
  ComicFree,
  ComicCharge,
  Cartoon,
}

const RankTypeEnumMap = {
  RankType.Error: 0,
  RankType.ComicFree: 1,
  RankType.ComicCharge: 2,
  RankType.Cartoon: 4,
};

enum RankDateType {
  Error,
  RankDay,
  RankWeek,
  RankMonth,
  RankTotal,
}

const RankDateTypeEnumMap = {
  RankDateType.Error: '',
  RankDateType.RankDay: 'day',
  RankDateType.RankWeek: 'week',
  RankDateType.RankMonth: 'month',
  RankDateType.RankTotal: 'total',
};
