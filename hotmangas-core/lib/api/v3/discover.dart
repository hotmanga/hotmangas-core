import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show DiscoverFreeCartoonListEntity, DiscoverListEntity;

final Map<String, dynamic> _queryParameters = {};

class DiscoverIndex extends HttpCore {
  Future<void> _getDiscoverIndex(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/h5/discoverIndex',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }

  Future<void> getFreeComicDiscoverIndex(
      {Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/h5/discoverIndex/freeComic',
        resolve: (data) {
          resolve?.call(DiscoverListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getChargeComicDiscoverIndex(
      {Function? resolve, Function? reject}) async {
    await _getDiscoverIndex(
        queryParameters: {'type': 2},
        resolve: (data) {
          resolve?.call(DiscoverListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getFreeCartoonDiscoverIndex(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    // 排序的 ordering 是固定的。
    _.addAll({'ordering': '-datetime_updated'});
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/made/free/cartoons',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(DiscoverFreeCartoonListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getChargeCartoonDiscoverIndex(
      {Function? resolve, Function? reject}) async {
    await _getDiscoverIndex(
        queryParameters: {'type': 4},
        resolve: (data) {
          resolve?.call(DiscoverListEntity.fromJson(data));
        },
        reject: reject);
  }
}

final discoverIndex = DiscoverIndex();
