import '../model.dart' show ResultsEntity;

class MemberSafetyListEntity extends ResultsEntity {
  List<Results>? results;

  MemberSafetyListEntity(code, message, this.results) : super(code, message);

  factory MemberSafetyListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberSafetyListEntityFromJson(srcJson);
}

MemberSafetyListEntity _$MemberSafetyListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberSafetyListEntity(
    json['code'],
    json['message'],
    (json['results'] as List).map((e) => Results.fromJson(e)).toList(),
  );
}

class Results extends Object {
  String? code;

  Results(
    this.code,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['code'],
  );
}
