import 'package:hotmangasCore/api/utils/export.dart' show datetimeFormat;
import 'package:hotmangasCore/model/core/common/author_or_company.dart'
    show AuthorOrCompany;
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberBookBrowserListEntity extends ResultsEntity {
  Results? results;

  MemberBookBrowserListEntity(code, message, this.results)
      : super(code, message);

  factory MemberBookBrowserListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberBookBrowserListEntityFromJson(srcJson);
}

MemberBookBrowserListEntity _$MemberBookBrowserListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberBookBrowserListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<MemberBookBrowserItem>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    json['list'] == null
        ? []
        : (json['list'] as List)
            .map((e) => MemberBookBrowserItem.fromJson(e))
            .toList(),
  );
}

class MemberBookBrowserItem extends Object {
  /// 当前用户最后浏览的章节 Id
  String? lastChapterId;

  /// 当前用户最后浏览的章节 Name
  String? lastChapterName;

  /// 当前用户最后浏览的轻小说 Entity
  MemberBookBrowserInnerItem? book;

  MemberBookBrowserItem(this.lastChapterId, this.lastChapterName, this.book);

  factory MemberBookBrowserItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberBookBrowserItemFromJson(srcJson);
}

MemberBookBrowserItem _$MemberBookBrowserItemFromJson(
    Map<String, dynamic> json) {
  return MemberBookBrowserItem(
      json['last_chapter_id'],
      json['last_chapter_name'],
      json['book'] == null
          ? null
          : MemberBookBrowserInnerItem.fromJson(json['book']));
}

class MemberBookBrowserInnerItem extends Object {
  /// 轻小说 UUID
  String? uuid;

  /// 轻小说的 b_display
  /// 如果为 false, 则表示该小说已被下架或转移, 清除.
  bool? bDisplay;

  /// 轻小说名称
  String? name;

  String? pathWord;

  List<AuthorOrCompany?>? _author;

  List<AuthorOrCompany?>? get author {
    return _author;
  }

  String? cover;

  int? status;

  int? popular;

  /// 最后更新时间
  DateTime? _datetimeUpdated;

  String? get datetimeUpdated {
    if (_datetimeUpdated == null) return '';
    return datetimeFormat(_datetimeUpdated);
  }

  /// 轻小说最后更新的章节 ID
  String? lastChapterId;

  /// 轻小说最后更新的章节 Name
  String? lastChapterName;

  MemberBookBrowserInnerItem(
      this.uuid,
      this.bDisplay,
      this.name,
      this.pathWord,
      this._author,
      this.cover,
      this.status,
      this.popular,
      this._datetimeUpdated,
      this.lastChapterId,
      this.lastChapterName);

  factory MemberBookBrowserInnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberBookBrowserInnerItemFromJson(srcJson);
}

MemberBookBrowserInnerItem _$MemberBookBrowserInnerItemFromJson(
    Map<String, dynamic> json) {
  return MemberBookBrowserInnerItem(
    json['uuid'],
    json['b_display'],
    json['name'],
    json['path_word'],
    json['author'] == null
        ? null
        : (json['author'] as List)
            .map((e) => AuthorOrCompany.fromJson(e))
            .toList(),
    json['cover'],
    json['status'],
    json['popular'],
    json['datetime_updated'] == null
        ? null
        : DateTime.parse(json['datetime_updated']),
    json['last_chapter_id'],
    json['last_chapter_name'],
  );
}
