import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class AdsAllEntity extends ResultsEntity {
  List<IdentItem?>? results;

  ///需要传输给android的广告列表
  List<Map<String, dynamic>?>? get resMap {
    return results?.map((e) => e?.toMap()).toList();
  }

  AdsAllEntity(code, message, this.results) : super(code, message);

  factory AdsAllEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$AdsAllEntityEntityFromJson(srcJson);
}

AdsAllEntity _$AdsAllEntityEntityFromJson(Map<String, dynamic> json) {
  return AdsAllEntity(
    json['code'],
    json['message'],
    (json['results'] as List)
        .map((e) =>
            e == null ? null : IdentItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

class IdentItem extends Object {
  /// id
  int? id;

  ///广告ident
  String? ident;

  /// 广告名称
  String? name;

  /// 广告展示ID
  String? appId;

  /// 广告类别 1目标价 2竞价
  int? priceType;

  ///目标价 目标价格默认为0
  int? ecpm;

  int? maxEcpm;

  ///渠道ID  2001 2002 2003等
  String? channel;

  /// base64 存日志时需要传给后台
  String? requestParams;

  /// 广告分成
  double? actualRate;

  IdentItem(
    this.id,
    this.ident,
    this.name,
    this.appId,
    this.priceType,
    this.ecpm,
    this.maxEcpm,
    this.channel,
    this.requestParams,
    this.actualRate,
  );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'ident': ident,
      'name': name,
      'appId': appId,
      'priceType': priceType,
      'ecpm': ecpm,
      'maxEcpm': maxEcpm,
      'channel': channel,
      'requestParams': requestParams,
      'actualRate': actualRate,
    };
  }

  factory IdentItem.fromJson(Map<String, dynamic> srcJson) =>
      _$IdentItemFromJson(srcJson);
}

IdentItem _$IdentItemFromJson(Map<String, dynamic> json) {
  return IdentItem(
    json['id'],
    json['ident'],
    json['name'],
    json['app_id'],
    json['price_type'],
    json['ecpm'],
    json['max_ecpm'],
    json['channel'],
    json['request_params'],
    json['actual_rate'],
  );
}
