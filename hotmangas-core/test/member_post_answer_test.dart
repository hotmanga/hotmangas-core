// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/member/member.dart';
import 'package:hotmangasCore/model/member/member_forget_answer_step1_entity.dart';

void main() {
  test('测试修改密码 Step1', () async {
    final userName = 'wxceshi047';

    MemberForgetAnswerEntity? response;
    await member.safety.postAnswerStep1(
        userName: userName,
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.token, isNotNull);
    expect(response?.results?.userName, equals(userName));
    expect(response?.results?.answer, isNotNull);
    expect(response?.results?.answer, isA<Answer>());
    expect(response?.results?.answer?.id, isNotNull);
    expect(response?.results?.answer?.question, isNotNull);
  });

  test('测试修改密码 Step2', () async {
    final userName = 'wxceshi047';

    MemberForgetAnswerEntity? response;
    await member.safety.postAnswerStep1(
        userName: userName,
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    var _token = response?.results?.token;
    var _id = response?.results?.answer?.id;
    var _answer = '123';

    await member.safety.postAnswerStep2(
        answerId: _id!,
        token: _token!,
        answer: _answer,
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.token, isNotNull);
    expect(response?.results?.userName, equals(userName));
    expect(response?.results?.answer, isNull);
  });

  test('测试修改密码 Step3', () async {
    final userName = 'wxceshi047';

    MemberForgetAnswerEntity? response;
    await member.safety.postAnswerStep1(
        userName: userName,
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    var _token = response?.results?.token;
    var _id = response?.results?.answer?.id;
    var _answer = '123';

    await member.safety.postAnswerStep2(
        answerId: _id!,
        token: _token!,
        answer: _answer,
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    _token = response?.results?.token;
    var _password = '111111';
    await member.safety.postAnswerStep3(
        token: _token!,
        password: _password,
        resolve: (data) {
          // response = data;
          print(data);
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.isSuccess, isTrue);
  });
}
