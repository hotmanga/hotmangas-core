import '../model.dart' show ResultsEntity;
import 'member_security_answer_item.dart' show SecurityAnswerItem;

class MemberSecurityAnswerEntity extends ResultsEntity {
  SecurityAnswerItem? results;

  MemberSecurityAnswerEntity(code, message, this.results)
      : super(code, message);

  factory MemberSecurityAnswerEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberSecutiryAnswerEntityFromJson(srcJson);
}

MemberSecurityAnswerEntity _$MemberSecutiryAnswerEntityFromJson(
    Map<String, dynamic> json) {
  return MemberSecurityAnswerEntity(
    json['code'],
    json['message'],
    SecurityAnswerItem.fromJson(json['results']),
  );
}
