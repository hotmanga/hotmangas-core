enum ImageRegionType { None, Mainland, Overseas }

const ImageRegionTypeEnumMap = {
  ImageRegionType.None: 0,
  ImageRegionType.Mainland: 1,
  ImageRegionType.Overseas: 0,
};

const ImageRegionTypeEnumMapString = {
  ImageRegionType.None: '港台及海外',
  ImageRegionType.Mainland: '大陆用户',
  ImageRegionType.Overseas: '港台及海外'
};
