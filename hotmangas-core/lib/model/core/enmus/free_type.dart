enum FreeType { None, Free, Charge, All }

const FreeTypeEnumMap = {
  FreeType.None: 0,
  FreeType.Free: 1,
  FreeType.Charge: 2,

  /// 这个并不是所有接口都会用到的，需要慎用。比如删除就可以传该值。
  FreeType.All: 0,
};

const FreeTypeEnumMapString = {
  FreeType.None: '错误',
  FreeType.Free: '免费',
  FreeType.Charge: '付费'
};
