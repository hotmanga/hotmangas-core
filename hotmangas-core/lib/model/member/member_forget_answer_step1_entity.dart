import '../model.dart' show ResultsEntity;

class MemberForgetAnswerEntity extends ResultsEntity {
  ForgetAnswerStepResults? results;

  MemberForgetAnswerEntity(code, message, this.results) : super(code, message);

  factory MemberForgetAnswerEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberForgetAnswerFromJson(srcJson);
}

MemberForgetAnswerEntity _$MemberForgetAnswerFromJson(
    Map<String, dynamic> json) {
  return MemberForgetAnswerEntity(
    json['code'],
    json['message'],
    ForgetAnswerStepResults.fromJson(json['results']),
  );
}

class ForgetAnswerStepResults extends Object {
  String? token;

  String? userName;

  // 不一定有的，比如Step2就没有answer
  Answer? answer;

  ForgetAnswerStepResults(this.token, this.userName, this.answer);

  factory ForgetAnswerStepResults.fromJson(Map<String, dynamic> srcJson) =>
      _$ForgetAnswerStepResultsFromJson(srcJson);
}

ForgetAnswerStepResults _$ForgetAnswerStepResultsFromJson(
    Map<String, dynamic> json) {
  return ForgetAnswerStepResults(
    json['token'],
    json['username'],
    json['answer'] == null ? null : Answer.fromJson(json['answer']),
  );
}

class Answer extends Object {
  int? id;

  String? question;

  Answer(this.id, this.question);

  factory Answer.fromJson(Map<String, dynamic> srcJson) =>
      _$AnswerFromJson(srcJson);
}

Answer _$AnswerFromJson(Map<String, dynamic> json) {
  return Answer(
    json['id'],
    json['question'],
  );
}
