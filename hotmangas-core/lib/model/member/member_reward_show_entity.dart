import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;
import 'package:hotmangasCore/model/member/member_reward_show.dart'
    show MemberRewardShow;

class MemberRewardShowEntity extends ResultsEntity {
  MemberRewardShow? results;

  MemberRewardShowEntity(code, message, this.results) : super(code, message);

  factory MemberRewardShowEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberRewardEntityFromJson(srcJson);
}

MemberRewardShowEntity _$MemberRewardEntityFromJson(Map<String, dynamic> json) {
  return MemberRewardShowEntity(json['code'], json['message'],
      MemberRewardShow.fromJson(json['results']));
}
