class ChapterContent extends Object {
  String? uuid;

  String? url;

  ChapterContent(this.uuid, this.url);

  factory ChapterContent.fromJson(Map<String, dynamic> srcJson) =>
      _$ChapterContentFromJson(srcJson);
}

ChapterContent _$ChapterContentFromJson(Map<String, dynamic> json) {
  return ChapterContent(
    json['uuid'],
    json['url'] ?? '',
  );
}
