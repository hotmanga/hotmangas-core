import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show CountListEntity, FreeType, FreeTypeEnumMap;
import 'package:meta/meta.dart' show required;

/// 默认 queryParameters 包含 limit 和 offset.
final Map<String, dynamic> _queryParameters = {
  'limit': 21,
  'offset': 0,
};

class Counts extends Object {
  final CountsComic comic = CountsComic();
  final CountsCartoon cartoon = CountsCartoon();
  final CountsBook book = CountsBook();
  final CountsPortrait portrait = CountsPortrait();
}

final counts = Counts();

/// 获取所有漫画下的独立内容，如题材，作者，男性向标签，女性向标签
class CountsComic extends HttpCore {
  /// /api/v3/theme/comic/count?free_type=1&limit=1&offset=0
  Future<void> getThemeList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/theme/comic/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// /api/v3/parody/comic/count?free_type=1&limit=1&offset=0
  Future<void> getParodyList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/parody/comic/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// /api/v3/author/comic/count?free_type=1&limit=1&offset=0
  Future<void> getAuthorList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/author/comic/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getFemaleList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/female/comic/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getMaleList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/male/comic/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }
}

/// 获取所有动画下的独立内容，如动画下所有公司，年份，女性向标签，男性向标签
class CountsCartoon extends HttpCore {
  Future<void> getThemeList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/theme/cartoon/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getCompanyList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/company/cartoon/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getYearList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/year/cartoon/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getFemaleList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/female/cartoon/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getMaleList(
      {@required FreeType? freeType,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/male/cartoon/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }
}

/// 获取所有轻小说下的独立内容, 如轻小说下的所有主题. 目前只有主题
class CountsBook extends HttpCore {
  /// /api/v3/theme/comic/count?free_type=1&limit=1&offset=0
  Future<void> getThemeList(
      {FreeType? freeType = FreeType.Free,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    _.addAll({'free_type': FreeTypeEnumMap[freeType]});
    await getCore(
        path: '/api/v3/theme/book/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }
}

/// 获取所有写真集下的独立内容, 如写真集下的所有标签. 目前只有标签
class CountsPortrait extends HttpCore {
  /// /api/v3/theme/comic/count?free_type=1&limit=1&offset=0
  Future<void> getPortraitList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/tag/post/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CountListEntity.fromJson(data));
        },
        reject: reject);
  }
}
