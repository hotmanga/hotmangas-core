
/// 线路播放的选择
enum PlayLineType{
  Line1,
  Line2,
  Line3
}

const PlayLineTypeEnumMap = {
  PlayLineType.Line1: 'line1',
  PlayLineType.Line2: 'line2',
  PlayLineType.Line3: 'line3',
};