import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';

void main() {
  test('用户在某个漫画下发表评论', () async {
    final userName = 'kbceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ResultsEntity? response;
    await member.comments.createComment(
        queryParameters: {
          'comic_id': 'b2da35fc-9ed5-11e8-adcb-00163e0ca5bd',
          'comment':
              'When Chuck Norris visits his ranch, he always makes sure to take a ride on Sherman, his Cape Buffalo.',
          'reply_id': ''
        },
        resolve: (data) {
          response = data;
        },
        reject: (HotmangasError error) {
          print(error.toString());
        });

    expect(response, isA<ResultsEntity>());
    expect(response?.code, equals(200));
  });

  test('获取某个用户下所有评论', () async {
    final userName = 'kbceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberCommentCollectListEntity? response;
    await member.comments.getCommentCollectList(
        queryParameters: {'comic_id': 'b2da35fc-9ed5-11e8-adcb-00163e0ca5bd'},
        resolve: (data) {
          response = data;
        },
        reject: (HotmangasError error) {
          print(error);
        });

    expect(response, isA<MemberCommentCollectListEntity>());
    expect(response?.code, equals(200));
    if (response!.results!.list!.isNotEmpty) {
      response?.results?.list?.forEach((element) {
        print(element.id);
      });
    }
  });

  test('删除某用户的评论', () async {
    final userName = 'kbceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ResultsEntity? response;
    await member.comments.delComment(
        queryParameters: {
          'comic_id': 'b2da35fc-9ed5-11e8-adcb-00163e0ca5bd', //漫画ID
          'comment_id': 4398, //评论ID
        },
        resolve: (data) {
          response = data;
        },
        reject: (HotmangasError error) {
          print(error);
        });

    expect(response, isA<ResultsEntity>());
    expect(response?.code, equals(200));
    expect(response?.isSuccess, isTrue);
  });

  test('用户在某个轻小说下发表评论', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ResultsEntity? response;
    await member.comments.createNovelComment(
        queryParameters: {
          'book_id': 'b8a1967a-6bd5-11ed-acd3-024352452ce0',
          'comment': '测试是否成功发布',
          'reply_id': ''
        },
        resolve: (data) {
          response = data;
        },
        reject: (HotmangasError error) {
          print(error.toString());
        });

    expect(response, isA<ResultsEntity>());
    expect(response?.code, equals(200));
  });

  test('获取某个轻小说下所有评论', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberCommentCollectListEntity? response;
    await member.comments.getNovelCommentCollectList(
        queryParameters: {
          'book_id': 'b8a1967a-6bd5-11ed-acd3-024352452ce0',
        },
        resolve: (data) {
          response = data;
        },
        reject: (HotmangasError error) {
          print(error);
        });

    expect(response, isA<MemberCommentCollectListEntity>());
    expect(response?.code, equals(200));
    if (response!.results!.list!.isNotEmpty) {
      response?.results?.list?.forEach((element) {
        print('id：${element.id}--------comment：${element.comment}');
      });
    }
  });
  test('删除某用户的评论', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ResultsEntity? response;
    await member.comments.delNovelComment(
        queryParameters: {
          'book_id': 'b8a1967a-6bd5-11ed-acd3-024352452ce0', //漫画ID
          'comment_id': 1, //评论ID
        },
        resolve: (data) {
          response = data;
        },
        reject: (HotmangasError error) {
          print(error);
        });

    expect(response, isA<ResultsEntity>());
    expect(response?.code, equals(200));
    expect(response?.isSuccess, isTrue);
  });
}
