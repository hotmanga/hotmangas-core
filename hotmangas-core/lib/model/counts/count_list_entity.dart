import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;
import 'package:hotmangasCore/model/counts/count_item.dart' show CountItem;

class CountListEntity extends ResultsEntity {
  Results? results;

  CountListEntity(code, message, this.results) : super(code, message);

  factory CountListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$CountListEntityFromJson(srcJson);
}

CountListEntity _$CountListEntityFromJson(Map<String, dynamic> json) {
  return CountListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<CountItem?>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    json['list'] == null
        ? null
        : (json['list'] as List).map((e) => CountItem.fromJson(e)).toList(),
  );
}
