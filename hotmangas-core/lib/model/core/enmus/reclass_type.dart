
enum ReclassType{
  None,
  Manga,
  Doujinshi,
}

const ReclassTypeEnumMap = {
  ReclassType.None: 0,
  ReclassType.Manga: 1,
  ReclassType.Doujinshi: 2,
};