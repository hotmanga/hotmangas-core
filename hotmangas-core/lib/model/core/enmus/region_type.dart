enum RegionType {
  /// 日本
  Japan,

  /// 韩国
  Korea,

  /// 欧美
  West,

  /// 港台
  HK,

  /// 内地
  Inland,

  /// 其他
  Other,
}

const RegionTypeEnumMap = {
  RegionType.Japan: 0,
  RegionType.Korea: 1,
  RegionType.West: 2,
  RegionType.HK: 3,
  RegionType.Inland: 4,
  RegionType.Other: 5
};
