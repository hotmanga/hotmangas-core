// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';

void main() {
  test('测试书架，获取漫画', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberCollectCartoonListEntity? entity;
    await member.browser.getCartoonList(
        queryParameters: {'free_type': 1, 'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });

    expect(entity, isA<MemberCollectCartoonListEntity>());
    expect(entity?.code, equals(200));
    expect(entity?.results?.list, isList);
    expect(entity?.results?.limit, equals(1));
    expect(entity?.results?.offset, equals(0));
  });
}
