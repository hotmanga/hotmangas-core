import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        ContentType,
        ContentTypeEnumMap,
        MemberBookCollectListEntity,
        MemberCollectCartoonListEntity,
        MemberCollectComicListEntity,
        MemberCollectPortraitListEntity,
        ResultsEntity;
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'limit': 21,
  'offset': 0,
};
final Map<String, dynamic> _queryDelParameters = {
  'ids': [],
};

class Collect extends HttpCore {
  Future<void> getComicList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/collect/comics',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberCollectComicListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> delComicList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryDelParameters);
    _.addAll(queryParameters!);
    await deleteCore(
        path: '/api/v3/member/collect/comics',
        formData: _,
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }

  Future<void> getCartoonList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/collect/cartoons',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberCollectCartoonListEntity.fromJson(data));
        },
        reject: reject);
  }

  ///获取写真书架列表
  Future<void> getPortraitList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/collect/posts',
        queryParameters: _,
        resolve: (data) {
          // resolve?.call(data);
          resolve?.call(MemberCollectPortraitListEntity.fromJson(data));
        },
        reject: reject);
  }

  ///删除书架里的写真
  Future<void> delPortraitList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryDelParameters);
    _.addAll(queryParameters!);
    await deleteCore(
        path: '/api/v3/member/collect/posts',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 收藏或者取消漫画
  Future<void> collectComicByUid(
      {@required String? uid,
      @required bool? collect,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/collect/comic',
        formData: {'comic_id': uid, 'is_collect': collect! ? 1 : 0},
        queryParameters: null,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 收藏或者取消动画
  Future<void> collectCartoonByUid(
      {@required String? uid,
      @required bool? collect,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/collect/cartoon',
        formData: {'cartoon_id': uid, 'is_collect': collect! ? 1 : 0},
        queryParameters: null,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 收藏或者取消写真
  Future<void> collectPortraitByUid(
      {@required String? uid,
      @required bool? collect,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/collect/post',
        formData: {'post_id': uid, 'is_collect': collect! ? 1 : 0},
        queryParameters: null,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 当收藏的内容 bDisplay 为 false 且用户想查阅时, 需要删除该内容.
  Future<void> deleteNotDisplayCollect(
      {@required String? uuid,
      @required ContentType? c_type,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/collects/delete',
        formData: {'uuids': uuid, 'c_type': ContentTypeEnumMap[c_type]},
        queryParameters: null,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取当前用户的轻小说收藏列表
  Future<void> getBookList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/collect/books',
        queryParameters: _,
        resolve: (data) {
          // resolve?.call(data);
          resolve?.call(MemberBookCollectListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 当前用户通过轻小说 ID 来设置收藏或者取消收藏该轻小说
  /// uid, 轻小说 ID
  /// collect, 是否收藏, true 为收藏, false 为取消收藏
  Future<void> collectBookById(
      {@required String? uid,
      @required bool? collect,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/collect/book',
        formData: {'book_id': uid, 'is_collect': collect! ? 1 : 0},
        queryParameters: null,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 删除当前用户的轻小说收藏列表
  /// queryParameters, Map 内参数名为 'ids', 类型为字符串数组.
  Future<void> delBookList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryDelParameters);
    _.addAll(queryParameters!);
    await deleteCore(
        path: '/api/v3/member/collect/books',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }
}
