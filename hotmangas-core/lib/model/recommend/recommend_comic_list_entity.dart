import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/common/inner_item.dart' show InnerItem;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class RecommendComicListEntity extends ResultsEntity {
  Results? results;

  RecommendComicListEntity(code, message, this.results) : super(code, message);

  factory RecommendComicListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$RecommendComicListEntity(srcJson);
}

RecommendComicListEntity _$RecommendComicListEntity(Map<String, dynamic> json) {
  return RecommendComicListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<RecommendComicListItem?>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) => _$Results(srcJson);
}

Results _$Results(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List)
        .map((e) => RecommendComicListItem.fromJson(e))
        .toList(),
  );
}

class RecommendComicListItem extends Object {
  int? type;

  InnerItem? comic;

  RecommendComicListItem(this.type, this.comic);

  factory RecommendComicListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$RecommendComicListItem(srcJson);
}

RecommendComicListItem _$RecommendComicListItem(Map<String, dynamic> json) {
  return RecommendComicListItem(
    json['type'],
    InnerItem.fromJson(json['comic']),
  );
}
