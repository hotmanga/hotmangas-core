import './lines.dart' show Lines;
import '../core/common/common_list_results.dart' show CommonListResults;
import '../core/enmus/content_type.dart' show ContentTypeEnumMap;
import '../core/enmus/decode.dart' show enumDecodeNullable;
import '../core/results_entity.dart' show ResultsEntity;

class CartoonListEntity extends ResultsEntity {
  Results? results;

  CartoonListEntity(
    code,
    message,
    this.results,
  ) : super(code, message);

  factory CartoonListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$CartoonListEntityFromJson(srcJson);
}

CartoonListEntity _$CartoonListEntityFromJson(Map<String, dynamic> json) {
  return CartoonListEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends CommonListResults {
  List<CartoonList?>? list;

  Results(this.list, total, limit, offset, type)
      : super(total, limit, offset, type);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    (json['list'] as List)
        .map((e) =>
            e == null ? null : CartoonList.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['total'],
    json['limit'],
    json['offset'],
    enumDecodeNullable(ContentTypeEnumMap, json['type']),
  );
}

class CartoonList extends Object {
  String? name;

  String? uuid;

  /// 视频的截图, H5 会用来展示视频列表. 有可能会为空.
  String? vCover;

  Lines? lines;

  CartoonList(
    this.name,
    this.uuid,
    this.vCover,
    this.lines,
  );

  factory CartoonList.fromJson(Map<String, dynamic> srcJson) =>
      _$CartoonListFromJson(srcJson);
}

CartoonList _$CartoonListFromJson(Map<String, dynamic> json) {
  return CartoonList(
    json['name'],
    json['uuid'],
    json['v_cover'] ?? '',
    json['lines'] == null
        ? null
        : Lines.fromJson(json['lines'] as Map<String, dynamic>),
  );
}
