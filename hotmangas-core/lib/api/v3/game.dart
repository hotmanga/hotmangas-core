import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show GameEntity;

final Map<String, dynamic> _queryParameters = {
  'limit': 20,
  'offset': 0,
};

class Game extends HttpCore {
  Future<void> getList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/games',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(GameEntity.fromJson(data));
        },
        reject: reject);
  }
}

final game = Game();
