// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show comic;
import 'package:hotmangasCore/model/cartoon/cartoon_detail_entity.dart';
import 'package:hotmangasCore/model/comic/comic_detail_entity.dart';
import 'package:test/test.dart';

void main() async {
  ComicDetailEntity? response;
  await comic.getDetail(
      pathWord: 'mowuniang',
      resolve: (ComicDetailEntity _response) {
        response = _response;
      });

  test('匿名访问漫画详情页 接口基本信息', () {
    expect(response, isA<ComicDetailEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.isLock, equals(false));
    expect(response?.results?.isLogin, equals(false));
    expect(response?.results?.isMobileBind, equals(false));
    expect(response?.results?.isVip, equals(false));
  });

  test('匿名访问漫画详情页 基本信息', () async {
    ComicDetailEntity? response;
    await comic.getDetail(
        pathWord: 'mowuniang',
        resolve: (ComicDetailEntity _response) {
          response = _response;
        });

    expect(response?.results?.isLock, equals(false));
    expect(response?.results?.isLogin, equals(false));
    expect(response?.results?.isMobileBind, equals(false));
    expect(response?.results?.isVip, equals(false));
    expect(response?.results?.comic?.datetimeUpdated, isNotNull);
    expect(response?.results?.comic?.datetimeUpdated, isA<String>());
    expect(response?.results?.popular, isA<int>());
  });

  test('匿名访问漫画详情页 分组信息', () {
    expect(response?.results?.groups?.list.first.groupName, equals('default'));
    expect(response?.results?.groups?.list.first.pathWord, equals('default'));
    expect(response?.results?.groups?.names.first, equals('default'));
  });

  test('匿名访问漫画详情页 漫画信息', () {
    var comic = response?.results?.comic;
    expect(comic, isA<ComicDetailItem>());
    expect(comic?.pathWord, equals('mowuniang'));
    expect(comic?.datetimeUpdated, isNotNull);
    expect(comic?.lastChapter, isNotNull);
    expect(comic?.lastChapter, isA<LastChapter>());
  });
}
