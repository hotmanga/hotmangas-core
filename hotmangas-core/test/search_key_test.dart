// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';

void main() async {
  SearchHotKeyListEntity? entity;
  await search.searchHotKey(
      queryParameters: {'limit': 1, 'offset': 0},
      resolve: (SearchHotKeyListEntity data) {
        entity = data;
      },
      reject: (error) {
        print(error);
      });

  test('查询搜索热词 接口基本信息', () {
    expect(entity!.code, equals(200));
    expect(entity!.results.limit, equals(1));
    expect(entity!.results.offset, equals(0));
    expect(entity!.results.list, isList);
    expect(entity!.results.list, isNotEmpty);
    expect(entity!.results.list.first, isNotNull);
    expect(entity!.results.list.first, isA<SearchHotKeyListItem>());
  });
}
