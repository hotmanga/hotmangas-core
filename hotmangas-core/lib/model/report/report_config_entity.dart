import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class ReportConfigListEntity extends ResultsEntity {
  ReportConfigItem? results;

  ReportConfigListEntity(code, message, this.results) : super(code, message);

  factory ReportConfigListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberComicCollectEntityFromJson(srcJson);
}

ReportConfigListEntity _$MemberComicCollectEntityFromJson(
    Map<String, dynamic> json) {
  return ReportConfigListEntity(
    json['code'],
    json['message'],
    ReportConfigItem.fromJson(json['results']),
  );
}

class ReportConfigItem extends Object {
  List<ReportTypeItem> reportType;
  String reportTip;

  ReportConfigItem(
    this.reportType,
    this.reportTip,
  );

  factory ReportConfigItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberComicCollectedItemFromJson(srcJson);
}

ReportConfigItem _$MemberComicCollectedItemFromJson(Map<String, dynamic> json) {
  return ReportConfigItem(
    (json['report_type'] as List)
        .map((e) => ReportTypeItem.fromJson(e))
        .toList(),
    json['report_tip'] ?? '',
  );
}

class ReportTypeItem extends Object {
  String? name;

  /// 举报类型
  int? type;

  ReportTypeItem(this.name, this.type);

  factory ReportTypeItem.fromJson(Map<String, dynamic> srcJson) =>
      _$ChapterContentFromJson(srcJson);
}

ReportTypeItem _$ChapterContentFromJson(Map<String, dynamic> json) {
  return ReportTypeItem(
    json['name'],
    json['type'],
  );
}
