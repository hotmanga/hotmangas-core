import 'package:hotmangasCore/model/core/common/reward_list_entity.dart';
import 'package:hotmangasCore/model/model.dart'
    show TagOrThemeItem, AuthorOrCompany;

import '../core/results_entity.dart' show ResultsEntity;

class PortraitDetailEntity extends ResultsEntity {
  Results? results;

  PortraitDetailEntity(code, message, this.results) : super(code, message);

  factory PortraitDetailEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitDetailEntityFromJson(srcJson);
}

PortraitDetailEntity _$PortraitDetailEntityFromJson(Map<String, dynamic> json) {
  return PortraitDetailEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends Object {
  bool? isLock;

  bool? isLogin;

  bool? isMobileBind;

  bool? isVip;

  PortraitDetailItem? post;

  int? popular;

  Results(this.isLock, this.isLogin, this.isMobileBind, this.isVip, this.post,
      this.popular);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['is_lock'],
      json['is_login'],
      json['is_mobile_bind'],
      json['is_vip'],
      json['post'] == null
          ? null
          : PortraitDetailItem.fromJson(json['post'] as Map<String, dynamic>),
      json['popular']);
}

class PortraitDetailItem extends Object {
  String? uuid;

  String? name;

  String? cover;

  String? ticket;

  List<AuthorOrCompany?>? _mannequins;

  List<TagOrThemeItem?>? tags;

  String? brief;

  String? datetimeUpdated;

  int? popular;

  List<AuthorOrCompany?>? get mannequins {
    return _mannequins;
  }

  /// 模特所对应的打赏用户列表
  Map<String?, List<RewardList?>?>? get mannequinsRewardMap {
    var _rewardMap = <String?, List<RewardList?>?>{};
    for (var i = 0; i < mannequins!.length; i++) {
      _rewardMap.putIfAbsent(mannequins![i]!.pathWord, () {
        return mannequins![i]!.rewardList;
      });
    }
    return _rewardMap;
  }

  /// 1是免費 2是付費
  int? freeType;

  /// 试看多少页
  int? readTimes;

  PortraitDetailItem(
    this.uuid,
    this.name,
    this.cover,
    this.ticket,
    this._mannequins,
    this.tags,
    this.brief,
    this.datetimeUpdated,
    this.popular,
    this.freeType,
    this.readTimes,
  );

  factory PortraitDetailItem.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitDetailItemFromJson(srcJson);
}

PortraitDetailItem _$PortraitDetailItemFromJson(Map<String, dynamic> json) {
  return PortraitDetailItem(
    json['uuid'] ?? '0',
    json['name'] ?? '',
    json['cover'] ?? '',
    json['ticket'] ?? '',
    (json['mannequins'] as List)
        .map((e) => e == null
            ? null
            : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['tags'] as List)
        .map((e) => e == null
            ? null
            : TagOrThemeItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['brief'] ?? '',
    json['datetime_updated'] ?? '',
    json['popular'] ?? '',
    json['free_type'],
    json['read_times'] ?? 0,
  );
}
