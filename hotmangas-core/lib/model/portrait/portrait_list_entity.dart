import 'package:hotmangasCore/model/core/common/author_or_company.dart'
    show AuthorOrCompany;
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class PortraitListEntity extends ResultsEntity {
  Results results;

  PortraitListEntity(code, message, this.results) : super(code, message);

  factory PortraitListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitListEntityEntityFromJson(srcJson);
}

PortraitListEntity _$PortraitListEntityEntityFromJson(
    Map<String, dynamic> json) {
  return PortraitListEntity(
      json['code'], json['message'], Results.fromJson(json['results']));
}

class Results extends CommonListResults {
  List<PortraitListItem> list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['total'],
      json['limit'],
      json['offset'],
      (json['list'] as List)
          .map((e) => PortraitListItem.fromJson(e as Map<String, dynamic>))
          .toList());
}

class PortraitListItem extends Object {
  String? uuid;

  String? name;

  String? cover;
  List<AuthorOrCompany>? _mannequins;

  List<AuthorOrCompany?>? get mannequins {
    return _mannequins;
  }

  int? popular;

  String? datetimeUpdated;

  ///发现页写真币显示数量
  String? ticket;

  PortraitListItem(this.uuid, this.name, this.cover, this._mannequins,
      this.popular, this.datetimeUpdated, this.ticket);

  factory PortraitListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitListItemItemFromJson(srcJson);
}

PortraitListItem _$PortraitListItemItemFromJson(Map<String, dynamic> json) {
  return PortraitListItem(
    json['uuid'] ?? '0',
    json['name'] ?? '',
    json['cover'] ?? '',
    (json['mannequins'] as List)
        .map((e) => AuthorOrCompany.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['popular'] ?? 0,
    json['datetime_updated'] ?? '',
    json['ticket'] ?? '',
  );
}
