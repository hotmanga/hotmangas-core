import 'package:hotmangasCore/model/core/enmus/content_type.dart'
    show ContentTypeEnumMap;
import 'package:hotmangasCore/model/core/enmus/decode.dart'
    show enumDecodeNullable;

import '../core/common/banner.dart' show Banners;
import '../core/common/common_list_results.dart' show CommonListResults;
import '../core/common/home_index_common_list_comic.dart' show CommonListComic;
import '../core/common/inner_item.dart' show InnerItem;
import '../core/results_entity.dart' show ResultsEntity;

class DiscoverListEntity extends ResultsEntity {
  Results? results;

  DiscoverListEntity(code, message, this.results) : super(code, message);

  factory DiscoverListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$DiscoverListEntityFromJson(srcJson);
}

class Results extends Object {
  List<DiscoverCommonListComic?>? list;

  List<Banners?>? banners;

  CommonListComic? recs;

  Results(
    this.list,
    this.banners,
    this.recs,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

class DiscoverCommonListComic extends CommonListResults {
  String? name;

  String? pathWord;

  List<InnerItem?>? list;

  DiscoverCommonListComic(
    this.name,
    this.pathWord,
    type,
    this.list,
    total,
    limit,
    offset,
  ) : super(
          total,
          limit,
          offset,
          type,
        );

  factory DiscoverCommonListComic.fromJson(Map<String, dynamic> srcJson) =>
      _$DiscoverCommonListComicFromJson(srcJson);
}

DiscoverListEntity _$DiscoverListEntityFromJson(Map<String, dynamic> json) {
  return DiscoverListEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    (json['list'] as List)
        .map((e) => e == null
            ? null
            : DiscoverCommonListComic.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['banners'] as List)
        .map((e) =>
            e == null ? null : Banners.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['recs'] == null
        ? null
        : CommonListComic.fromJson(json['recs'] as Map<String, dynamic>),
  );
}

DiscoverCommonListComic _$DiscoverCommonListComicFromJson(
    Map<String, dynamic> json) {
  return DiscoverCommonListComic(
    json['name'],
    json['path_word'],
    enumDecodeNullable(ContentTypeEnumMap, json['type']),
    (json['list'] as List)
        .map((e) =>
            e == null ? null : InnerItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['total'],
    json['limit'],
    json['offset'],
  );
}
