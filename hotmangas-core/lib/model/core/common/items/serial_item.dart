class SerialItem extends Object {
  String? name;

  String? pathWord;

  String? alias;

  SerialItem(this.name, this.pathWord, this.alias);

  factory SerialItem.fromJson(Map<String, dynamic> srcJson) =>
      _$SerialItemFromJson(srcJson);
}

SerialItem _$SerialItemFromJson(Map<String, dynamic> json) {
  return SerialItem(
    json['name'],
    json['path_word'],
    json['alias'],
  );
}
