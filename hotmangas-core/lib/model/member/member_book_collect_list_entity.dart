import 'package:hotmangasCore/api/utils/export.dart' show datetimeFormat;
import 'package:hotmangasCore/model/core/common/author_or_company.dart';
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberBookCollectListEntity extends ResultsEntity {
  Results? results;

  MemberBookCollectListEntity(code, message, this.results)
      : super(code, message);

  factory MemberBookCollectListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberBookCollectListEntityFromJson(srcJson);
}

MemberBookCollectListEntity _$MemberBookCollectListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberBookCollectListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<MemberCollectBookItem?>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['total'],
      json['limit'],
      json['offset'],
      json['list'] == null
          ? null
          : (json['list'] as List)
              .map((e) => MemberCollectBookItem.fromJson(e))
              .toList());
}

class MemberCollectBookItem extends Object {
  int? uuid;

  MemberLastBrowseInnerItem? lastBrowse;

  MemberBookInnerItem? book;

  MemberCollectBookItem(this.uuid, this.lastBrowse, this.book);

  factory MemberCollectBookItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberCollectBookItemFromJson(srcJson);
}

MemberCollectBookItem _$MemberCollectBookItemFromJson(
    Map<String, dynamic> json) {
  return MemberCollectBookItem(
      json['uuid'],
      json['last_browse'] == null
          ? null
          : MemberLastBrowseInnerItem.fromJson(json['last_browse']),
      json['book'] == null ? null : MemberBookInnerItem.fromJson(json['book']));
}

class MemberLastBrowseInnerItem extends Object {
  String? lastBrowseId;

  String? lastBrowseName;

  MemberLastBrowseInnerItem(this.lastBrowseId, this.lastBrowseName);

  factory MemberLastBrowseInnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberLastBrowseInnerItemFromJson(srcJson);
}

MemberLastBrowseInnerItem _$MemberLastBrowseInnerItemFromJson(
    Map<String, dynamic> json) {
  return MemberLastBrowseInnerItem(
      json['last_browse_id'], json['last_browse_name']);
}

class MemberBookInnerItem extends Object {
  String? uuid;

  bool? bDisplay;

  String? name;

  String? pathWord;

  List<AuthorOrCompany?>? _author;

  List<AuthorOrCompany?>? get author {
    return _author;
  }

  String? cover;

  int? status;

  int? popular;

  DateTime? _datetimeUpdated;

  String? get datetimeUpdated {
    if (_datetimeUpdated == null) return '';

    return datetimeFormat(_datetimeUpdated);
  }

  String? lastChapterId;

  String? lastChapterName;

  MemberBookInnerItem(
    this.uuid,
    this.bDisplay,
    this.name,
    this.pathWord,
    this._author,
    this.cover,
    this.status,
    this.popular,
    this._datetimeUpdated,
    this.lastChapterId,
    this.lastChapterName,
  );

  factory MemberBookInnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberBookInnerItemFromJson(srcJson);
}

MemberBookInnerItem _$MemberBookInnerItemFromJson(Map<String, dynamic> json) {
  return MemberBookInnerItem(
      json['uuid'],
      json['b_display'],
      json['name'],
      json['path_word'],
      json['author'] == null
          ? null
          : (json['author'] as List)
              .map((e) => e == null ? null : AuthorOrCompany.fromJson(e))
              .toList(),
      json['cover'],
      json['status'],
      json['popular'],
      json['datetime_updated'] == null
          ? null
          : DateTime.parse(json['datetime_updated']),
      json['last_chapter_id'],
      json['last_chapter_name']);
}
