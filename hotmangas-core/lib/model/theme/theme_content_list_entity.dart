import '../core/common/common_list_results.dart' show CommonListResults;
import '../core/common/inner_item.dart' show InnerItem;
import '../core/results_entity.dart' show ResultsEntity;

class ThemeContentListEntity extends ResultsEntity {
  Results? results;

  ThemeContentListEntity(
    code,
    message,
    this.results,
  ) : super(code, message);

  factory ThemeContentListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$ThemeContentListEntityFromJson(srcJson);
}

class Results extends CommonListResults {
  List<InnerItem?>? list;

  Results(this.list, total, limit, offset, type)
      : super(total, limit, offset, type);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

ThemeContentListEntity _$ThemeContentListEntityFromJson(
    Map<String, dynamic> json) {
  return ThemeContentListEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    (json['list'] as List)
        .map((e) =>
            e == null ? null : InnerItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['total'],
    json['limit'],
    json['offset'],
    null,
  );
}
