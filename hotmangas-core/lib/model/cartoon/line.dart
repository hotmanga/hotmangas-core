class Line extends Object {
  bool? config;

  Line(
    this.config,
  );

  factory Line.fromJson(Map<String, dynamic> srcJson) =>
      _$LineFromJson(srcJson);
}

Line _$LineFromJson(Map<String, dynamic> json) {
  return Line(
    json['config'],
  );
}
