// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/comic/comic_chapter_entity.dart';

void main() async {
  ComicChapterEntity? response;
  await comic.getChapters(
      comicPathWord: 'mowuniang',
      groupPathWord: 'default',
      queryParameters: {'limit': 1, 'offset': 0},
      resolve: (ComicChapterEntity data) {
        response = data;
      },
      reject: (error) {
        print(error);
      });
  test('匿名访问漫画详情页的章节列表 接口基本信息', () {
    expect(response, isA<ComicChapterEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list, isA<List<ComicChapterItem>>());
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
  });

  test('匿名访问漫画详情页的章节列表 基本信息', () {
    var list = response?.results?.list;
    var chapter = list?.first;
    expect(list?.length, equals(1));
    expect(chapter, isA<ComicChapterItem>());
    expect(chapter?.comicPathWord, equals('mowuniang'));
    expect(chapter?.groupPathWord, equals('default'));
    expect(chapter?.hasPrev, isFalse);
    expect(chapter?.prev, isNull);
    expect(chapter?.hasNext, isTrue);
    expect(chapter?.next, isNotNull);
    expect(chapter?.index, equals(0));
  });
}
