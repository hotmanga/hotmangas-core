import 'package:hotmangasCore/model/core/common/author_or_company.dart'
    show AuthorOrCompany;
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class SearchCartoonListEntity extends ResultsEntity {
  Results results;

  SearchCartoonListEntity(code, message, this.results) : super(code, message);

  factory SearchCartoonListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$SearchCartoonListEntityFromJson(srcJson);
}

SearchCartoonListEntity _$SearchCartoonListEntityFromJson(
    Map<String, dynamic> json) {
  return SearchCartoonListEntity(
      json['code'], json['message'], Results.fromJson(json['results']));
}

class Results extends CommonListResults {
  List<SearchCartoonListItem> list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['total'],
      json['limit'],
      json['offset'],
      (json['list'] as List)
          .map((e) => SearchCartoonListItem.fromJson(e as Map<String, dynamic>))
          .toList());
}

class SearchCartoonListItem extends Object {
  String? cartoonType;

  String? freeType;

  String? grade;

  AuthorOrCompany? company;

  // String uuid;

  String? cover;

  String? category;

  String? name;

  String? lang;

  String? pathWord;

  int? popular;

  String? region;

  String? years;

  SearchCartoonListItem(
      this.cartoonType,
      this.freeType,
      this.grade,
      this.company,
      this.cover,
      this.category,
      this.name,
      this.lang,
      this.pathWord,
      this.popular,
      this.region,
      this.years);

  factory SearchCartoonListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$SearchComicListItemFromJson(srcJson);
}

SearchCartoonListItem _$SearchComicListItemFromJson(Map<String, dynamic> json) {
  return SearchCartoonListItem(
      json['cartoon_type'],
      json['free_type'],
      json['grade'],
      AuthorOrCompany.fromJson(json['company']),
      // json['uuid'],
      json['cover'],
      json['category'],
      json['name'],
      json['lang'],
      json['path_word'],
      json['popular'],
      json['region'],
      json['years'] ?? '');
}
