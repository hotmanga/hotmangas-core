// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';

void main() {
  // test('获取 爱情题材 下 免费动画 列表', () async {
  //   ThemeContentListEntity _;
  //   await theme.getCartoonThemeContentList(
  //       themepPathWord: 'aiqing',
  //       queryParameters: {
  //         'free_type': FreeTypeEnumMap[FreeType.Free],
  //         'limit': 1,
  //         'offset': 0
  //       },
  //       resolve: (ThemeContentListEntity data) {
  //         _ = data;
  //       },
  //       reject: (error) {
  //         print(error);
  //       });

  //   expect(_, isA<ThemeContentListEntity>());
  //   expect(_.code, equals(200));
  //   expect(_.results.limit, equals(1));
  //   expect(_.results.offset, equals(0));
  //   expect(_.results.type, isNull, reason: 'type 该接口没有传入，应该为null');
  //   expect(_.results.list.first, isNotNull);
  //   expect(_.results.list.first, isA<InnerItem>());
  // });

  test('测试某个题材下的动画', () async {
    await cartoon.getQueryList(
        queryParameters: {
          'free_type': FreeTypeEnumMap[FreeType.Charge],
          'theme': 'Uncensored',
          'offset': 0,
          'limit': 1,
          'ordering': OrderingTypeEnumMap[OrderingType.DatetimeUpdatedUp],
        },
        resolve: (entity) {
          print(entity);
        },
        reject: (error) {
          print(error);
        });
  });

  test('测试某个题材下的漫画', () async {
    await comic.getQueryList(
        queryParameters: {
          'free_type': FreeTypeEnumMap[FreeType.Charge],
          'theme': 'COLOR',
          'offset': 0,
          'limit': 1,
          'ordering': OrderingTypeEnumMap[OrderingType.DatetimeUpdatedUp],
        },
        resolve: (entity) {
          print(entity);
        },
        reject: (error) {
          print(error);
        });
  });
}
