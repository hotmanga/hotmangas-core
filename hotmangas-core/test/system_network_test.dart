// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show system;
import 'package:hotmangasCore/model/model.dart' show SystemNetworkEntity;

void main() {
  test('测试获取基础配置接口', () async {
    SystemNetworkEntity? response;
    await system.network.getNetwork(resolve: (data) {
      response = data;
      print(response!.results?.image?.length);
    });
    expect(response!.runtimeType, equals(SystemNetworkEntity));
    expect(response!.code, equals(200), reason: '1');
    expect(response!.message, equals('请求成功'), reason: '2');
    expect(response!.results?.static?.length, greaterThan(0), reason: '3');
    expect(response!.results?.image?.length, greaterThan(0), reason: '4');
    expect(response!.results?.share?.length, greaterThan(0), reason: '5');
  });

  test('测试BaseUrl是否被替换掉', () async {
    SystemNetworkEntity? response;
    await system.network.getNetwork(resolve: (data) {
      response = data;
      print(response!.results?.image?.length);
    });
    await system.config.get1000(resolve: (res) {
      print(res);
    });
  });
}
