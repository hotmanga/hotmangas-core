import 'dart:convert' show Base64Encoder, utf8;
import 'dart:math' show Random;

import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/hot/http_core_extra.dart' show httpCoreExtra;
import 'package:hotmangasCore/model/model.dart'
    show MemberInfoEntity, ResultsEntity;
import 'package:meta/meta.dart' show required;

class Login extends HttpCore {
  void addToken({@required String? token}) {
    httpCoreExtra.addToken(token: token);
  }

  void clearToken() {
    httpCoreExtra.clearToken();
  }

  Future<void> login({
    @required String? userName,
    @required String? passWord,
    Function? resolve,
    Function? reject,
  }) async {
    final salt = Random.secure().nextInt(9999).toString();
    await postCore(
        path: '/api/v3/login',
        queryParameters: null,
        formData: {
          'username': userName,
          'password': Base64Encoder().convert(utf8.encode('$passWord-$salt')),
          'salt': salt,
        },
        resolve: (data) {
          var member = MemberInfoEntity.fromJson(data);
          addToken(token: member.results?.token);
          resolve?.call(member);
        },
        reject: reject);
  }

  Future<void> logout({
    Function? resolve,
    Function? reject,
  }) async {
    await postCore(
        path: '/api/v3/logout',
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }
}
