import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show SystemConfigEntity;

class SystemConfigV2 extends HttpCore {
  Future<void> _getCore(
      {String? path, Function? resolve, Function? reject}) async {
    await getCore(
        path: path,
        resolve: (data) {
          var systemConfig = SystemConfigEntity.fromJson(data);
          resolve?.call(systemConfig);
        },
        reject: reject);
  }

  Future<void> get11001({Function? resolve, Function? reject}) async {
    await _getCore(
        path: '/api/v2/system/config/11001/1',
        resolve: resolve,
        reject: reject);
  }

  @deprecated
  Future<void> get300({resolve, reject}) async {
    await _getCore(
        path: '/api/v2/system/config/300/3', resolve: resolve, reject: reject);
  }

  @deprecated
  Future<void> get1000({resolve, reject}) async {
    await _getCore(
        path: '/api/v2/system/config/1000/3', resolve: resolve, reject: reject);
  }

  @deprecated
  Future<void> get1001({resolve, reject}) async {
    await _getCore(
        path: '/api/v2/system/config/1001/3', resolve: resolve, reject: reject);
  }

  @deprecated
  Future<void> get2020({resolve, reject}) async {
    await _getCore(
        path: '/api/v2/system/config/2020/3', resolve: resolve, reject: reject);
  }

  @deprecated
  Future<void> get30001({resolve, reject}) async {
    await _getCore(
        path: '/api/v2/system/config/30001/3',
        resolve: resolve,
        reject: reject);
  }
}

final system = SystemConfigV2();
