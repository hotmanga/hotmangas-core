import 'package:hotmangasCore/model/model.dart';

import '../core/results_entity.dart' show ResultsEntity;

class CopyAdsEntity extends ResultsEntity {
  Results? results;

  CopyAdsEntity(code, message, this.results) : super(code, message);

  factory CopyAdsEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$CopyAdsEntityFromJson(srcJson);
}

class Results extends Object {
  double? times;

  String? msg;

  Results(this.times, this.msg);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

CopyAdsEntity _$CopyAdsEntityFromJson(Map<String, dynamic> json) {
  return CopyAdsEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['times'] != null ? json['times'].toDouble() : json['times'],
      json['msg']);
}
