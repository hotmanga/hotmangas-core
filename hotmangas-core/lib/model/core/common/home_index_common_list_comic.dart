import 'home_index_common_list_comic_item.dart' show CommonListItemComic;

class CommonListComic extends Object {
  List<CommonListItemComic?>? list;

  int? total;

  int? limit;

  int? offset;

  CommonListComic(
    this.list,
    this.total,
    this.limit,
    this.offset,
  );

  factory CommonListComic.fromJson(Map<String, dynamic> srcJson) =>
      _$CommonListComicFromJson(srcJson);
}

CommonListComic _$CommonListComicFromJson(Map<String, dynamic> json) {
  return CommonListComic(
    (json['list'] as List)
        .map((e) => e == null
            ? null
            : CommonListItemComic.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['total'],
    json['limit'],
    json['offset'],
  );
}
