import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;

import '../core/results_entity.dart' show ResultsEntity;

class ComicChapterEntity extends ResultsEntity {
  Results? results;

  ComicChapterEntity(code, message, this.results) : super(code, message);

  factory ComicChapterEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$ComicChapterEntity(srcJson);
}

ComicChapterEntity _$ComicChapterEntity(Map<String, dynamic> json) {
  return ComicChapterEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends CommonListResults {
  List<ComicChapterItem?>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(json['total'], json['limit'], json['offset'],
      (json['list'] as List).map((e) => ComicChapterItem.fromJson(e)).toList());
}

class ComicChapterItem extends Object {
  int? index;

  String? uuid;

  int? count;

  int? size;

  String? name;

  String? comicId;

  String? comicPathWord;

  String? groupId;

  String? groupPathWord;

  int? type;

  int? ordered;

  DateTime? datetimeCreated;

  String? prev;

  String? next;

  bool? get hasNext {
    return next != null;
  }

  bool? get hasPrev {
    return prev != null;
  }

  ComicChapterItem(
      this.index,
      this.ordered,
      this.uuid,
      this.count,
      this.size,
      this.name,
      this.comicId,
      this.comicPathWord,
      this.groupId,
      this.groupPathWord,
      this.type,
      this.datetimeCreated,
      this.prev,
      this.next);

  factory ComicChapterItem.fromJson(Map<String, dynamic> srcJson) =>
      _$ComicChapterItem(srcJson);
}

ComicChapterItem _$ComicChapterItem(Map<String, dynamic> json) {
  return ComicChapterItem(
    json['index'],
    json['ordered'],
    json['uuid'],
    json['count'],
    json['size'],
    json['name'],
    json['comic_id'],
    json['comic_path_word'],
    json['group_id'],
    json['group_path_word'],
    json['type'],
    DateTime.parse(json['datetime_created']),
    json['prev'],
    json['next'],
  );
}
