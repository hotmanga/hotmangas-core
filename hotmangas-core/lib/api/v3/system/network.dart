import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/hot/options.dart' show SystemNetworkCache;
import 'package:hotmangasCore/model/model.dart' show SystemNetworkEntity;

class SystemNetwork extends HttpCore {
  Future<void> getNetwork({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/system/network2',
        resolve: (data) {
          SystemNetworkEntity? entity = SystemNetworkEntity.fromJson(data);
          resolve?.call(entity);
          SystemNetworkCache.setApis(entity.results?.api?.first);
        },
        reject: reject);
  }
}

final systemNetwork = SystemNetwork();
