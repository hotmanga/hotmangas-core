import '../core/common/common_list_results.dart' show CommonListResults;
import '../core/enmus/content_type.dart' show ContentTypeEnumMap;
import '../core/enmus/decode.dart' show enumDecodeNullable;
import '../core/results_entity.dart' show ResultsEntity;

class GameEntity extends ResultsEntity {
  Results? results;

  GameEntity(
    code,
    message,
    this.results,
  ) : super(code, message);

  factory GameEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$GameEntityFromJson(srcJson);
}

class Results extends CommonListResults {
  List<GameList?>? list;

  Results(
    total,
    this.list,
    limit,
    offset,
    type,
  ) : super(total, limit, offset, type);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

class GameList extends Object {
  bool? enabled;

  String? enabledCnt;

  String? disabledCnt;

  String? name;

  String? cover;

  String? url;

  String? intro;

  GameList(
    this.enabled,
    this.enabledCnt,
    this.disabledCnt,
    this.name,
    this.cover,
    this.url,
    this.intro,
  );

  factory GameList.fromJson(Map<String, dynamic> srcJson) =>
      _$GameListFromJson(srcJson);
}

GameEntity _$GameEntityFromJson(Map<String, dynamic> json) {
  return GameEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    (json['list'] as List)
        .map((e) =>
            e == null ? null : GameList.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['limit'],
    json['offset'],
    enumDecodeNullable(ContentTypeEnumMap, json['type']),
  );
}

GameList _$GameListFromJson(Map<String, dynamic> json) {
  return GameList(
    json['enabled'],
    json['enabled_cnt'],
    json['disabled_cnt'],
    json['name'],
    json['cover'],
    json['url'],
    json['intro'],
  );
}
