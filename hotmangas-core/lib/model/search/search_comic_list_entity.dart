import 'package:hotmangasCore/model/core/common/author_or_company.dart'
    show AuthorOrCompany;
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class SearchComicListEntity extends ResultsEntity {
  Results results;

  SearchComicListEntity(code, message, this.results) : super(code, message);

  factory SearchComicListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$SearchComicListEntityFromJson(srcJson);
}

SearchComicListEntity _$SearchComicListEntityFromJson(
    Map<String, dynamic> json) {
  return SearchComicListEntity(
      json['code'], json['message'], Results.fromJson(json['results']));
}

class Results extends CommonListResults {
  List<SearchComicListItem> list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['total'],
      json['limit'],
      json['offset'],
      (json['list'] as List)
          .map((e) => SearchComicListItem.fromJson(e as Map<String, dynamic>))
          .toList());
}

class SearchComicListItem extends Object {
  String? reclass;

  int? imgType;

  String? role;

  List<AuthorOrCompany>? _author;

  List<AuthorOrCompany?>? get author {
    return _author;
  }
  // String uuid;

  String? cover;

  String? _origName;

  String get origName {
    return _origName ?? '';
  }

  String? name;

  String? _alias;

  String get alias {
    return _alias ?? '';
  }

  String? pathWord;

  int? popular;

  String? status;

  SearchComicListItem(
      this.reclass,
      this.imgType,
      this.role,
      this._author,
      // this.uuid,
      this.cover,
      this._origName,
      this.name,
      this._alias,
      this.pathWord,
      this.popular,
      this.status);

  factory SearchComicListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$SearchComicListItemFromJson(srcJson);
}

SearchComicListItem _$SearchComicListItemFromJson(Map<String, dynamic> json) {
  return SearchComicListItem(
      json['reclass'],
      json['img_type'],
      json['role'],
      (json['author'] as List)
          .map((e) => AuthorOrCompany.fromJson(e as Map<String, dynamic>))
          .toList(),
      // json['uuid'],
      json['cover'],
      json['orig_name'],
      json['name'],
      json['alias'],
      json['path_word'],
      json['popular'],
      json['status']);
}
