class LastChapterItem extends Object {
  String? uuid;

  String? name;

  LastChapterItem(this.uuid, this.name);

  factory LastChapterItem.fromJson(Map<String, dynamic> srcJson) =>
      _$LastChapterFromJson(srcJson);
}

LastChapterItem _$LastChapterFromJson(Map<String, dynamic> json) {
  return LastChapterItem(
    json['uuid'],
    json['name'],
  );
}
