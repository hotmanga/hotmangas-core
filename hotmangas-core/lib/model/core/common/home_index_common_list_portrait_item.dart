import 'package:hotmangasCore/model/core/common/portrait_item.dart';

class CommonListItemPortrait extends Object {
  PortraitItemEntity? post;

  CommonListItemPortrait(
    this.post,
  );

  factory CommonListItemPortrait.fromJson(Map<String, dynamic> srcJson) =>
      _$CommonListItemComicFromJson(srcJson);
}

CommonListItemPortrait _$CommonListItemComicFromJson(
    Map<String, dynamic> json) {
  return CommonListItemPortrait(
    json['post'] == null
        ? null
        : PortraitItemEntity.fromJson(json['post'] as Map<String, dynamic>),
  );
}
