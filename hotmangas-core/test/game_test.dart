// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show game;
import 'package:hotmangasCore/model/model.dart' show GameEntity;

void main() {
  test('测试获取游戏列表', () async {
    GameEntity? response;
    await game.getList(resolve: (GameEntity data) {
      response = data;
    }, reject: (error) {
      print(error);
    });
    expect(response, isA<GameEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.limit, greaterThan(0));
    expect(response?.results?.offset, isZero);
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.length, greaterThan(0));
  });
}
