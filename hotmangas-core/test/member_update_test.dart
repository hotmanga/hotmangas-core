// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试提交修改info信息', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    final _modifyNickName = '047';
    final _modifyGenderValue = 1;

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    await member.update.postInfo(
        fromData: {
          'nickname': _modifyNickName,
          'gender': _modifyGenderValue,
        },
        resolve: (data) {
          print(data);
        },
        reject: (error) {});

    MemberUpdateInfoEntity? actual;
    await member.update.getInfo(resolve: (data) {
      actual = data;
    }, reject: (error) {
      print(error);
    });

    var info = actual?.results?.info;
    expect(info!.nickname, _modifyNickName);
    expect(info.gender?.value, _modifyGenderValue);
  });

  test('测试获取用户info信息', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberUpdateInfoEntity? entity;
    await member.update.getInfo(
        resolve: (data) {
          entity = data;
        },
        reject: (error) {});

    expect(entity, isA<MemberUpdateInfoEntity>());
    expect(entity?.code, equals(200));
    var info = entity?.results?.info;
    expect(info?.nickname, isNotEmpty, reason: '1');
    expect(info?.avatar, isNotEmpty, reason: '2');
    expect(info?.gender, isA<Gender>());
    expect(info?.username, equals(userName));
    expect(info?.mobile, isNotEmpty, reason: '3');
    expect(info?.mobileRegion, isNotEmpty, reason: '4');
    expect(info?.inviteCode, isNotEmpty, reason: '5');
    var genders = entity?.results?.genders;
    expect(genders, isList);
  });
}
