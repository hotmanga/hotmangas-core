import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show MemberInfoEntity;

class Info extends HttpCore {
  Future<void> getInfo({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/member/info',
        queryParameters: null,
        resolve: (data) {
          resolve?.call(MemberInfoEntity.fromJson(data));
        },
        reject: reject);
  }
}
