class RestrictItem extends Object {
  int? value;

  String? display;

  RestrictItem(this.value, this.display);

  factory RestrictItem.fromJson(Map<String, dynamic> srcJson) =>
      _$RestrictFromJson(srcJson);
}

RestrictItem _$RestrictFromJson(Map<String, dynamic> json) {
  return RestrictItem(json['value'], json['display']);
}
