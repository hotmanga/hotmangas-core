import './line.dart' show Line;

class Lines extends Object {
  Line? line1;

  Line? line2;

  Line? line3;

  Lines(
    this.line1,
    this.line2,
    this.line3,
  );

  factory Lines.fromJson(Map<String, dynamic> srcJson) =>
      _$LinesFromJson(srcJson);
}

Lines _$LinesFromJson(Map<String, dynamic> json) {
  return Lines(
    json['line1'] == null
        ? null
        : Line.fromJson(json['line1'] as Map<String, dynamic>),
    json['line2'] == null
        ? null
        : Line.fromJson(json['line2'] as Map<String, dynamic>),
    json['line3'] == null
        ? null
        : Line.fromJson(json['line3'] as Map<String, dynamic>),
  );
}
