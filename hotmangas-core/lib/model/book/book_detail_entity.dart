import 'package:hotmangasCore/api/utils/datetime_format.dart';
import 'package:hotmangasCore/model/core/common/author_or_company.dart';
import 'package:hotmangasCore/model/core/common/items/book_last_chapter.dart';
import 'package:hotmangasCore/model/core/common/items/region_item.dart';
import 'package:hotmangasCore/model/core/common/items/serial_item.dart';
import 'package:hotmangasCore/model/core/common/items/status_item.dart';
import 'package:hotmangasCore/model/core/common/items/tag_item.dart';
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class BookDetailEntity extends ResultsEntity {
  Results? results;

  BookDetailEntity(code, message, this.results) : super(code, message);

  factory BookDetailEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$BookDetailEntityFromJson(srcJson);
}

BookDetailEntity _$BookDetailEntityFromJson(Map<String, dynamic> json) {
  return BookDetailEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends Object {
  /// 未登录是 true,  不允许浏览当前内容
  /// 已登录是 false, 可以浏览当前内容
  bool? isLock;

  /// 是否登录
  bool? isLogin;

  /// 是否绑定手机
  bool? isMobileBind;

  /// 是否是 VIP
  bool? isVip;

  /// 小说info
  BookDetailItem? book;

  /// 热度
  int? popular;

  Results(this.isLock, this.isLogin, this.isMobileBind, this.isVip, this.book,
      this.popular);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['is_lock'],
    json['is_login'],
    json['is_mobile_bind'],
    json['is_vip'],
    json['book'] == null
        ? null
        : BookDetailItem.fromJson(json['book'] as Map<String, dynamic>),
    json['popular'],
  );
}

class BookDetailItem extends Object {
  /// 小说 UUID
  String? uuid;

  /// 小说名称
  String? name;

  /// 小说 pathword
  String? pathWord;

  /// 小说是否关闭评论
  bool? closeComment;

  /// 小说是否关闭吐槽
  bool? closeRoast;

  /// 小说地域
  RegionItem? region;

  /// 小说状态 连载, 已完结
  StatusItem? status;

  /// 小说作者
  List<AuthorOrCompany?>? _author;

  /// 是否包含作者
  bool? get hasAuthor {
    return _author?.isNotEmpty;
  }

  List<AuthorOrCompany?>? get author {
    return _author;
  }

  /// 小说主题
  List<TagOrThemeItem?>? theme;

  /// 是否包含主题
  bool? get hasTagOrTheme {
    return theme?.isNotEmpty;
  }

  /// 小说系列
  List<SerialItem?>? parodies;

  /// 是否包含系列
  bool? get hasParodies {
    return parodies?.isNotEmpty;
  }

  /// 小说简介
  String? brief;

  DateTime? _datetimeUpdated;

  /// datetimeUpdated 的 get 属性, 直接返回 'yyyy-MM-dd' 格式
  String? get datetimeUpdated {
    return datetimeFormat(_datetimeUpdated);
  }

  /// 小说封面
  String? cover;

  /// 小说最后更新章节
  BookLastChapter? lastChapter;

  /// 小说热度
  int? popular;

  BookDetailItem(
    this.uuid,
    this.name,
    this.pathWord,
    this.closeComment,
    this.closeRoast,
    this.region,
    this.status,
    this._author,
    this.theme,
    this.parodies,
    this.brief,
    this._datetimeUpdated,
    this.cover,
    this.lastChapter,
    this.popular,
  );

  factory BookDetailItem.fromJson(Map<String, dynamic> srcJson) =>
      _$BookDetailItemFromJson(srcJson);
}

BookDetailItem _$BookDetailItemFromJson(Map<String, dynamic> json) {
  return BookDetailItem(
    json['uuid'],
    json['name'],
    json['path_word'],
    json['close_comment'],
    json['close_roast'],
    RegionItem.fromJson(json['region']),
    StatusItem.fromJson(json['status']),
    (json['author'] as List)
        .map((e) => e == null
            ? null
            : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['theme'] as List)
        .map((e) => e == null
            ? null
            : TagOrThemeItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['parodies'] as List)
        .map((e) =>
            e == null ? null : SerialItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['brief'],
    json['datetime_updated'] == null
        ? null
        : DateTime.parse(json['datetime_updated']),
    json['cover'],
    json['last_chapter'] == null
        ? null
        : BookLastChapter.fromJson(json['last_chapter']),
    json['popular'],
  );
}
