import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show FilterListEntity;

final Map<String, dynamic> _queryParameters = {};

/// /api/v3/h5/filterIndex/comic/tags?type=1
/// /api/v3/h5/filterIndex/comic/tags?type=2
/// /api/v3/h5/filterIndex/cartoon/tags?type=1
/// /api/v3/h5/filterIndex/cartoon/tags?type=3
class FilterIndex extends HttpCore {
  Future<void> _getDiscoverIndex(
      {String? path,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/h5/filter/$path/tags',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(FilterListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getFreeComicFilterIndex(
      {Function? resolve, Function? reject}) async {
    await _getDiscoverIndex(
        path: 'comic',
        queryParameters: {'type': 1},
        resolve: resolve,
        reject: reject);
  }

  Future<void> getChargeComicFilterIndex(
      {Function? resolve, Function? reject}) async {
    await _getDiscoverIndex(
        path: 'comic',
        queryParameters: {'type': 2},
        resolve: resolve,
        reject: reject);
  }

  Future<void> getChargeCartoonFilterIndex(
      {Function? resolve, Function? reject}) async {
    await _getDiscoverIndex(
        path: 'cartoon',
        queryParameters: {'type': 1},
        resolve: resolve,
        reject: reject);
  }

  Future<void> getHomeIndexCartoon(
      {Function? resolve, Function? reject}) async {
    await _getDiscoverIndex(
        path: 'cartoon',
        queryParameters: {'type': 3},
        resolve: resolve,
        reject: reject);
  }

  /// 获取轻小说下所有题材
  Future<void> getBookFilterIndex({Function? resolve, Function? reject}) async {
    await _getDiscoverIndex(
      path: 'book',
      queryParameters: {},
      resolve: resolve,
      reject: reject,
    );
  }

  /// 获取写真币下所有tag
  Future<void> getPortraitFilterIndex(
      {Function? resolve, Function? reject}) async {
    await _getDiscoverIndex(
      path: 'post',
      queryParameters: {},
      resolve: resolve,
      reject: reject,
    );
  }
}

final filterIndex = FilterIndex();
