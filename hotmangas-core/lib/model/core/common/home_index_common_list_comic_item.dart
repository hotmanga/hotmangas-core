import 'inner_item.dart' show InnerItem;

class CommonListItemComic extends Object {
  InnerItem? comic;

  CommonListItemComic(
    this.comic,
  );

  factory CommonListItemComic.fromJson(Map<String, dynamic> srcJson) =>
      _$CommonListItemComicFromJson(srcJson);
}

CommonListItemComic _$CommonListItemComicFromJson(Map<String, dynamic> json) {
  return CommonListItemComic(
    json['comic'] == null
        ? null
        : InnerItem.fromJson(json['comic'] as Map<String, dynamic>),
  );
}
