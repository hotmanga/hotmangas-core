import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        UpdateListEntity,
        UpdatePortraitListEntity,
        UpdateType,
        UpdateTypeEnumMap;

final Map<String, dynamic> _queryParameters = {
  'date': UpdateTypeEnumMap[UpdateType.Error],
  'limit': 21,
  'offset': 0,
};

class Update extends HttpCore {
  @deprecated
  Future<void> getList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/updates',
        queryParameters: _,
        resolve: (data) {
          var updateListEntity = UpdateListEntity.fromJson(data);
          resolve?.call(updateListEntity);
        },
        reject: reject);
  }

  // 全新上架
  Future<void> getNewestList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/update/newest',
        queryParameters: _,
        resolve: (data) {
          var updateListEntity = UpdateListEntity.fromJson(data);
          resolve?.call(updateListEntity);
        },
        reject: reject);
  }

  /// 写真全新上架
  Future<void> getPortraitNewestList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/update/post/newest',
        queryParameters: _,
        resolve: (data) {
          var updatePortraitListEntity =
              UpdatePortraitListEntity.fromJson(data);
          resolve?.call(updatePortraitListEntity);
        },
        reject: reject);
  }
}

final update = Update();
