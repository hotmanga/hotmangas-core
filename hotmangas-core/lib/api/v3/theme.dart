import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show FreeType, FreeTypeEnumMap, ThemeContentListEntity;
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'free_type': FreeTypeEnumMap[FreeType.None],
  'limit': 21,
  'offset': 0,
};

class Theme extends HttpCore {
  Future<void> _getList(
      {String? path,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: path,
        queryParameters: _,
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }

  Future<void> getCartoonThemeList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await _getList(
        path: '/api/v3/theme/cartoon/count',
        queryParameters: _,
        resolve: (data) {
          // var rankListEntity = RankListEntity.fromJson(data);
          // resolve(rankListEntity);
          resolve?.call(data);
        },
        reject: reject);
  }

  //path_word aiqing
  @deprecated
  Future<void> getCartoonThemeContentList(
      {@required String? themepPathWord,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await _getList(
        path: '/api/v3/theme/$themepPathWord/cartoons',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(ThemeContentListEntity.fromJson(data));
          // resolve(data);
        },
        reject: reject);
  }

  Future<void> getComicThemeList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await _getList(
        path: '/api/v3/theme/comic/count',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }
}

final theme = Theme();
