import './lines.dart' show Lines;
import './video_list.dart' show VideoList;
import '../core/results_entity.dart' show ResultsEntity;

class PlayInfoEntity extends ResultsEntity {
  Results? results;

  PlayInfoEntity(int code, String message, this.results) : super(code, message);

  factory PlayInfoEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$PlayInfoCloudflareEntityFromJson(srcJson);
}

PlayInfoEntity _$PlayInfoCloudflareEntityFromJson(Map<String, dynamic> json) {
  return PlayInfoEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends Object {
  bool? isLogin;
  bool? isMobileBind;
  bool? isVip;
  bool? isLock;

  Cartoon? cartoon;

  Chapter? chapter;

  Results(this.isLogin, this.isMobileBind, this.isVip, this.isLock,
      this.cartoon, this.chapter);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['is_login'],
    json['is_mobile_bind'],
    json['is_vip'],
    json['is_lock'],
    Cartoon.fromJson(json['cartoon']),
    Chapter.fromJson(json['chapter']),
  );
}

class Cartoon extends Object {
  String? name;

  String? uuid;

  String? pathWord;

  Cartoon(this.name, this.uuid, this.pathWord);

  factory Cartoon.fromJson(Map<String, dynamic> srcJson) =>
      _$CartoonFromJson(srcJson);
}

Cartoon _$CartoonFromJson(Map<String, dynamic> json) {
  return Cartoon(
    json['name'],
    json['uuid'],
    json['path_word'],
  );
}

class Chapter extends Object {
  int? count;

  String? name;

  // String cover;

  String? vid;

  String? video;

  String? vCover;

  Lines? lines;

  Chapter(this.count, this.name, this.vid, this.video, this.vCover, this.lines,
      this.videoList);

  List<VideoList?>? videoList;

  factory Chapter.fromJson(Map<String, dynamic> srcJson) =>
      _$ChapterFromJson(srcJson);
}

Chapter _$ChapterFromJson(Map<String, dynamic> json) {
  return Chapter(
    json['count'],
    json['name'],
    json['vid'],
    json['video'],
    json['v_cover'],
    Lines.fromJson(json['lines']),
    (json['video_list'] as List).map((e) => VideoList.fromJson(e)).toList(),
  );
}
