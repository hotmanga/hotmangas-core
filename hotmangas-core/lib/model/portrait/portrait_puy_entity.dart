import '../core/results_entity.dart' show ResultsEntity;

///购买写真后的返回数据
class PortraitPuyEntity extends ResultsEntity {
  Results? results;

  PortraitPuyEntity(code, message, this.results) : super(code, message);

  factory PortraitPuyEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitPuyEntity(srcJson);
}

PortraitPuyEntity _$PortraitPuyEntity(Map<String, dynamic> json) {
  return PortraitPuyEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends Object {
  String? ticket;
  String? rewardTicket;

  ///打赏金额
  String? rewards;

  ///看广告的写真币和购买的写真币总和
  int? get ticketAll {
    return int.parse(ticket!) + int.parse(rewardTicket!);
  }

  Results(
    this.ticket,
    this.rewardTicket,
    this.rewards,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['ticket'] ?? '0',
    json['reward_ticket'] ?? '0',
    json['rewards'] ?? '0',
  );
}
