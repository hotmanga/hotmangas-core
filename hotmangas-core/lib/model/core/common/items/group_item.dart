class GroupWrapper extends Object {
  final Map<String, dynamic> _innerGroups = {};

  final List<GroupItem> list = [];

  GroupWrapper(Map<String, dynamic> innerGroups) {
    _innerGroups.addAll(innerGroups);
    innerGroups.forEach((key, value) {
      list.add(
          GroupItem(key, value['path_word'], value['count'], value['name']));
    });
  }

  List<String> get names {
    return _innerGroups.keys.toList();
  }
}

class GroupItem extends Object {
  String? groupName;

  String? pathWord;

  int? count;

  String? name;

  @override
  String toString() {
    return 'GroupName:$groupName|Name:$name|PathWord:$pathWord';
  }

  GroupItem(this.groupName, this.pathWord, this.count, this.name);
}
