// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show book, member;
import 'package:hotmangasCore/model/model.dart'
    show
        BookDetailEntity,
        BookVolumeEntity,
        BookContentType,
        BookVolumesListEntity,
        FilterBookListEntity,
        HotmangasError,
        MemberCollectEntityV2,
        TxtEncodingType;
import 'package:test/test.dart';

void main() async {
  test('调用轻小说检索接口, 默认不带参数', () async {
    FilterBookListEntity? response;
    await book.getQueryList(
        queryParameters: {
          'offset': 0,
          'limit': 1,
        },
        resolve: (FilterBookListEntity _response) {
          response = _response;
        });
    expect(response, isA<FilterBookListEntity>());
    expect(response?.code, equals(200));
  });

  test('调用轻小说接口, 主题为 AA , 判断长度是否为1', () async {
    FilterBookListEntity? response;
    await book.getQueryList(
        queryParameters: {
          'theme': 'AA',
          'offset': 0,
          'limit': 1,
        },
        resolve: (FilterBookListEntity _response) {
          response = _response;
        });
    expect(response, isA<FilterBookListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list?.length, equals(1));
  });

  test('调用轻小说接口, 主题为 BB , 判断长度是否为 0', () async {
    FilterBookListEntity? response;
    await book.getQueryList(
        queryParameters: {
          'theme': 'BB',
          'offset': 0,
          'limit': 1,
        },
        resolve: (FilterBookListEntity _response) {
          response = _response;
        });
    expect(response, isA<FilterBookListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list?.length, equals(0));
  });

  test('获取测试用轻小说详情页', () async {
    BookDetailEntity? response;
    final pathWord = 'testbook';
    await book.getDetail(
        pathWord: pathWord,
        resolve: (BookDetailEntity _response) {
          response = _response;
        },
        reject: (HotmangasError error) {
          print('${error.errorType} ${error.message}');
        });

    expect(response, isA<BookDetailEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.book?.uuid,
        equals('be75b54e-58e9-11ed-94c0-024352452ce0'));
    expect(response?.results?.book?.pathWord, equals(pathWord));
    expect(response?.results?.book?.closeComment, equals(false));
    expect(response?.results?.book?.closeRoast, equals(false));
    expect(response?.results?.book?.closeRoast, equals(false));
    expect(response?.results?.book?.parodies?.length, equals(1));
    expect(response?.results?.book?.lastChapter?.isEmpty, equals(false));
  });

  test('获取测试用轻小说详情页-获取用户状态信息, 未登录请求', () async {
    MemberCollectEntityV2? response;

    await book.getCurrentBookCollect(
        pathWord: 'testbook',
        resolve: (MemberCollectEntityV2 _response) {
          response = _response;
        },
        reject: (HotmangasError error) {
          print('${error.errorType} ${error.message}');
        });

    expect(response, isA<MemberCollectEntityV2>());
    expect(response?.code, equals(200));
    expect(response?.results?.hasCollect, equals(false));
    expect(response?.results?.isLock, equals(true));
    expect(response?.results?.isLogin, equals(false));
    expect(response?.results?.isMobileBind, equals(false));
    expect(response?.results?.browse, equals(null));
  });

  test('获取测试用轻小说详情页-获取用户状态信息, 已登录请求', () async {
    final userName = 'kbceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberCollectEntityV2? response;

    await book.getCurrentBookCollect(
        pathWord: 'testbook',
        resolve: (MemberCollectEntityV2 _response) {
          response = _response;
        },
        reject: (HotmangasError error) {
          print('${error.errorType} ${error.message}');
        });

    expect(response, isA<MemberCollectEntityV2>());
    expect(response?.code, equals(200));
    expect(response?.results?.hasCollect, equals(false));
    expect(response?.results?.isLock, equals(false));
    expect(response?.results?.isLogin, equals(true));
    expect(response?.results?.isMobileBind, equals(false));
    expect(response?.results?.browse, equals(null));
  });

  test('测试获取轻小说所有章节', () async {
    BookVolumesListEntity? response;
    await book.getVolumes(
        pathWord: 'testbook',
        resolve: (BookVolumesListEntity _response) {
          response = _response;
        },
        reject: (HotmangasError error) {
          print('${error.errorType} ${error.message}');
        });
    expect(response, isA<BookVolumesListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.volumes?.length, equals(1));
    expect(response?.results?.volumes?[0]?.prev, equals(null));
    expect(response?.results?.volumes?[0]?.hasNoPrev, equals(true));
    expect(response?.results?.volumes?[0]?.hasPrev, equals(false));
    expect(response?.results?.volumes?[0]?.next, equals(null));
    expect(response?.results?.volumes?[0]?.hasNoNext, equals(true));
    expect(response?.results?.volumes?[0]?.hasNext, equals(false));
  });

  test('测试获取轻小说某个章节的内容', () async {
    BookVolumeEntity? response;
    await book.getVolumeContents(
        pathWord: 'ceshiImage',
        volumeId: '4243',
        resolve: (BookVolumeEntity _response) {
          response = _response;
        },
        reject: (HotmangasError error) {
          print('${error.errorType} ${error.message}');
        });
    expect(response, isA<BookVolumeEntity>());
    expect(response?.results?.volume?.contents!.first!.contentType,
        equals(BookContentType.Text));
    expect(response?.code, equals(200));
    expect(response?.results?.volume?.id, equals('4243'));
    expect(response?.results?.volume?.textAddress, isNotEmpty);
    expect(response?.results?.volume?.contents?.length, greaterThan(1));
    expect(
        response?.results?.volume?.txtEncoding, equals(TxtEncodingType.UTF8));
  });
}
