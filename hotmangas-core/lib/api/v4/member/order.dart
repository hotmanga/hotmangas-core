import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show MemberOrderEntity;

class Order extends HttpCore {
  /// 正式用接口 /api/v4/member/order
  Future<void> createOrder(
      {Map<String, dynamic>? formData,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v4/member/order',
        formData: formData,
        queryParameters: null,
        resolve: (data) {
          resolve?.call(MemberOrderEntity.fromJson(data));
        },
        reject: reject);
  }
}
