import 'package:hotmangasCore/model/core/common/inner_item.dart';

class CommonPageComic extends Object {
  List<InnerItem?>? list;

  int? total;

  int? limit;

  int? offset;

  CommonPageComic(
    this.list,
    this.total,
    this.limit,
    this.offset,
  );

  factory CommonPageComic.fromJson(Map<String, dynamic> srcJson) =>
      _$CommonListComicFromJson(srcJson);
}

CommonPageComic _$CommonListComicFromJson(Map<String, dynamic> json) {
  return CommonPageComic(
    (json['list'] as List)
        .map((e) =>
            e == null ? null : InnerItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['total'],
    json['limit'],
    json['offset'],
  );
}
