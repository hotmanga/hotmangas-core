import 'home_index_common_list_cartoon_item.dart' show CommonListCartoonItem;

class CommonListCartoon extends Object {
  List<CommonListCartoonItem?>? list;

  int? total;

  int? limit;

  int? offset;

  CommonListCartoon(
    this.list,
    this.total,
    this.limit,
    this.offset,
  );

  factory CommonListCartoon.fromJson(Map<String, dynamic> srcJson) =>
      _$CommonListCartoonFromJson(srcJson);
}

CommonListCartoon _$CommonListCartoonFromJson(Map<String, dynamic> json) {
  return CommonListCartoon(
    (json['list'] as List)
        .map((e) => e == null
            ? null
            : CommonListCartoonItem.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['total'],
    json['limit'],
    json['offset'],
  );
}
