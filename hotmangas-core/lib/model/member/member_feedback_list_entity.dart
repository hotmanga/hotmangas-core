import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/enmus/decode.dart'
    show enumDecodeNullable;
import 'package:hotmangasCore/model/core/enmus/feed_back_type.dart'
    show FeedBackTypeEnumMap, FeedBackType;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberFeedBackListEntity extends ResultsEntity {
  Results? results;

  MemberFeedBackListEntity(code, message, this.results) : super(code, message);

  factory MemberFeedBackListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberFeedBackListEntityFromJson(srcJson);
}

MemberFeedBackListEntity _$MemberFeedBackListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberFeedBackListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<MemberFeedBackItem>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List).map((e) => MemberFeedBackItem.fromJson(e)).toList(),
  );
}

class MemberFeedBackItem extends Object {
  FeedBackTypeItem? type;

  String? message;

  String? _reply;

  String? get reply {
    return _reply!;
  }

  bool? get isReplied {
    return _reply == null;
  }

  String? _dateTimeReply;

  String? get dateTimeReply {
    return _dateTimeReply ?? '';
  }

  String? dateTimeCreated;

  MemberFeedBackItem(this.type, this.message, this._reply, this._dateTimeReply,
      this.dateTimeCreated);

  factory MemberFeedBackItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberFeedBackItemFromJson(srcJson);
}

MemberFeedBackItem _$MemberFeedBackItemFromJson(Map<String, dynamic> json) {
  return MemberFeedBackItem(
    FeedBackTypeItem.fromJson(json['type']),
    json['message'],
    json['reply'],
    json['datetime_reply'],
    json['datetime_created'],
  );
}

class FeedBackTypeItem extends Object {
  int _value;

  String display;

  FeedBackType? get value {
    return enumDecodeNullable(FeedBackTypeEnumMap, _value);
  }

  FeedBackTypeItem(this._value, this.display);

  factory FeedBackTypeItem.fromJson(Map<String, dynamic> srcJson) =>
      _$FeedBackTypeFromJson(srcJson);
}

FeedBackTypeItem _$FeedBackTypeFromJson(Map<String, dynamic> json) {
  return FeedBackTypeItem(json['value'], json['display']);
}
