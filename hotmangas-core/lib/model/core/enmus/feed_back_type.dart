enum FeedBackType {
  /// 无
  None,
  /// 支付错误
  PayError,
  /// 网站报错
  SomethingWrong,
  /// 建议
  Advise,
}

const FeedBackTypeEnumMap = {
  FeedBackType.None: 0,
  FeedBackType.PayError: 1,
  FeedBackType.SomethingWrong: 2,
  FeedBackType.Advise: 3,
};
