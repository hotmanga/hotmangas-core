// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show comic;
import 'package:hotmangasCore/api/v3/api.dart' show copyApi;
import 'package:hotmangasCore/api/v3/api.dart' show member;
import 'package:hotmangasCore/model/core/enmus/home_index_banner_type.dart';
import 'package:hotmangasCore/model/model.dart';

void main() {
  test('拷贝漫画首页接口', () async {
    CopyHomeIndexEntity? response;
    await copyApi.getHomeIndex(
        reject: () {},
        resolve: (CopyHomeIndexEntity entity) {
          response = entity;
          print(response?.results?.banners?.first?.type);
        });
    expect(response, isA<CopyHomeIndexEntity>(), reason: '1');
    expect(response?.code, equals(200), reason: '2');
    expect(response?.results?.banners?.first?.type, equals(BannerType.Inline),
        reason: '3');
    expect(response?.results?.topics?.list?.length, equals(4), reason: '4');
    expect(response?.results?.recComics?.list?.length, equals(3), reason: '5');
    expect(response?.results?.rankDayComics?.list?.length, equals(6),
        reason: '6');
    expect(response?.results?.rankWeekComics?.list?.length, equals(6),
        reason: '7');
    expect(response?.results?.hotComics?.length, equals(12), reason: '8');
    expect(response?.results?.newComics?.length, equals(12), reason: '9');
    expect(response?.results?.finishComics?.list?.length, equals(6),
        reason: '10');

    // expect(response?.results?.limit, equals(6));
    // expect(response?.results?.offset, equals(2));
    // expect(response?.results?.list, isList);
    // expect(response?.results??.list?.length, equals(6));
  });
  test('拷贝漫画广告接口', () async {
    CopyAdsEntity? response;
    await copyApi.getAdsShow(
        reject: () {},
        resolve: (CopyAdsEntity entity) {
          response = entity;
        });
    expect(response, isA<CopyAdsEntity>(), reason: '1');
    expect(response?.code, equals(200), reason: '2');
    expect(response?.results?.times, equals(6.0), reason: '3');

    // expect(response?.results?.limit, equals(6));
    // expect(response?.results?.offset, equals(2));
    // expect(response?.results?.list, isList);
    // expect(response?.results??.list?.length, equals(6));
  });
  test('拷贝漫画下载接口', () async {
    final userName = 'copy002';
    final passWord = '111111';
    ComicChapterContentSignEntity? response;
    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await comic.getChapterContentByDownload(
        comicPathWord: 'xiaolvlvyuaili',
        comicChapterId: '87c402ba-e5df-11e9-abbb-00163e0ca5bd',
        reject: () {},
        resolve: (ComicChapterContentSignEntity entity) {
          response = entity;
        });
    expect(response, isA<ComicChapterContentSignEntity>(), reason: '1');
    expect(response?.code, equals(200), reason: '2');
    expect(response?.results?.isVip, equals(false), reason: '3');
    expect(response?.results?.dayDownloads, equals(49), reason: '4');
    expect(response?.results?.dayDownloads, equals(123), reason: '5');
  });
}
