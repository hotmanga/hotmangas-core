import 'package:hotmangasCore/model/core/common/author_or_company.dart';

class PortraitItemEntity extends Object {
  String? uuid;

  String? name;

  String? cover;

  String? ticket;

  List<AuthorOrCompany?>? _mannequins;

  String? brief;

  String? datetimeUpdated;

  int? popular;

  ///模特
  List<AuthorOrCompany?>? get mannequins {
    return _mannequins;
  }

  ///纯展示用模特名称
  String get textMannequins {
    return mannequins!.map((e) => e!.name).toList().join(',');
  }

  PortraitItemEntity(
    this.uuid,
    this.name,
    this.cover,
    this.ticket,
    this._mannequins,
    this.brief,
    this.datetimeUpdated,
    this.popular,
  );

  factory PortraitItemEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitItemFromJson(srcJson);
}

PortraitItemEntity _$PortraitItemFromJson(Map<String, dynamic> json) {
  return PortraitItemEntity(
    json['uuid'] ?? '0',
    json['name'] ?? '',
    json['cover'] ?? '',
    json['ticket'] ?? '',
    (json['mannequins'] as List)
        .map((e) => e == null
            ? null
            : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['brief'] ?? '',
    json['datetime_updated'] ?? '',
    json['popular'] ?? '',
  );
}
