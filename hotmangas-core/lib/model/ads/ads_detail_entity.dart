import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class AdsDetailEntity extends ResultsEntity {
  Results? results;

  AdsDetailEntity(code, message, this.results) : super(code, message);

  factory AdsDetailEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$AdsDetailEntityFromJson(srcJson);
}

AdsDetailEntity _$AdsDetailEntityFromJson(Map<String, dynamic> json) {
  return AdsDetailEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends Object {
  /// 广告点击 曝光 错误需要传的值
  String? request_params;

  /// request_id
  String? request_id;

  /// 渠道ident
  String? channel_ident;

  ///  渠道广告位ID
  String? channel_show_id;

  Results(this.request_params, this.request_id, this.channel_ident,
      this.channel_show_id);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['request_params'],
    json['request_id'],
    json['channel_ident'],
    json['channel_show_id'],
  );
}
