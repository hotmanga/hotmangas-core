import 'package:hotmangasCore/api/utils/datetime_format.dart'
    show datetimeFormat;

class MemberInfo extends Object {
  String? token;

  String? userId;

  String? userName;

  String? nickName;

  String? avatar;

  bool? isAuthenticated;

  String _dateTimeCreated;

  String? get dateTimeCreated {
    if (_dateTimeCreated.isEmpty) {
      return '-';
    }
    return datetimeFormat(DateTime.parse(_dateTimeCreated));
  }

  bool? bVerifyEmail;

  String? email;

  String? mobile;

  String? mobileRegion;

  /// 积分?
  // @deprecated
  // int? point;

  int? _comicVip;

  bool get isComicVip {
    return _comicVip == 10;
  }

  String _comicVipEnd;

  String? get comicVipEnd {
    return datetimeFormat(DateTime.parse(_comicVipEnd));
  }

  String _comicVipStart;

  String? get comicVipStart {
    return datetimeFormat(DateTime.parse(_comicVipStart));
  }

  int get comicVipResidue {
    if (_comicVipEnd.isEmpty) {
      return 0;
    }
    final endTime = DateTime.parse(_comicVipEnd);
    final nowTime = DateTime.now().toUtc();
    var diff =
        (endTime.difference(nowTime).inMilliseconds / 1000 / 3600 / 24).ceil();
    if (diff < 0) {
      diff = 0;
    }
    return diff;
  }

  int? _cartoonVip;

  bool get isCartoonVip {
    return _cartoonVip == 20;
  }

  String _cartoonVipEnd;

  /// 用户看广告获取的写真赠币
  double? _rewardTicket;

  /// 用户看广告获取的写真赠币
  String? get rewardTicket {
    String? _str = _rewardTicket.toString();
    return _str.split('.')[0].toString();
  }

  double? _ticket;

  /// 写真币
  String? get ticket {
    String? _str = _ticket.toString();
    return _str.split('.')[0].toString();
  }

  ///看广告的写真币和购买的写真币总和
  int? get ticketAll {
    return int.parse(ticket!) + int.parse(rewardTicket!);
  }

  String? get cartoonVipEnd {
    return datetimeFormat(DateTime.parse(_cartoonVipEnd));
  }

  // @JsonName('cartoon_vip_start')
  String _cartoonVipStart;

  String? get cartoonVipStart {
    return datetimeFormat(DateTime.parse(_cartoonVipStart));
  }

  int get cartoonVipResidue {
    if (_cartoonVipEnd.isEmpty) {
      return 0;
    }
    final endTime = DateTime.parse(_cartoonVipEnd);
    final nowTime = DateTime.now().toUtc();
    var diff =
        (endTime.difference(nowTime).inMilliseconds / 1000 / 3600 / 24).ceil();
    if (diff < 0) {
      diff = 0;
    }
    return diff;
  }

  // @JsonName('invite_code')
  String? inviteCode;

  // @JsonName('invited')
  String? invited;

  // @JsonName('b_sstv')
  bool? bSstv;

  // @JsonName('scy_answer')
  bool? _scyAnswer;

  bool get hasScyAnswer {
    return _scyAnswer!;
  }

  // 用戶每日下載次數
  int? dayDownloads;

  // 用戶贊助次數
  int? vipDownloads;

  // 用戶激勵視頻次數
  int? rewardDownloads;

  String _adsVipEnd;

  // VIP 结束时间
  String? get adsVipEnd {
    if (_adsVipEnd.isEmpty) {
      return '-';
    }
    return datetimeFormat(DateTime.parse(_adsVipEnd));
  }

  /// 广告赞助的 vip 是否过期了.
  bool get isVip {
    if (_adsVipEnd.isEmpty) {
      return false;
    }
    if (DateTime.parse(_adsVipEnd).difference(DateTime.now()).inDays >= 0) {
      return true;
    } else {
      return false;
    }
  }

  /// 写真订阅会员到期时间详细
  String? postVipEndTime;

  /// 写真订阅会员到期时间
  String? get postVipEnd {
    if (postVipEndTime!.isEmpty) {
      return '-';
    }
    return datetimeFormat(DateTime.parse(postVipEndTime!));
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'token': token,
      'user_id': userId,
      'username': userName,
      'nickname': nickName,
      'avatar': avatar,
      'is_authenticated': isAuthenticated,
      'datetime_created': dateTimeCreated,
      'b_verify_email': bVerifyEmail,
      'email': email,
      'mobile': mobile,
      'mobile_region': mobileRegion,
      // 'point': point,
      'comic_vip': _comicVip,
      'comic_vip_end': _comicVipEnd,
      'comic_vip_start': _comicVipStart,
      'cartoon_vip': _cartoonVip,
      'cartoon_vip_end': _cartoonVipEnd,
      'cartoon_vip_start': _cartoonVipStart,
      'ads_vip_end': _adsVipEnd,
      'dayDownloads': dayDownloads,
      'vipDownloads': vipDownloads,
      'rewardDownloads': rewardDownloads,
      'invite_code': inviteCode,
      'invited': invited,
      'b_sstv': bSstv,
      'scy_answer': _scyAnswer,
      'ticket': _ticket,
      'rewardTicket': _rewardTicket,
      'post_vip_end': postVipEndTime,
    };
  }

  MemberInfo(
    this.token,
    this.userId,
    this.userName,
    this.nickName,
    this.avatar,
    this.isAuthenticated,
    this._dateTimeCreated,
    this.bVerifyEmail,
    this.email,
    this.mobile,
    this.mobileRegion,
    // this.point,
    this._comicVip,
    this._comicVipEnd,
    this._comicVipStart,
    this._cartoonVip,
    this._cartoonVipEnd,
    this._cartoonVipStart,
    this._adsVipEnd,
    this.dayDownloads,
    this.vipDownloads,
    this.rewardDownloads,
    this.inviteCode,
    this.invited,
    this.bSstv,
    this._scyAnswer,
    this._ticket,
    this._rewardTicket,
    this.postVipEndTime,
  );

  factory MemberInfo.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberInfoFromJson(srcJson);
}

MemberInfo _$MemberInfoFromJson(Map<String, dynamic> json) {
  return MemberInfo(
    json['token'] ?? '',
    json['user_id'] ?? '',
    json['username'] ?? '',
    json['nickname'] ?? '',
    json['avatar'] ?? '',
    json['is_authenticated'] ?? false,
    json['datetime_created'] ?? '',
    json['b_verify_email'] ?? false,
    json['email'] ?? '',
    json['mobile'] ?? '',
    json['mobile_region'] ?? '',
    // json['point'],
    json['comic_vip'] ?? 0,
    json['comic_vip_end'] ?? '',
    json['comic_vip_start'] ?? '',
    json['cartoon_vip'],
    json['cartoon_vip_end'] ?? '',
    json['cartoon_vip_start'] ?? '',
    json['ads_vip_end'] ?? '',
    json['day_downloads'] ?? 0,
    json['vip_downloads'] ?? 0,
    json['reward_downloads'] ?? 0,
    json['invite_code'] ?? '',
    json['invited'] ?? '',
    json['b_sstv'],
    json['scy_answer'],
    json['ticket'] ?? 0,
    json['reward_ticket'] ?? 0,
    json['post_vip_end'] ?? '',
  );
}
