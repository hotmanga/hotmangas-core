import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class BookVolumesListEntity extends ResultsEntity {
  Results? results;

  BookVolumesListEntity(code, message, this.results) : super(code, message);

  factory BookVolumesListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$FilterBookListEntity(srcJson);
}

BookVolumesListEntity _$FilterBookListEntity(Map<String, dynamic> json) {
  return BookVolumesListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<BookVolumesListItem?>? volumes;

  /// 是否含有 Volumes
  bool? get hasVolumes {
    return volumes?.isNotEmpty;
  }

  Results(total, limit, offset, this.volumes)
      : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) => _$Results(srcJson);
}

Results _$Results(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List).map((e) => BookVolumesListItem.fromJson(e)).toList(),
  );
}

class BookVolumesListItem extends Object {
  /// 当前数组的索引号, 但前端并么有用
  int? index;

  /// 章节 ID
  String? id;

  /// 章节的 count, 前端无用
  int? count;

  /// 章节列表的排序序号
  int? sort;

  /// 章节的标题
  String? name;

  /// 章节对应的 book 的 ID
  String? bookId;

  /// 章节对应的 book 的 pathword.
  String? bookPathWord;

  /// 章节对应上一章节
  String? prev;

  /// 章节对应的下一章节
  String? next;

  bool get hasPrev {
    return prev != null;
  }

  bool get hasNoPrev {
    return prev == null;
  }

  bool get hasNoNext {
    return next == null;
  }

  bool get hasNext {
    return next != null;
  }

  BookVolumesListItem(this.index, this.id, this.count, this.sort, this.name,
      this.bookId, this.bookPathWord, this.prev, this.next);

  factory BookVolumesListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$BookVolumesListItem(srcJson);
}

BookVolumesListItem _$BookVolumesListItem(Map<String, dynamic> json) {
  return BookVolumesListItem(
    json['index'],
    json['id'],
    json['count'],
    json['sort'],
    json['name'],
    json['book_id'],
    json['book_path_word'],
    json['prev'],
    json['next'],
  );
}
