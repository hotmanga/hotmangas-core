import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;
import 'package:hotmangasCore/model/filter/filter_item.dart' show FilterItem;

class FilterListEntity extends ResultsEntity {
  Results? results;

  FilterListEntity(code, message, this.results) : super(code, message);

  factory FilterListEntity.fromJson(Map<String, dynamic> srcJson) =>
      $_FilterListEntityFromJson(srcJson);
}

FilterListEntity $_FilterListEntityFromJson(Map<String, dynamic> json) {
  return FilterListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends Object {
  List<FilterItem>? theme;

  List<FilterItem>? female;

  List<FilterItem>? male;

  List<FilterItem>? company;

  List<FilterItem>? cartoonType;

  List<FilterItem>? year;

  List<FilterItem>? ordering;

  /// 地域和已完结
  List<FilterItem>? top;

  /// 写真集标签
  List<FilterItem>? tags;

  // Ordering ordering;

  Results(
    this.theme,
    this.female,
    this.male,
    this.company,
    this.cartoonType,
    this.year,
    this.ordering,
    this.top,
    this.tags,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      $_ResultsFromJson(srcJson);
}

Results $_ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['theme'] == null
        ? null
        : (json['theme'] as List).map((e) => FilterItem.fromJson(e)).toList(),
    json['female'] == null
        ? null
        : (json['female'] as List).map((e) => FilterItem.fromJson(e)).toList(),
    json['male'] == null
        ? null
        : (json['male'] as List).map((e) => FilterItem.fromJson(e)).toList(),
    json['company'] == null
        ? null
        : (json['company'] as List).map((e) => FilterItem.fromJson(e)).toList(),
    json['cartoon_type'] == null
        ? null
        : (json['cartoon_type'] as List)
            .map((e) => FilterItem.fromJson(e))
            .toList(),
    json['year'] == null
        ? null
        : (json['year'] as List).map((e) => FilterItem.fromJson(e)).toList(),
    json['ordering'] == null
        ? null
        : (json['ordering'] as List)
            .map((e) => FilterItem.fromJson(e))
            .toList(),
    json['top'] == null
        ? null
        : (json['top'] as List).map((e) => FilterItem.fromJson(e)).toList(),
    json['tags'] == null
        ? null
        : (json['tags'] as List).map((e) => FilterItem.fromJson(e)).toList(),
  );
}
