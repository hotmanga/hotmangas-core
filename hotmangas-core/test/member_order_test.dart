// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';

void main() {
  test('测试用户获取订单列表', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberOrderListEntity? entity;
    var _limit = 20;
    var _offset = 0;
    await member.order.getList(
        queryParameters: {'limit': _limit, 'offset': _offset},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results?.limit, equals(_limit));
    expect(entity?.results?.offset, equals(_offset));
    expect(entity?.results?.hasOrders, isTrue);
  });
}
