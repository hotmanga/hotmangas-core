// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试书架，获取动画', () async {
    member.login.clearToken();
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberCollectCartoonListEntity? entity;
    await member.collect.getCartoonList(
        queryParameters: {'free_type': 1, 'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });

    expect(entity, isA<MemberCollectCartoonListEntity>());
    expect(entity?.code, equals(200));
    expect(entity?.results?.list, isList);
    expect(entity?.results?.limit, equals(1));
    expect(entity?.results?.offset, equals(0));
  });

  test('测试当前动画是否收藏 未登录用户', () async {
    member.login.clearToken();
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var cartoonPathWord = 'ishuzokureviewers';
    await comic.getCurrentComicCollect(
        pathWord: cartoonPathWord,
        resolve: (data) {
          print(data);
        },
        reject: (data) {
          print(data);
        });
  });

  test('测试当前漫画是否收藏 已登录VIP会员用户 付费漫画 未收藏', () async {
    member.login.clearToken();
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var comicPathWord = 'chiyukisannosaiminappli';
    MemberCollectEntity? entity;
    await cartoon.getCurrentCartoonCollect(
        pathWord: comicPathWord,
        resolve: (data) {
          entity = data;
        },
        reject: (data) {
          print(data);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isA<MemberCollectedItem>());
    expect(entity?.results?.hasCollect, isFalse);
    expect(entity?.results?.collect, equals(-1));
    expect(entity?.results?.isLock, isFalse);
    expect(entity?.results?.isLogin, isTrue);
    expect(entity?.results?.isMobileBind, isTrue);
    expect(entity?.results?.isVip, isTrue);
  });

  test('测试当前漫画是否收藏 已登录VIP会员用户 付费漫画 已收藏', () async {
    member.login.clearToken();
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var comicPathWord = 'milkychucgheji';
    MemberCollectEntity? entity;
    await comic.getCurrentComicCollect(
        pathWord: comicPathWord,
        resolve: (data) {
          entity = data;
        },
        reject: (data) {
          print(data);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isA<MemberCollectedItem>());
    expect(entity?.results?.hasCollect, isTrue);
    expect(entity?.results?.collect, greaterThan(0));
    expect(entity?.results?.isLock, isFalse);
    expect(entity?.results?.isLogin, isTrue);
    expect(entity?.results?.isMobileBind, isTrue);
    expect(entity?.results?.isVip, isTrue);
  });

  test('测试当前漫画是否收藏 已登录VIP会员用户 免费漫画 未收藏', () async {
    member.login.clearToken();
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var comicPathWord = 'nidenayi';
    MemberCollectEntity? entity;
    await comic.getCurrentComicCollect(
        pathWord: comicPathWord,
        resolve: (data) {
          entity = data;
        },
        reject: (data) {
          print(data);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isA<MemberCollectedItem>());
    expect(entity?.results?.hasCollect, isFalse);
    expect(entity?.results?.collect, equals(-1));
    expect(entity?.results?.isLock, isFalse);
    expect(entity?.results?.isLogin, isTrue);
    expect(entity?.results?.isMobileBind, isTrue);
    expect(entity?.results?.isVip, isTrue);
  });

  test('测试当前漫画是否收藏 已登录VIP会员用户 免费漫画 已收藏', () async {
    member.login.clearToken();
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var comicPathWord = 'yiquanchaoren';
    MemberCollectEntity? entity;
    await comic.getCurrentComicCollect(
        pathWord: comicPathWord,
        resolve: (data) {
          entity = data;
        },
        reject: (data) {
          print(data);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isA<MemberCollectedItem>());
    expect(entity?.results?.hasCollect, isTrue);
    expect(entity?.results?.collect, greaterThan(0));
    expect(entity?.results?.isLock, isFalse);
    expect(entity?.results?.isLogin, isTrue);
    expect(entity?.results?.isMobileBind, isTrue);
    expect(entity?.results?.isVip, isTrue);
  });

  /// ----------------------------XXXXX-------------------------
  test('测试当前漫画是否收藏 已登录非VIP会员用户 付费漫画 未收藏', () async {
    member.login.clearToken();
    final userName = 'wxceshi063';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var comicPathWord = 'chuugokudou';
    MemberCollectEntity? entity;
    await comic.getCurrentComicCollect(
        pathWord: comicPathWord,
        resolve: (data) {
          entity = data;
        },
        reject: (data) {
          print(data);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isA<MemberCollectedItem>());
    expect(entity?.results?.hasCollect, isFalse);
    expect(entity?.results?.collect, equals(-1));
    expect(entity?.results?.isLock, isTrue);
    expect(entity?.results?.isLogin, isTrue);
    expect(entity?.results?.isMobileBind, isFalse);
    expect(entity?.results?.isVip, isFalse);
  });

  test('测试当前漫画是否收藏 已登录非VIP会员用户 付费漫画 已收藏', () async {
    member.login.clearToken();
    final userName = 'wxceshi063';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var comicPathWord = 'howtousedolls';
    MemberCollectEntity? entity;
    await comic.getCurrentComicCollect(
        pathWord: comicPathWord,
        resolve: (data) {
          entity = data;
        },
        reject: (data) {
          print(data);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isA<MemberCollectedItem>());
    expect(entity?.results?.hasCollect, isTrue);
    expect(entity?.results?.collect, greaterThan(0));
    expect(entity?.results?.isLock, isTrue);
    expect(entity?.results?.isLogin, isTrue);
    expect(entity?.results?.isMobileBind, isFalse);
    expect(entity?.results?.isVip, isFalse);
  });

  test('测试当前漫画是否收藏 已登录非VIP会员用户 免费漫画 未收藏', () async {
    member.login.clearToken();
    final userName = 'wxceshi063';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var comicPathWord = 'chengweibaihedeespoir';
    MemberCollectEntity? entity;
    await comic.getCurrentComicCollect(
        pathWord: comicPathWord,
        resolve: (data) {
          entity = data;
        },
        reject: (data) {
          print(data);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isA<MemberCollectedItem>());
    expect(entity?.results?.hasCollect, isFalse, reason: '1');
    expect(entity?.results?.collect, equals(-1));
    expect(entity?.results?.isLock, isFalse, reason: '2');
    expect(entity?.results?.isLogin, isTrue);
    expect(entity?.results?.isMobileBind, isFalse, reason: '3');
    expect(entity?.results?.isVip, isFalse, reason: '4');
  });

  test('测试当前漫画是否收藏 已登录非VIP会员用户 免费漫画 已收藏', () async {
    member.login.clearToken();
    final userName = 'wxceshi063';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var comicPathWord = 'kagasantoissho';
    MemberCollectEntity? entity;
    await comic.getCurrentComicCollect(
        pathWord: comicPathWord,
        resolve: (data) {
          entity = data;
        },
        reject: (data) {
          print(data);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isA<MemberCollectedItem>());
    expect(entity?.results?.hasCollect, isTrue);
    expect(entity?.results?.collect, greaterThan(0));
    expect(entity?.results?.isLock, isFalse);
    expect(entity?.results?.isLogin, isTrue);
    expect(entity?.results?.isMobileBind, isFalse);
    expect(entity?.results?.isVip, isFalse);
  });
}
