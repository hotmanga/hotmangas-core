import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        MemberCollectPortraitEntity,
        PortraitChapterContentEntity,
        PortraitChapterEntity,
        PortraitDetailEntity,
        PortraitListEntity,
        PortraitRewardListEntity;
import 'package:hotmangasCore/model/portrait/portrait_puy_entity.dart';
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'mannequin': '',
  'tag': '',
  'ordering': '',
};

final Map<String, dynamic> _queryParametersLimit = {
  'limit': 21,
  'offset': 0,
};

class PortraitApi extends HttpCore {
  /// 获取写真列表
  Future<void> getPortraitList({
    Map<String, dynamic>? queryParameters,
    Function? resolve,
    Function? reject,
  }) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/posts',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(PortraitListEntity.fromJson(data));
        },
        reject: reject);
  }

  ///获取写真详情页
  Future<void> getDetail(
      {@required String? uuid, Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/post/$uuid',
        resolve: (data) {
          resolve?.call(PortraitDetailEntity.fromJson(data));
        },
        reject: reject);
  }

  ///获取写真详情信息-个人相关
  Future<void> getCurrentPortraitCollect(
      {@required String? uuid, Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/post/$uuid/query',
        resolve: (data) {
          resolve?.call(MemberCollectPortraitEntity.fromJson(data));
          // resolve?.call(data);
        },
        reject: reject);
  }

  ///获取写真的章节列表
  Future<void> getChapters(
      {@required String? portraitUuid,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParametersLimit);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/post/$portraitUuid/chapters',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(PortraitChapterEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取写真某个章节的图片
  Future<void> getChapterContent(
      {@required String? portraitUuid,
      @required String? portraitChapterId,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/post/$portraitUuid/chapter/$portraitChapterId',
        resolve: (data) {
          resolve?.call(PortraitChapterContentEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 下载写真某个章节的图片
  Future<void> getChapterContentByDownload(
      {@required String? portraitUuid,
      @required String? portraitChapterId,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/post/$portraitUuid/chapterdownload/$portraitChapterId',
        resolve: (data) {
          resolve?.call(PortraitChapterContentEntity.fromJson(data));
          // resolve?.call(data);
        },
        reject: reject);
  }

  /// 购买写真接口
  Future<void> postPuy(
      {@required String? post_id, Function? resolve, Function? reject}) async {
    await postCore(
        path: '/api/v3/member/post/buy',
        formData: {'post_id': post_id},
        resolve: (data) {
          resolve?.call(PortraitPuyEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取模特打赏列表
  Future<void> getMannequinRewardList(
      {@required String? mannequin,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/mannequin/rewards',
        queryParameters: {
          'mannequin': mannequin,
        },
        resolve: (data) {
          resolve?.call(PortraitRewardListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 打赏模特
  Future<void> postRewardMannequin(
      {@required String? post_id,
      @required String? mannequin,
      @required String? ticket,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/mannequin/reward',
        formData: {
          'post_id': post_id,
          'mannequin': mannequin,
          'ticket': ticket,
        },
        resolve: (data) {
          // resolve?.call(data);
          resolve?.call(PortraitPuyEntity.fromJson(data));
        },
        reject: reject);
  }
}

final portrait = PortraitApi();
