import 'package:hotmangasCore/model/model.dart' show ResultsEntity;

import './common_list_results.dart' show CommonListResults;
import './inner_item.dart' show InnerItem;

class CommonListEntity extends ResultsEntity {
  Results? results;

  CommonListEntity(code, message, this.results) : super(code, message);

  factory CommonListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$CartoonThemeListEntityFromJson(srcJson);
}

CommonListEntity _$CartoonThemeListEntityFromJson(Map<String, dynamic> json) {
  return CommonListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<InnerItem?>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List).map((e) => InnerItem.fromJson(e)).toList(),
  );
}
