import 'dart:math';

import 'package:test/test.dart';

// class ABCRatio {
//   static const int aRatio = 5;
//   static const int bRatio = 3;
//   static const int cRatio = 2;
//   static int aCount = 0;
//   static int bCount = 0;
//   static int cCount = 0;

//   static Iterable<String> getABC(int totalLoops) sync* {
//     final int totalRatio = aRatio + bRatio + cRatio;
//     final int aLimit = (totalLoops * aRatio) ~/ totalRatio;
//     final int bLimit = (totalLoops * bRatio) ~/ totalRatio;
//     final int cLimit = (totalLoops * cRatio) ~/ totalRatio;
//     final Random random = Random();
//     for (int i = 0; i < totalLoops; i++) {
//       final int num = random.nextInt(totalRatio);
//       if (aCount < aLimit && num < aRatio) {
//         aCount++;
//         if (aCount == aLimit) {
//           bCount = 0;
//           cCount = 0;
//         }
//         yield 'a';
//       } else if (bCount < bLimit && num < aRatio + bRatio) {
//         bCount++;
//         if (bCount == bLimit) {
//           aCount = 0;
//           cCount = 0;
//         }
//         yield 'b';
//       } else if (cCount < cLimit && num < aRatio + bRatio + cRatio) {
//         cCount++;
//         if (cCount == cLimit) {
//           aCount = 0;
//           bCount = 0;
//         }
//         yield 'c';
//       } else {
//         i--;
//       }
//     }
//   }
// }
List<String> getABC(int n) {
  final results = List<String>.filled(n, '');
  final aCount = (n * 0.5).floor();
  final bCount = (n * 0.3).floor();
  final cCount = (n * 0.2).floor();
  var aIndex = 0, bIndex = 0, cIndex = 0;

  for (var i = 0; i < results.length; i++) {
    if (aIndex < aCount) {
      results[i] = 'a';
      aIndex++;
    } else if (bIndex < bCount) {
      results[i] = 'b';
      bIndex++;
    } else if (cIndex < cCount) {
      results[i] = 'c';
      cIndex++;
    }
  }

  // Shuffle the results to make the order random
  final random = Random();
  for (var i = results.length - 1; i > 0; i--) {
    final j = random.nextInt(i + 1);
    final temp = results[i];
    results[i] = results[j];
    results[j] = temp;
  }

  return results;
}

//
String getABCOne() {
  final random = Random.secure();

  // Define the desired proportions of each character
  final aWeight = 5;
  final bWeight = 3;
  final cWeight = 2;
  final totalWeight = aWeight + bWeight + cWeight;

  // Generate a random number between 0 and the total weight
  final randomNum = random.nextInt(totalWeight);

  // Select the character based on the random number and weights
  if (randomNum < aWeight) {
    return 'a';
  } else if (randomNum < aWeight + bWeight) {
    return 'b';
  } else {
    return 'c';
  }
}

void main() {
  test('532', () {
    var a = 0;
    var b = 0;
    var c = 0;
    var e = [];
    // e.addAll(getABC(10));
    for (var i = 0; i < 1000; i++) {
      e.add(getABCOne());
    }

    // e.addAll(abc.getNextABCList(16));

    for (var i = 0; i < e.length; i++) {
      if (e[i] == 'a') {
        a++;
      }
      if (e[i] == 'b') {
        b++;
      }
      if (e[i] == 'c') {
        c++;
      }
    }
    print('a:$a,b:$b,c:$c');
  });
}
