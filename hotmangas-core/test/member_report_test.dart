import 'package:hotmangasCore/api/v3/member/member.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试获取吐槽列表', () async {
    MemberCommentCollectListEntity? response;
    await member.roasts.getRoastList(
        queryParameters: {
          'chapter_id': 'cc046314-9fc4-11e8-a46d-00163e0ca5bd',
          'limit': 20,
          'offset': 0,
        },
        resolve: (MemberCommentCollectListEntity entity) {
          response = entity;
          if (response!.results!.list!.isNotEmpty) {
            for (var element in response!.results!.list!) {
              print('${element.id}, ${element.comment}');
            }
          }
        },
        reject: (err) {
          print(err.toString());
        });
    expect(response, isNotNull);
    expect(response?.code, equals(200));
  });

  test('测试用户发布吐槽', () async {
    final userName = 'kbceshi002';
    final passWord = '111111';
    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ResultsEntity? response;
    await member.roasts.createRoast(
        queryParameters: {
          'chapter_id': 'cc046314-9fc4-11e8-a46d-00163e0ca5bd',
          'roast': '测试测试测试', //吐槽内容 3-200
        },
        resolve: (entity) {
          response = entity;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<ResultsEntity>());
    expect(response?.code, equals(200));
  });

  test('测试删除吐槽', () async {
    final userName = 'kbceshi002';
    final passWord = '111111';
    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ResultsEntity? response;
    await member.roasts.delRoast(
        queryParameters: {
          'chapter_id': 'cc046314-9fc4-11e8-a46d-00163e0ca5bd',
          'roast_id': 5062, //吐槽内容 3-200
        },
        resolve: (entity) {
          response = entity;
        },
        reject: (err) {
          print(err.toString());
        });

    expect(response, isA<ResultsEntity>());
    expect(response?.code, equals(200));
  });
}
