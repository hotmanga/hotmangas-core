// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' as api_v3;
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() async {
  test('测试获取线路1', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await api_v3.member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var pathword = 'unitiastdstxzydnsqqshxdtcgjq';
    var uuid = '5916041a-37bc-11eb-b96e-024352452ce0';

    PlayInfoEntity? actual;
    await api_v3.cartoon.getPlayInfo(
        playLineType: PlayLineType.Line1,
        cartoonPathword: pathword,
        cartoonChapterId: uuid,
        resolve: (data) {
          actual = data;
        },
        reject: (error) {
          print(error);
        });

    expect(actual, isA<PlayInfoEntity>());
    expect(actual?.code, equals(200));
    expect(actual?.results?.chapter?.vid, isNull);
    expect(actual?.results?.chapter?.video, isNotNull);
    expect(actual?.results?.chapter?.videoList, isNull);
  });

  test('测试获取线路2', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await api_v3.member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var pathword = 'unitiastdstxzydnsqqshxdtcgjq';
    var uuid = '5916041a-37bc-11eb-b96e-024352452ce0';

    PlayInfoEntity? actual;
    await api_v3.cartoon.getPlayInfo(
        playLineType: PlayLineType.Line2,
        cartoonPathword: pathword,
        cartoonChapterId: uuid,
        resolve: (data) {
          actual = data;
        },
        reject: (error) {
          print(error);
        });

    expect(actual, isA<PlayInfoEntity>());
    expect(actual?.code, equals(200));
    expect(actual?.results?.chapter?.vid, isNotNull);
    expect(actual?.results?.chapter?.video, isNotNull);
    expect(actual?.results?.chapter?.videoList, isNull);
    // expect(actual.results.chapter.videoList, isList);
    // expect(actual.results.chapter.videoList.length, isNonZero);
  });

  test('测试获取线路3', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await api_v3.member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var pathword = 'unitiastdstxzydnsqqshxdtcgjq';
    var uuid = '5916041a-37bc-11eb-b96e-024352452ce0';

    PlayInfoEntity? actual;
    await api_v3.cartoon.getPlayInfo(
        playLineType: PlayLineType.Line3,
        cartoonPathword: pathword,
        cartoonChapterId: uuid,
        resolve: (data) {
          actual = data;
        },
        reject: (error) {
          print(error);
        });

    expect(actual, isA<PlayInfoEntity>(), reason: '1');
    expect(actual?.code, equals(200), reason: '2');
    expect(actual?.results?.chapter?.videoList, isNotNull, reason: '3');
    expect(actual?.results?.chapter?.videoList, isList, reason: '4');
    expect(actual?.results?.chapter?.videoList?.length, isNonZero, reason: '5');
  });
}
