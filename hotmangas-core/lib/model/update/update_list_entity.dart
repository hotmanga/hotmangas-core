import 'package:hotmangasCore/api/utils/datetime_format.dart'
    show datetimeFormat;

import '../core/common/common_list_results.dart' show CommonListResults;
import '../core/common/inner_item.dart' show InnerItem;
import '../core/enmus/content_type.dart' show ContentType, ContentTypeEnumMap;
import '../core/enmus/decode.dart' show enumDecodeNullable;
import '../core/results_entity.dart' show ResultsEntity;

class UpdateListEntity extends ResultsEntity {
  Results? results;

  UpdateListEntity(
    code,
    message,
    this.results,
  ) : super(code, message);

  factory UpdateListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$UpdateListEntityFromJson(srcJson);
}

class Results extends CommonListResults {
  List<UpdateListItem?>? list;

  @override
  bool get hasWrapperItem {
    return true;
  }

  Results(this.list, total, limit, offset, type)
      : super(total, limit, offset, type);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

class UpdateListItem extends Object {
  DateTime? _datetimeCreated;

  /// 代替 displayDatetimeCreated.
  String? get datetimeCreated {
    return datetimeFormat(_datetimeCreated);
  }

  InnerItem? _comic;

  InnerItem? _cartoon;

  InnerItem? get wrapperItem {
    if (_comic != null) {
      return _comic;
    } else if (_cartoon != null) {
      return _cartoon;
    } else {
      return null;
    }
  }

  ContentType get currentType {
    if (_comic != null) {
      return ContentType.Comic;
    } else if (_cartoon != null) {
      return ContentType.Cartoon;
    } else {
      return ContentType.None;
    }
  }

  UpdateListItem(this._datetimeCreated, this._comic, this._cartoon);

  factory UpdateListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$UpdateListItemFromJson(srcJson);
}

UpdateListEntity _$UpdateListEntityFromJson(Map<String, dynamic> json) {
  return UpdateListEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      (json['list'] as List)
          .map((e) => e == null
              ? null
              : UpdateListItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['total'],
      json['limit'],
      json['offset'],
      enumDecodeNullable(ContentTypeEnumMap, json['type']));
}

UpdateListItem _$UpdateListItemFromJson(Map<String, dynamic> json) {
  return UpdateListItem(
      json['datetime_created'] == null
          ? null
          : DateTime.parse(json['datetime_created']),
      json['comic'] == null
          ? null
          : InnerItem.fromJson(json['comic'] as Map<String, dynamic>),
      json['cartoon'] == null
          ? null
          : InnerItem.fromJson(json['cartoon'] as Map<String, dynamic>));
}
