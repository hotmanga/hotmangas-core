import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart'
    show MemberBookBrowserListEntity;
import 'package:test/test.dart';

void main() {
  test('测试获取当前用户轻小说浏览记录', () async {
    final userName = 'hotman1234567';
    final passWord = 'hotman1234567';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberBookBrowserListEntity? response;
    await member.browser.getBooksList(
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<MemberBookBrowserListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list?.length, equals(1));
    expect(response?.results?.list?[0].lastChapterId, equals('4'));
    expect(response?.results?.list?[0].lastChapterName, equals('11111'));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list?[0].book?.uuid,
        equals('be75b54e-58e9-11ed-94c0-024352452ce0'));
    expect(response?.results?.list?[0].book?.status, equals(1));
    expect(response?.results?.list?[0].book?.datetimeUpdated,
        equals('2022-11-01'));
  });

  test('测试用户清空轻小说浏览记录的接口', () async {
    final userName = 'hotman1234567';
    final passWord = 'hotman1234567';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    await member.browser.deleteBooks(resolve: (data) {
      print(data);
    }, reject: (error) {
      print(error);
    });
  });
}
