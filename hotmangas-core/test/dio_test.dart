@Timeout(Duration(seconds: 10))

import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:test/test.dart';

void main() async {
  test('测试DIO', () async {
    await HttpCore().getCore(
        path: '/api/v3/system/config/5000/3',
        queryParameters: {'daio': '123'},
        reject: (value) {
          print(value.toString());
        },
        resolve: (value) {
          print(value.toString());
        });
  });
}
