enum BannerType {
  /// 0  # '图片推荐' will remove
  @deprecated
  Pics,

  /// 1 漫画
  Comics,

  /// 2  # '文字推荐' will remove
  @deprecated
  Words,

  /// 3  # '内链'
  Inline,

  /// 4  # '外链' 用外部浏览器打开主要用来展示友商链接
  Outline,

  /// 5 动画
  Cartoons,

  ///6 # 外链  用内部webView打开 主要展示我们自己的域名
  UrlOutLine,

  ///130 写真
  Portrait,

  ///140  漫画专题
  Subject,

  ///120 轻小说
  Novel,

  ///102 漫画作者
  ComicAuthor,

  ///101 漫画题材
  ComicTheme,

  ///122 轻小说作者
  NovelAuthor,

  ///121 轻小说题材
  NovelTheme,

  ///132 写真模特
  PortraitMannequin,

  ///131 写真标签
  PortraitTag,
}

// 需要修改banner.g.dart
const BannerTypeEnumMap = {
  // BannerType.Pics: 0,
  BannerType.Comics: 1,
  // BannerType.Words: 2,
  BannerType.Inline: 3,
  BannerType.Outline: 4,
  BannerType.Cartoons: 5,
  BannerType.UrlOutLine: 6,
  BannerType.Portrait: 130,
  BannerType.Subject: 140,

  BannerType.Novel: 120,
  BannerType.ComicAuthor: 102,
  BannerType.ComicTheme: 101,
  BannerType.NovelAuthor: 122,
  BannerType.NovelTheme: 121,
  BannerType.PortraitMannequin: 132,
  BannerType.PortraitTag: 131,
};
