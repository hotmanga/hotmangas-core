class MemberRewardShow extends Object {
  // 用戶每日下載次數
  int? dayDownloads;

  // 用戶贊助次數
  int? vipDownloads;

  // 用戶激勵視頻次數
  int? rewardDownloads;

  /// 用户看广告获取的写真赠币
  String? _rewardTicket;

  /// 用户看广告获取的写真赠币
  String? get rewardTicket {
    String? _str = _rewardTicket.toString();
    return _str.split('.')[0].toString();
  }

  ///用户剩余的写真币(付费)
  String? _ticket;

  /// 写真币
  String? get ticket {
    String? _str = _ticket.toString();
    return _str.split('.')[0].toString();
  }

  ///看广告的写真币和购买的写真币总和
  int? get ticketAll {
    return int.parse(ticket!) + int.parse(rewardTicket!);
  }

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'dayDownloads': dayDownloads,
      'vipDownloads': vipDownloads,
      'rewardDownloads': rewardDownloads,
      'rewardTicket': _rewardTicket,
      'ticket': ticket,
    };
  }

  MemberRewardShow(
    this.dayDownloads,
    this.vipDownloads,
    this.rewardDownloads,
    this._rewardTicket,
    this._ticket,
  );

  factory MemberRewardShow.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberInfoFromJson(srcJson);
}

MemberRewardShow _$MemberInfoFromJson(Map<String, dynamic> json) {
  return MemberRewardShow(
    json['day_downloads'] ?? 0,
    json['vip_downloads'] ?? 0,
    json['reward_downloads'] ?? 0,
    json['reward_ticket'] ?? '0',
    json['ticket'] ?? '0',
  );
}
