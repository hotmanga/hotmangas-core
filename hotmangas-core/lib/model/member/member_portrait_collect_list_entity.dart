import 'package:hotmangasCore/model/core/common/author_or_company.dart'
    show AuthorOrCompany;
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberCollectPortraitListEntity extends ResultsEntity {
  Results? results;

  MemberCollectPortraitListEntity(code, message, this.results)
      : super(code, message);

  factory MemberCollectPortraitListEntity.fromJson(
          Map<String, dynamic> srcJson) =>
      _$MemberCollectPortraitListEntityFromJson(srcJson);
}

MemberCollectPortraitListEntity _$MemberCollectPortraitListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberCollectPortraitListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<MemberCollectPortraitItem>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List)
        .map((e) => MemberCollectPortraitItem.fromJson(e))
        .toList(),
  );
}

class MemberCollectPortraitItem extends Object {
  int? id;
  CollectPortraitInnerItem? post;
  MemberCollectPortraitItem(this.id, this.post);

  factory MemberCollectPortraitItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberCollectItemFromJson(srcJson);
}

MemberCollectPortraitItem _$MemberCollectItemFromJson(
    Map<String, dynamic> json) {
  return MemberCollectPortraitItem(
    json['id'],
    json['post'] == null
        ? null
        : CollectPortraitInnerItem.fromJson(json['post']),
  );
}

class CollectPortraitInnerItem extends Object {
  String? uuid;
  bool? bDisplay;
  String? name;

  List<AuthorOrCompany?>? mannequins;
  String? cover;
  int? popular;
  String? datetimeUpdated;

  CollectPortraitInnerItem(
    this.uuid,
    this.bDisplay,
    this.name,
    this.mannequins,
    this.cover,
    this.popular,
    this.datetimeUpdated,
  );

  factory CollectPortraitInnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$CollectInnerItemFromJson(srcJson);
}

CollectPortraitInnerItem _$CollectInnerItemFromJson(Map<String, dynamic> json) {
  return CollectPortraitInnerItem(
    json['uuid'],
    json['b_display'],
    json['name'],
    json['mannequins'] == null
        ? null
        : (json['mannequins'] as List)
            .map((e) => e == null
                ? null
                : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
            .toList(),
    json['cover'],
    json['popular'],
    json['datetime_updated'],
  );
}
