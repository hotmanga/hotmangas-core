import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class SearchHotKeyListEntity extends ResultsEntity {
  Results results;

  SearchHotKeyListEntity(code, message, this.results) : super(code, message);

  factory SearchHotKeyListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$SearchHotKeyListEntityFromJson(srcJson);
}

SearchHotKeyListEntity _$SearchHotKeyListEntityFromJson(
    Map<String, dynamic> json) {
  return SearchHotKeyListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<SearchHotKeyListItem> list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List)
        .map((e) => SearchHotKeyListItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

class SearchHotKeyListItem extends Object {
  String keyword;

  int clicks;

  SearchHotKeyListItem(this.keyword, this.clicks);

  factory SearchHotKeyListItem.fromJson(Map<String, dynamic> srcJson) =>
      _$SearchHotKeyListItemFromJson(srcJson);
}

SearchHotKeyListItem _$SearchHotKeyListItemFromJson(Map<String, dynamic> json) {
  return SearchHotKeyListItem(
    json['keyword'],
    json['clicks'],
  );
}
