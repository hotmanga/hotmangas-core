class SecurityAnswerItem extends Object {
  int? id;

  String? question;

  String? answer;

  // 仅限错误是才有
  String? detail;

  SecurityAnswerItem(this.id, this.question, this.answer, this.detail);

  factory SecurityAnswerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$SecurityAnswerItemFromJson(srcJson);
}

SecurityAnswerItem _$SecurityAnswerItemFromJson(Map<String, dynamic> json) {
  return SecurityAnswerItem(
    json['id'],
    json['question'],
    json['answer'],
    json['detail'],
  );
}
