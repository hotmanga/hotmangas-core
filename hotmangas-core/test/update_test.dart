// Package imports:
// import 'package:test/test.dart';

// Project imports:
// import 'package:hotmangasCore/api/v3/api.dart' show update;

// import 'package:hotmangasCore/model/model.dart'
    // show UpdateListEntity, UpdateType, UpdateTypeEnumMap, ContentType;

// void main() {
  // test('测试获取最近更新接口-默认，返回状态210', () async {
  //   UpdateListEntity? response;
  //   await update.getList(
  //       queryParameters: {},
  //       resolve: (data) {
  //         response = data;
  //       });
  //   expect(response, isA<UpdateListEntity>(), reason: '1');
  //   expect(response!.code, equals(210), reason: '2');
  // });

  // test('测试获取最近更新接口-WeeklyComicFree', () async {
  //   UpdateListEntity? response;
  //   await update.getList(
  //       queryParameters: {
  //         'date': UpdateTypeEnumMap[UpdateType.WeeklyComicFree]
  //       },
  //       resolve: (UpdateListEntity data) {
  //         print(data.results?.list?.first?.datetimeCreated);
  //         response = data;
  //       });
  //   expect(response, isA<UpdateListEntity>());
  //   expect(response!.code, equals(200));
  //   expect(response!.results?.type, ContentType.Comic);
  //   expect(response!.results?.list?.first?.currentType, ContentType.Comic);
  //   expect(response!.results?.list?.first?.datetimeCreated, isNotNull,
  //       reason: '1');
  //   expect(response!.results?.list?.first?.wrapperItem, isNotNull, reason: '2');
  // });

  // test('测试获取最近更新接口-WeeklyComicCharge', () async {
  //   UpdateListEntity? response;
  //   await update.getList(
  //       queryParameters: {
  //         'date': UpdateTypeEnumMap[UpdateType.WeeklyComicCharge]
  //       },
  //       resolve: (data) {
  //         response = data;
  //       });
  //   expect(response, isA<UpdateListEntity>());
  //   expect(response!.code, equals(200));
  //   expect(response!.results?.type, ContentType.Comic);
  //   expect(response!.results?.list?.first?.currentType, ContentType.Comic);
  //   expect(response!.results?.list?.first?.datetimeCreated, isNotNull);
  //   expect(response!.results?.list?.first?.wrapperItem, isNotNull);
  // });

  // test('测试获取最近更新接口-WeeklyCartoon', () async {
  //   UpdateListEntity? response;
  //   await update.getList(
  //       queryParameters: {'date': UpdateTypeEnumMap[UpdateType.WeeklyCartoon]},
  //       resolve: (data) {
  //         response = data;
  //       });
  //   expect(response, isA<UpdateListEntity>());
  //   expect(response!.code, equals(200));
  //   expect(response!.results?.type, ContentType.Cartoon);
  //   expect(response!.results?.list?.first?.currentType, ContentType.Cartoon);
  //   expect(response!.results?.list?.first?.datetimeCreated, isNotNull);
  //   expect(response!.results?.list?.first?.wrapperItem, isNotNull);
  // });
// }
