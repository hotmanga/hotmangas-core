import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class SystemAppVersionInfoEntity extends ResultsEntity {
  Results? results;

  SystemAppVersionInfoEntity(code, message, this.results)
      : super(code, message);

  factory SystemAppVersionInfoEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$SystemAppVersionInfoEntityFromJson(srcJson);
}

SystemAppVersionInfoEntity _$SystemAppVersionInfoEntityFromJson(
    Map<String, dynamic> json) {
  return SystemAppVersionInfoEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends Object {
  SystemAppVersionInfo? android;
  SystemAppVersionInfo? androidTest;
  SystemAppVersionInfo? androidAll;
  SystemAppVersionInfo? ios;

  Results(this.android, this.androidTest, this.androidAll, this.ios);

  factory Results.fromJson(Map<String, dynamic>? srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic>? json) {
  return Results(
    SystemAppVersionInfo.fromJson(json?['android']),
    SystemAppVersionInfo.fromJson(json?['androidTest']),
    SystemAppVersionInfo.fromJson(json?['androidAll']),
    SystemAppVersionInfo.fromJson(json?['ios']),
  );
}

class SystemAppVersionInfo extends Object {
  bool? update;

  String? version;

  String? name;

  String? url;

  bool? forced;

  bool get exist => url != null;

  bool hasNewVersion(String currentVersion) {
    var _hasNewVersion = false;
    var _current = _splitVersion(currentVersion);
    var _lastest = _splitVersion(version!);

    var length =
        _lastest.length < _current.length ? _lastest.length : _current.length;

    for (var i = 0; i < length; i++) {
      if (_lastest[i] > _current[i]) {
        _hasNewVersion = true;
        break;
      } else if (_lastest[i] < _current[i]) {
        break;
      } else {
        // equal
        continue;
      }
    }

    return _hasNewVersion;
  }

  List<int> _splitVersion(String version) {
    return version.split('.').map((e) => int.parse(e)).toList();
  }

  SystemAppVersionInfo(
      this.name, this.update, this.version, this.url, this.forced);

  factory SystemAppVersionInfo.fromJson(Map<String, dynamic>? srcJson) =>
      _$SystemAppVersionInfoFromJson(srcJson)!;
}

SystemAppVersionInfo? _$SystemAppVersionInfoFromJson(
    Map<String, dynamic>? json) {
  return SystemAppVersionInfo(
    json?['name'],
    json?['update'],
    json?['version'],
    json?['url'],
    json?['forced'],
  );
  // if (json != null && json.isNotEmpty) {
  //   return SystemAppVersionInfo(
  //     json['name'],
  //     json['update'],
  //     json['version'],
  //     json['url'],
  //   );
  // } else {
  //   return SystemAppVersionInfo(
  //     json?['name'],
  //     json?['update'],
  //     json?['version'],
  //     json?['url'],
  //   );
  // }
}
