// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' as api_v3;
import 'package:hotmangasCore/api/v4/api.dart' as api_v4;
import 'package:hotmangasCore/model/model.dart'
    show MemberOrderEntity, MemberOrderPayUrlItem;
import 'package:test/test.dart';

void main() {
  test('测试用户获取订单列表', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await api_v3.member.login.login(
      userName: userName,
      passWord: passWord,
    );

    final _good_id = '0cd4c8f4-6d5e-11e9-8c1b-024352452ce0';
    final _channel_id = 'd10e2b6e-fc76-11e9-9896-024352452ce0';

    MemberOrderEntity? actual;

    await api_v4.member.order.createOrder(
        formData: {
          'goods_id': _good_id,
          'channel_id': _channel_id,
          'num': 1,
        },
        resolve: (data) {
          actual = data;
        },
        reject: (error) {
          print(error);
        });

    expect(actual, isA<MemberOrderEntity>());
    expect(actual?.code, equals(200));
    var item = actual?.results;
    expect(item, isA<MemberOrderPayUrlItem>());
    expect(item?.payUrl, isNotEmpty);
  });
}
