import '../enmus/content_type.dart' show ContentType, ContentTypeEnumMap;
import '../enmus/decode.dart' show enumDecodeNullable;

class Topics extends Object {
  List<Topic?>? list;

  Topics(
    this.list,
  );

  factory Topics.fromJson(Map<String, dynamic> srcJson) =>
      _$TopicsFromJson(srcJson);
}

class Topic extends Object {
  String? title;

  String? cover;

  String? pathWord;

  ContentType? type;

  String? datetime_created;

  Topic(
      this.title, this.cover, this.pathWord, this.type, this.datetime_created);

  factory Topic.fromJson(Map<String, dynamic> srcJson) =>
      _$TopicFromJson(srcJson);
}

Topics _$TopicsFromJson(Map<String, dynamic> json) {
  return Topics(
    (json['list'] as List)
        .map(
            (e) => e == null ? null : Topic.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Topic _$TopicFromJson(Map<String, dynamic> json) {
  return Topic(
    json['title'],
    json['cover'],
    json['path_word'],
    enumDecodeNullable(ContentTypeEnumMap, json['type']),
    json['datetime_created'] ?? '',
  );
}
