import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        MemberRewardCreateEntity,
        MemberRewardShowEntity,
        MemberRewardShowTotalEntity;
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'show_id': '',
  'create_at': '', //日期格式 秒级 2022-04-12 17:01:01
  'end_at': '' //日期格式 秒级 2022-04-12 17:02:01
};

class Reward extends HttpCore {
  Future<void> rewardCreate(
      {@required String? createAt, Function? resolve, Function? reject}) async {
    await postCore(
        path: '/api/v3/member/ads/create',
        formData: {'create_at': createAt!},
        queryParameters: null,
        resolve: (data) {
          resolve?.call(MemberRewardCreateEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 请求完毕后展示激励广告
  Future<void> rewardVerify({
    Function? resolve,
    Function? reject,
  }) async {
    await postCore(
      path: '/api/v3/member/ads/verify',
      resolve: (data) {
        resolve?.call(MemberRewardShowEntity.fromJson(data));
      },
      reject: reject,
    );
  }

  /// 请求完毕后展示激励广告（写真赠币）
  Future<void> ticketVerify({
    Function? resolve,
    Function? reject,
  }) async {
    await postCore(
      path: '/api/v3/member/ads/ticketverify',
      resolve: (data) {
        resolve?.call(MemberRewardShowEntity.fromJson(data));
      },
      reject: reject,
    );
  }

  /// 写真激励广告请求次数
  Future<void> ticketVerifyTotal({
    Function? resolve,
    Function? reject,
  }) async {
    await getCore(
      path: '/api/v3/member/ads/ticketview',
      resolve: (data) {
        resolve?.call(MemberRewardShowTotalEntity.fromJson(data));
      },
      reject: reject,
    );
  }

  Future<void> reWardShow(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v3/member/ads/show',
        formData: _,
        queryParameters: null,
        resolve: (data) {
          resolve?.call(MemberRewardShowEntity.fromJson(data));
        },
        reject: reject);
  }
}
