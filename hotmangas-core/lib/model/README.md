<!-- @format -->

## model 说明

model 下所有 XXXentity.dart 都需要生成 XXXentity.g.dart 文件，需要执行以下命令

### json 文件

json 文件是接口返回的例子。数据部分可能有精简。

### \*.dart 文件是什么

\*.dart 文件时 entity 类，\*.g.dart 是 entity 对应的文件。

### \*.dart 怎么来？

[https://caijinglong.github.io/json2dart/index_ch.html]这里可以依靠在线工具生成。

### 生成完报错？

需要运行如下命令，可以生成 \*.g.dart 文件，需要在项目目录，比如该项目为 'dio-full' 下运行。

```bash
pub run build_runner build
JsonKey
```

### 使用的库

```yaml
dependencies:
  json_annotation: ^3.0.1

dev_dependencies:
  build_runner: ^1.8.0
  json_serializable: ^3.2.5
```

### 重点

- 枚举类型会在执行 `pub run build_runner build` 后被替换成原始内容
