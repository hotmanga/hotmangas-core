import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show MemberCommentCollectListEntity, ResultsEntity;

final Map<String, dynamic>? _queryParameters = {
  'chapter_id': '', //章节ID
  'roast': '', //吐槽内容 3-200
};
final Map<String, dynamic>? _getListMap = {
  'chapter_id': '', //用户Id
  'limit': '',
  'offset': '',
};
final Map<String, dynamic>? _delRoast = {
  'chapter_id': '', //章节ID
  'roast_id': '', //吐槽ID
};

class Roasts extends HttpCore {
  /// 创建用户吐槽
  Future<void> createRoast(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters!);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v3/member/roast',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  ///获取用户的吐槽列表
  Future<void> getRoastList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_getListMap!);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/roasts',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberCommentCollectListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> delRoast(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_delRoast!);
    _.addAll(queryParameters!);
    await deleteCore(
        path: '/api/v3/member/roast',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }
}
