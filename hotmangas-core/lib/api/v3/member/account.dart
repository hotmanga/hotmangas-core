import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/core/results_entity.dart';

final Map<String, dynamic>? _queryParameters = {
  'username': '',
  'password': '',
  'mobile': '',
  'region': '',
  'code': '',
  'source': '',
  'version': '',
  'platform': '',
  'invite_code': '',
  'question': '',
  'answer': '',
};

class Account extends HttpCore {
  Future<void> register(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters!);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v3/register',
        formData: _,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }
}
