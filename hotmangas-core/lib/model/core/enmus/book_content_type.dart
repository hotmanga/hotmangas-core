enum BookContentType {
  /// 无
  None,

  /// image
  Image,

  /// text
  Text,
}

@deprecated
const BookContentTypeTypeMap = {
  BookContentType.None: 0,
  BookContentType.Image: 2,
  BookContentType.Text: 1,
};
