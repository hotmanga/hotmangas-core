import 'package:hotmangasCore/model/comic/comic_browse_entity.dart'
    show ComicQueryBrowseEntity;
import 'package:hotmangasCore/model/core/common/author_or_company.dart'
    show AuthorOrCompany;
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/common/inner_item.dart' show InnerItem;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberCollectComicListEntity extends ResultsEntity {
  Results? results;

  MemberCollectComicListEntity(code, message, this.results)
      : super(code, message);

  factory MemberCollectComicListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberCollectComicListEntityFromJson(srcJson);
}

MemberCollectComicListEntity _$MemberCollectComicListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberCollectComicListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<MemberCollectComicItem>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List)
        .map((e) => MemberCollectComicItem.fromJson(e))
        .toList(),
  );
}

class MemberCollectComicItem extends Object {
  int? uuid;

  String? name;

  bool? bFolder;

  CollectComicInnerItem? comic;

  int? folderId;

  lastBrowseItem? lastBrowse;

  bool? isNewChapter() {
    if (lastBrowse == null) {
      return false;
    }
    if (lastBrowse!.lastBrowseName == null) {
      return false;
    }
    if (lastBrowse!.lastBrowseName == comic!.lastChapterName) {
      return false;
    }
    return true;
  }

  MemberCollectComicItem(this.uuid, this.name, this.bFolder, this.folderId,
      this.comic, this.lastBrowse);

  factory MemberCollectComicItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberCollectItemFromJson(srcJson);
}

MemberCollectComicItem _$MemberCollectItemFromJson(Map<String, dynamic> json) {
  return MemberCollectComicItem(
    json['uuid'],
    json['name'],
    json['b_folder'],
    json['folder_id'],
    CollectComicInnerItem.fromJson(json['comic']),
    json['last_browse'] != null
        ? lastBrowseItem.fromJson(json['last_browse'])
        : null,
  );
}

class CollectComicInnerItem extends InnerItem {
  bool? bDisplay;

  int? status;

  String? lastChapterId;

  String? lastChapterName;

  ComicQueryBrowseEntity? browse;

  CollectComicInnerItem(
    name,
    pathWord,
    author,
    company,
    cover,
    popular,
    datetimeUpdated,
    mannequins,
    this.bDisplay,
    this.status,
    this.lastChapterId,
    this.lastChapterName,
    this.browse,
  ) : super(
          name,
          pathWord,
          author,
          company,
          cover,
          popular,
          datetimeUpdated,
          mannequins,
          null,
          null,
          null,
        );

  factory CollectComicInnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$CollectInnerItemFromJson(srcJson);
}

CollectComicInnerItem _$CollectInnerItemFromJson(Map<String, dynamic> json) {
  return CollectComicInnerItem(
    json['name'],
    json['path_word'],
    (json['author'] as List).map((e) => AuthorOrCompany.fromJson(e)).toList(),
    null,
    json['cover'],
    json['popular'],
    json['datetime_updated'] ?? '',
    json['mannequins'] == null
        ? null
        : (json['mannequins'] as List)
            .map((e) => e == null
                ? null
                : AuthorOrCompany.fromJson(e as Map<String, dynamic>))
            .toList(),
    json['b_display'],
    json['status'],
    json['last_chapter_id'],
    json['last_chapter_name'],
    json['browse'] != null
        ? ComicQueryBrowseEntity.fromJson(json['browse'])
        : null,
  );
}

class lastBrowseItem extends Object {
  String? lastBrowseId;
  String? lastBrowseName;
  lastBrowseItem(
    this.lastBrowseId,
    this.lastBrowseName,
  );

  factory lastBrowseItem.fromJson(Map<String, dynamic> srcJson) {
    return _$lastBrowseItemFromJson(srcJson);
  }
}

lastBrowseItem _$lastBrowseItemFromJson(Map<String, dynamic> json) {
  return lastBrowseItem(
    json['last_browse_id'] ?? '',
    json['last_browse_name'] ?? '',
  );
}
