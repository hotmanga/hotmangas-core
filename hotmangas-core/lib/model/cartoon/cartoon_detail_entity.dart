import 'package:hotmangasCore/api/utils/datetime_format.dart';
import 'package:hotmangasCore/model/core/common/items/free_type_item.dart'
    show FreeTypeItem;

class CartoonDetailEntity extends Object {
  int? code;

  String? message;

  Results? results;

  CartoonDetailEntity(
    this.code,
    this.message,
    this.results,
  );

  factory CartoonDetailEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$CartoonDetailEntityFromJson(srcJson);
}

CartoonDetailEntity _$CartoonDetailEntityFromJson(Map<String, dynamic> json) {
  return CartoonDetailEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends Object {
  Cartoon? cartoon;

  int? popular;

  String? collect;

  bool? isLogin;

  bool? isMobileBind;

  bool? isVip;

  bool? isLock;

  Results(
    this.cartoon,
    this.popular,
    this.collect,
    this.isLogin,
    this.isMobileBind,
    this.isVip,
    this.isLock,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['cartoon'] == null
        ? null
        : Cartoon.fromJson(json['cartoon'] as Map<String, dynamic>),
    json['popular'],
    json['collect'] ?? '',
    json['is_login'],
    json['is_mobile_bind'],
    json['is_vip'],
    json['is_lock'],
  );
}

class Cartoon extends Object {
  String? uuid;

  String? pathWord;

  FreeTypeItem? freeType;

  Grade? grade;

  CartoonType? cartoonType;

  Category? category;

  String? name;

  String? cover;

  List<Theme?>? theme;

  List<Females?>? females;

  List<Males?>? males;

  Parodies? parodies;

  Company? company;

  String? years;

  DateTime? _datetimeUpdated;

  LastChapter? lastChapter;

  int? popular;

  bool? bSubtitle;

  String? brief;

  String? get datetimeUpdated {
    return datetimeFormat(_datetimeUpdated);
  }

  Cartoon(
    this.uuid,
    this.pathWord,
    this.freeType,
    this.grade,
    this.cartoonType,
    this.category,
    this.name,
    this.cover,
    this.theme,
    this.females,
    this.males,
    this.parodies,
    this.company,
    this.years,
    this._datetimeUpdated,
    this.lastChapter,
    this.popular,
    this.bSubtitle,
    this.brief,
  );

  factory Cartoon.fromJson(Map<String, dynamic> srcJson) =>
      _$CartoonFromJson(srcJson);
}

Cartoon _$CartoonFromJson(Map<String, dynamic> json) {
  return Cartoon(
    json['uuid'],
    json['path_word'],
    json['free_type'] == null
        ? null
        : FreeTypeItem.fromJson(json['free_type'] as Map<String, dynamic>),
    json['grade'] == null
        ? null
        : Grade.fromJson(json['grade'] as Map<String, dynamic>),
    json['cartoon_type'] == null
        ? null
        : CartoonType.fromJson(json['cartoon_type'] as Map<String, dynamic>),
    json['category'] == null
        ? null
        : Category.fromJson(json['category'] as Map<String, dynamic>),
    json['name'],
    json['cover'],
    (json['theme'] as List)
        .map(
            (e) => e == null ? null : Theme.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['females'] as List)
        .map((e) =>
            e == null ? null : Females.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['males'] as List)
        .map(
            (e) => e == null ? null : Males.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['parodies'] == null
        ? null
        : Parodies.fromJson(json['parodies'] as Map<String, dynamic>),
    json['company'] == null
        ? null
        : Company.fromJson(json['company'] as Map<String, dynamic>),
    json['years'],
    json['datetime_updated'] == null
        ? null
        : DateTime.parse(json['datetime_updated']),
    json['last_chapter'] == null
        ? null
        : LastChapter.fromJson(json['last_chapter'] as Map<String, dynamic>),
    json['popular'],
    json['b_subtitle'],
    json['brief'],
  );
}

class Grade extends Object {
  int? value;

  String? display;

  Grade(
    this.value,
    this.display,
  );

  factory Grade.fromJson(Map<String, dynamic> srcJson) =>
      _$GradeFromJson(srcJson);
}

class CartoonType extends Object {
  int? value;

  String? display;

  CartoonType(
    this.value,
    this.display,
  );

  factory CartoonType.fromJson(Map<String, dynamic> srcJson) =>
      _$CartoonTypeFromJson(srcJson);
}

class Category extends Object {
  int? value;

  String? display;

  Category(
    this.value,
    this.display,
  );

  factory Category.fromJson(Map<String, dynamic> srcJson) =>
      _$CategoryFromJson(srcJson);
}

class Theme extends Object {
  String? name;

  String? pathWord;

  Theme(
    this.name,
    this.pathWord,
  );

  factory Theme.fromJson(Map<String, dynamic> srcJson) =>
      _$ThemeFromJson(srcJson);
}

class Females extends Object {
  String? pathWord;

  int? gender;

  String? name;

  Females(
    this.pathWord,
    this.gender,
    this.name,
  );

  factory Females.fromJson(Map<String, dynamic> srcJson) =>
      _$FemalesFromJson(srcJson);
}

class Males extends Object {
  String? pathWord;

  int? gender;

  String? name;

  Males(
    this.pathWord,
    this.gender,
    this.name,
  );

  factory Males.fromJson(Map<String, dynamic> srcJson) =>
      _$MalesFromJson(srcJson);
}

/// 系列
class Parodies extends Object {
  String? name;

  /// 翻译名
  String? transName;

  String? pathWord;

  Parodies(
    this.name,
    this.transName,
    this.pathWord,
  );

  @override
  String toString() => name!;

  factory Parodies.fromJson(Map<String, dynamic> srcJson) =>
      _$ParodiesFromJson(srcJson);
}

class Company extends Object {
  String? name;

  String? pathWord;

  Company(
    this.name,
    this.pathWord,
  );

  factory Company.fromJson(Map<String, dynamic> srcJson) =>
      _$CompanyFromJson(srcJson);
}

class LastChapter extends Object {
  String? uuid;

  String? name;

  bool get isEmpty {
    return uuid!.isEmpty && name!.isEmpty;
  }

  LastChapter(
    this.uuid,
    this.name,
  );

  factory LastChapter.fromJson(Map<String, dynamic> srcJson) =>
      _$LastChapterFromJson(srcJson);
}

Grade _$GradeFromJson(Map<String, dynamic> json) {
  return Grade(
    json['value'],
    json['display'],
  );
}

CartoonType _$CartoonTypeFromJson(Map<String, dynamic> json) {
  return CartoonType(
    json['value'],
    json['display'],
  );
}

Category _$CategoryFromJson(Map<String, dynamic> json) {
  return Category(
    json['value'],
    json['display'],
  );
}

Theme _$ThemeFromJson(Map<String, dynamic> json) {
  return Theme(
    json['name'],
    json['path_word'],
  );
}

Females _$FemalesFromJson(Map<String, dynamic> json) {
  return Females(
    json['path_word'],
    json['gender'],
    json['name'],
  );
}

Males _$MalesFromJson(Map<String, dynamic> json) {
  return Males(
    json['path_word'],
    json['gender'],
    json['name'],
  );
}

Parodies _$ParodiesFromJson(Map<String, dynamic> json) {
  return Parodies(
    json['name'],
    json['trans_name'] ?? '',
    json['path_word'],
  );
}

Company _$CompanyFromJson(Map<String, dynamic> json) {
  return Company(
    json['name'],
    json['path_word'],
  );
}

LastChapter _$LastChapterFromJson(Map<String, dynamic> json) {
  return LastChapter(
    json['uuid'],
    json['name'],
  );
}
