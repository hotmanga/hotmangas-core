enum RestrictType {
  /// 一般向
  Normal,

  /// 擦边级
  Sensitive,

  /// 裸露级
  Bare,

  /// 情色级
  Erotic,

  /// 血腥级
  Bloody,
}

const RestrictTypeEnumType = {
  RestrictType.Normal: 0,
  RestrictType.Sensitive: 1,
  RestrictType.Bare: 2,
  RestrictType.Erotic: 3,
  RestrictType.Bloody: 4,
};
