import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;
import 'package:hotmangasCore/model/member/member_cartoon_collect_list_entity.dart'
    show CollectCartoonInnerItem;

class DiscoverFreeCartoonListEntity extends ResultsEntity {
  Results? results;

  DiscoverFreeCartoonListEntity(code, message, this.results)
      : super(code, message);

  factory DiscoverFreeCartoonListEntity.fromJson(
          Map<String, dynamic> srcJson) =>
      _$DiscoverFreeCartoonListEntityFromJson(srcJson);
}

DiscoverFreeCartoonListEntity _$DiscoverFreeCartoonListEntityFromJson(
    Map<String, dynamic> json) {
  return DiscoverFreeCartoonListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<CollectCartoonInnerItem?>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List)
        .map((e) => e == null
            ? null
            : CollectCartoonInnerItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}
