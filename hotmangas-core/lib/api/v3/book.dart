import 'package:meta/meta.dart' show required;

import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        FilterBookListEntity,
        BookDetailEntity,
        BookVolumeEntity,
        MemberCollectEntityV2,
        BookVolumesListEntity;

final Map<String, dynamic> _queryParameters = {
  'ordering': '-datetime_updated',
  'limit': 21,
  'offset': 0,
};

class Book extends HttpCore {
  /// 获取轻小说详情页
  Future<void> getDetail({
    @required String? pathWord,
    Function? resolve,
    Function? reject,
  }) async {
    await getCore(
        path: '/api/v3/book/$pathWord',
        resolve: (data) {
          resolve?.call(BookDetailEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 检索轻小说, 可选参数有: theme, ordering, author, parodies
  Future<void> getQueryList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/books',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(FilterBookListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取当前轻小说的收藏状态, 浏览状态, 和当前用户的是否 VIP, 是否登录等基本状态.
  Future<void> getCurrentBookCollect(
      {@required String? pathWord, Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/book/$pathWord/query',
        resolve: (data) {
          resolve?.call(MemberCollectEntityV2.fromJson(data));
        },
        reject: reject);
  }

  /// 获取当前轻小说的所有章节
  Future<void> getVolumes(
      {@required String? pathWord, Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/book/$pathWord/volumes',
        resolve: (data) {
          resolve?.call(BookVolumesListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取某个轻小说的指定章节的具体信息, 主要是为了获取 txt
  Future<void> getVolumeContents({
    @required String? pathWord,
    @required String? volumeId,
    Function? resolve,
    Function? reject,
  }) async {
    await getCore(
      path: '/api/v3/book/$pathWord/volume/$volumeId',
      resolve: (data) {
        resolve?.call(BookVolumeEntity.fromJson(data));
      },
      reject: reject,
    );
  }
}

final book = Book();
