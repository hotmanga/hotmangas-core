import '../core/common/common_list_results.dart' show CommonListResults;
import '../core/common/home_index_topic.dart' show Topic;
import '../core/enmus/content_type.dart' show ContentTypeEnumMap;
import '../core/enmus/decode.dart' show enumDecodeNullable;
import '../core/results_entity.dart' show ResultsEntity;

class TopicListEntity extends ResultsEntity {
  Results? results;

  TopicListEntity(
    code,
    message,
    this.results,
  ) : super(code, message);

  factory TopicListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$TopicListEntityFromJson(srcJson);
}

class Results extends CommonListResults {
  List<Topic?>? list;

  Results(this.list, total, limit, offset, type)
      : super(total, limit, offset, type);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

TopicListEntity _$TopicListEntityFromJson(Map<String, dynamic> json) {
  return TopicListEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      (json['list'] as List)
          .map((e) =>
              e == null ? null : Topic.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['total'],
      json['limit'],
      json['offset'],
      enumDecodeNullable(ContentTypeEnumMap, json['type']));
}
