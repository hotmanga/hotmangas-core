import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class GoodListEntity extends ResultsEntity {
  Results? results;

  GoodListEntity(code, message, this.results) : super(code, message);

  factory GoodListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$GoodListEntity(srcJson);
}

GoodListEntity _$GoodListEntity(Map<String, dynamic> json) {
  return GoodListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends Object {
  Goods? goods;

  bool? show;

  String? code;

  Results(this.show, this.code, this.goods);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['show'],
    json['code'],
    Goods.fromJson(json['goods']),
  );
}

class Goods extends Object {
  List<GoodItem>? vip10;

  List<GoodItem>? vip20;

  List<GoodItem>? vip30;

  List<GoodItem>? vip60;

  /// 写真币
  List<GoodItem>? vip61;

  /// 写真订阅会员
  List<GoodItem>? vip62;

  /// google写真币
  List<GoodItem>? vip71;

  Goods(this.vip10, this.vip20, this.vip30, this.vip60, this.vip61, this.vip71,
      this.vip62);

  factory Goods.fromJson(Map<String, dynamic> srcJson) =>
      _$GoodsFromJson(srcJson);
}

Goods _$GoodsFromJson(Map<String, dynamic> json) {
  return Goods(
    json['10'] == null
        ? []
        : (json['10'] as List).map((e) => GoodItem.fromJson(e)).toList(),
    json['20'] == null
        ? []
        : (json['20'] as List).map((e) => GoodItem.fromJson(e)).toList(),
    json['30'] == null
        ? []
        : (json['30'] as List).map((e) => GoodItem.fromJson(e)).toList(),
    json['60'] == null
        ? []
        : (json['60'] as List).map((e) => GoodItem.fromJson(e)).toList(),
    json['61'] == null
        ? []
        : (json['61'] as List).map((e) => GoodItem.fromJson(e)).toList(),
    json['71'] == null
        ? []
        : (json['71'] as List).map((e) => GoodItem.fromJson(e)).toList(),
    json['62'] == null
        ? []
        : (json['62'] as List).map((e) => GoodItem.fromJson(e)).toList(),
  );
}

class GoodItem extends Object {
  String? uuid;

  String? name;

  /// 赞助免广告时间
  int? number;

  /// 赞助下载次数
  int? number2;

  /// 写真币
  String? _number3;

  ///写真币
  String get number3 {
    if (_number3 == '0.00') {
      return '0';
    }
    return _number3!.substring(0, _number3!.indexOf('.'));
  }

  int? vipType;

  String? priceOrig;

  ///价格
  String? price;

  String? dollar;

  String? dollar_orig;

  String? monthName;

  int? channels;

  String? brief;

  ///提示文案
  String? ticketStr;

  GoodItem(
    this.uuid,
    this.name,
    this.number,
    this.number2,
    this._number3,
    this.vipType,
    this.priceOrig,
    this.price,
    this.dollar,
    this.dollar_orig,
    this.monthName,
    this.channels,
    this.brief,
    this.ticketStr,
  );

  factory GoodItem.fromJson(Map<String, dynamic>? srcJson) =>
      _$GoodItemFromJson(srcJson);
}

GoodItem _$GoodItemFromJson(Map<String, dynamic>? json) {
  return GoodItem(
    json?['uuid'],
    json?['name'],
    json?['num'],
    json?['num2'],
    json?['num3'],
    json?['vip_type'],
    json?['price_orig'],
    json?['price'],
    json?['dollar'],
    json?['dollar_orig'],
    json?['month_name'],
    json?['channels'],
    json?['brief'],
    json?['ticket_str'],
  );
}
