import 'package:hotmangasCore/model/core/common/inner_item.dart' show InnerItem;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

import '../core/common/common_list_results.dart' show CommonListResults;

class TopicsDetailListEntity extends ResultsEntity {
  Results? results;

  TopicsDetailListEntity(code, message, this.results) : super(code, message);

  factory TopicsDetailListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$TopicsDetailListEntityFromJson(srcJson);
}

TopicsDetailListEntity _$TopicsDetailListEntityFromJson(
    Map<String, dynamic> json) {
  return TopicsDetailListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<InnerItem>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List).map((e) => InnerItem.fromJson(e)).toList(),
  );
}
