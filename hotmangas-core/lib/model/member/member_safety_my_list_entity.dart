import '../model.dart' show ResultsEntity;
import 'member_security_answer_item.dart' show SecurityAnswerItem;

class MemberSafetyMyListEntity extends ResultsEntity {
  List<SecurityAnswerItem>? results;

  MemberSafetyMyListEntity(code, message, this.results) : super(code, message);

  factory MemberSafetyMyListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberSafetyMyListEntityFromJson(srcJson);
}

MemberSafetyMyListEntity _$MemberSafetyMyListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberSafetyMyListEntity(
    json['code'],
    json['message'],
    (json['results'] as List)
        .map((e) => SecurityAnswerItem.fromJson(e))
        .toList(),
  );
}
