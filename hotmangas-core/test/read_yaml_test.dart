import 'dart:io';

import 'package:hotmangasCore/hot/dio_warpper.dart';
import 'package:test/expect.dart';
import 'package:test/scaffolding.dart';
import 'package:yaml/yaml.dart';

void main() {
  test('readYaml', () async {
    var file = File('./pubspec.yaml');
    String? version;
    await file.readAsString().then((String content) {
      Map yaml = loadYaml(content);
      print(yaml['version']);
      version = yaml['version'];
    });

    expect(version, equals('1.4.5'));
  });

  test('Lanucher', () async {
    await HttpCore().getCore(
        path:
            'https://mapi.hotmangasg.com:12001/api/v3/topic/laosiji20/contents?limit=20&offset=20',
        queryParameters: {'flutter': '123'},
        reject: (value) {
          print(value.toString());
        },
        resolve: (value) {
          print(value.toString());
        });
  });
}
