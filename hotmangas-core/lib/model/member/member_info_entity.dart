import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;
import 'package:hotmangasCore/model/member/member_info.dart' show MemberInfo;

class MemberInfoEntity extends ResultsEntity {
  MemberInfo? results;

  MemberInfoEntity(code, message, this.results) : super(code, message);

  factory MemberInfoEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberInfoEntityFromJson(srcJson);
}

MemberInfoEntity _$MemberInfoEntityFromJson(Map<String, dynamic> json) {
  return MemberInfoEntity(
      json['code'], json['message'], MemberInfo.fromJson(json['results']));
}
