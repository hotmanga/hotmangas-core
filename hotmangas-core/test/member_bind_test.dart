import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() async {
  test('测试绑定InviteCode', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    final inviteCode = '2DOCX';
    MemberInfoEntity? actual;
    await member.bind.bindInviteCode(
        inviteCode: inviteCode,
        resolve: (data) {
          actual = data;
        },
        reject: (error) {
          print(error);
        });

    expect(actual, isA<MemberInfoEntity>());
    expect(actual?.code, equals(200));
    final memberInfo = actual?.results;
    expect(memberInfo, isA<MemberInfo>());
    expect(memberInfo?.invited, equals(inviteCode));
  });
}
