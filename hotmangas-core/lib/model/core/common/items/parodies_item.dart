@deprecated
class ParodiesItem extends Object {
  String? pathWord;

  String? name;

  ParodiesItem(this.pathWord, this.name);

  factory ParodiesItem.fromJson(Map<String, dynamic> srcJson) =>
      _$ParodiesFromJson(srcJson);
}

@deprecated
ParodiesItem _$ParodiesFromJson(Map<String, dynamic> json) {
  return ParodiesItem(
    json['path_word'],
    json['name'],
  );
}
