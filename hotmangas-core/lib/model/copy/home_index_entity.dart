import 'package:hotmangasCore/model/model.dart'
    show
        ResultsEntity,
        Banners,
        CommonListItemComic,
        CommonListComic,
        CommonPageComic,
        Topics;

class CopyHomeIndexEntity extends ResultsEntity {
  Results? results;

  CopyHomeIndexEntity(code, message, this.results) : super(code, message);

  factory CopyHomeIndexEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$HomeIndexEntityFromJson(srcJson);
}

class Results extends Object {
  List<Banners?>? banners;

  Topics? topics;

  List<CommonListItemComic?>? hotComics;

  List<CommonListItemComic?>? newComics;

  CommonListComic? recComics;

  CommonPageComic? finishComics;

  CommonListComic? rankDayComics;

  CommonListComic? rankWeekComics;

  CommonListComic? rankMonthComics;
  Topics? topicsList;

  Results(
    this.banners,
    this.topics,
    this.recComics,
    this.hotComics,
    this.newComics,
    this.finishComics,
    this.rankDayComics,
    this.rankWeekComics,
    this.rankMonthComics,
    this.topicsList,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

CopyHomeIndexEntity _$HomeIndexEntityFromJson(Map<String, dynamic> json) {
  return CopyHomeIndexEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    (json['banners'] as List)
        .map((e) =>
            e == null ? null : Banners.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['topics'] == null
        ? null
        : Topics.fromJson(json['topics'] as Map<String, dynamic>),
    json['recComics'] == null
        ? null
        : CommonListComic.fromJson(json['recComics'] as Map<String, dynamic>),
    (json['hotComics'] as List)
        .map((e) => e == null
            ? null
            : CommonListItemComic.fromJson(e as Map<String, dynamic>))
        .toList(),
    (json['newComics'] as List)
        .map((e) => e == null
            ? null
            : CommonListItemComic.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['finishComics'] == null
        ? null
        : CommonPageComic.fromJson(
            json['finishComics'] as Map<String, dynamic>),
    json['rankDayComics'] == null
        ? null
        : CommonListComic.fromJson(
            json['rankDayComics'] as Map<String, dynamic>),
    json['rankWeekComics'] == null
        ? null
        : CommonListComic.fromJson(
            json['rankWeekComics'] as Map<String, dynamic>),
    json['rankMonthComics'] == null
        ? null
        : CommonListComic.fromJson(
            json['rankMonthComics'] as Map<String, dynamic>),
    json['topicsList'] == null
        ? null
        : Topics.fromJson(json['topicsList'] as Map<String, dynamic>),
  );
}
