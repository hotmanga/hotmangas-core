// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/member/member.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试获取安全问题列表', () async {
    MemberSafetyListEntity? response;
    await member.safety.getList(resolve: (entity) {
      response = entity;
    }, reject: (error) {
      print(error);
    });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results, isList);
    expect(response?.results?.length, equals(4));
  });

  test('测试获取我的安全问题, ', () async {
    final userName = 'wxceshi047';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberSafetyMyListEntity? response;
    await member.safety.getMyList(resolve: (entity) {
      response = entity;
    }, reject: (error) {
      print(error);
    });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results, isList);
    expect(response?.results?.length, equals(1));
    expect(response?.results?.first, isNotNull);
  });

  test('测试修改问题，登录修改自己的问题', () async {
    final userName = 'wxceshi047';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var _id = 3;
    var _question = '我的父亲(母亲)叫什么？';
    var _answer = '123';

    MemberSecurityAnswerEntity? response;
    await member.safety.postSecurityAnwser(
        id: _id,
        question: _question,
        answer: _answer,
        resolve: (entity) {
          response = entity;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results, isA<SecurityAnswerItem>());
    expect(response?.results?.id, equals(_id));
    expect(response?.results?.question, equals(_question));
    expect(response?.results?.answer, equals(_answer));
    expect(response?.results?.answer, isNull);
  });

  test('测试修改问题，登录但修改不是自己的问题', () async {
    final userName = 'wxceshi047';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    var _id = 1;
    var _question = '我的父亲(母亲)叫什么？';
    var _answer = '123';

    MemberSecurityAnswerEntity? response;
    await member.safety.postSecurityAnwser(
        id: _id,
        question: _question,
        answer: _answer,
        resolve: (entity) {
          response = entity;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(210));
    expect(response?.results, isA<SecurityAnswerItem>());
    expect(response?.results?.id, isNull);
    expect(response?.results?.question, isNull);
    expect(response?.results?.answer, isNull);
    expect(response?.results?.detail, isNotNull);
  });
}
