class FreeTypeItem extends Object {
  int? value;

  String? display;

  FreeTypeItem(this.value, this.display);

  factory FreeTypeItem.fromJson(Map<String, dynamic> srcJson) =>
      _$FreeTypeItemFromJson(srcJson);
}

FreeTypeItem _$FreeTypeItemFromJson(Map<String, dynamic> json) {
  return FreeTypeItem(
    json['value'],
    json['display'],
  );
}
