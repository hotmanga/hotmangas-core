import '../enmus/content_type.dart' show ContentType, ContentTypeEnumMap;
import '../enmus/decode.dart' show enumDecodeNullable;

class CommonListResults extends Object {
  int? total;

  int? limit;

  int? offset;

  ContentType? type;

  bool? get hasWrapperItem {
    return false;
  }

  CommonListResults(this.total, this.limit, this.offset, this.type);

  factory CommonListResults.fromJson(Map<String, dynamic> srcJson) =>
      _$CommonListResultsFromJson(srcJson);
}

CommonListResults _$CommonListResultsFromJson(Map<String, dynamic> json) {
  return CommonListResults(
    json['total'],
    json['limit'],
    json['offset'],
    enumDecodeNullable(ContentTypeEnumMap, json['type']),
  );
}
