import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class GoodEntity extends ResultsEntity {
  Results? results;

  GoodEntity(code, message, this.results) : super(code, message);

  factory GoodEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$GoodEntity(srcJson);
}

GoodEntity _$GoodEntity(Map<String, dynamic> json) {
  return GoodEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends Object {
  String? uuid;

  String? name;

  String? priceOrig;

  String? price;

  List<Channel>? channels;

  Results(this.uuid, this.name, this.priceOrig, this.price, this.channels);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['uuid'],
    json['name'],
    json['price_orig'],
    json['price'],
    (json['channels'] as List)
        .map((e) => Channel.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

class Channel extends Object {
  String? uuid;

  String? name;

  ChannelItem? channelItem;

  WayItem? wayItem;

  Channel(this.uuid, this.name, this.channelItem, this.wayItem);

  factory Channel.fromJson(Map<String, dynamic> srcJson) =>
      _$ChannelFromJson(srcJson);
}

Channel _$ChannelFromJson(Map<String, dynamic> json) {
  return Channel(
    json['uuid'],
    json['name'],
    ChannelItem.fromJson(json['channel']),
    WayItem.fromJson(json['way']),
  );
}

class ChannelItem extends Object {
  int? value;

  String? display;

  ChannelItem(this.value, this.display);

  factory ChannelItem.fromJson(Map<String, dynamic> srcJson) =>
      _$ChannelItemFromJson(srcJson);
}

ChannelItem _$ChannelItemFromJson(Map<String, dynamic> json) {
  return ChannelItem(
    json['value'],
    json['display'],
  );
}

class WayItem extends Object {
  int? value;

  String? display;

  WayItem(this.value, this.display);

  factory WayItem.fromJson(Map<String, dynamic> srcJson) =>
      _$WayItemFromJson(srcJson);
}

WayItem _$WayItemFromJson(Map<String, dynamic> json) {
  return WayItem(
    json['value'],
    json['display'],
  );
}
