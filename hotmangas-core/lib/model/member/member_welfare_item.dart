class MemberWelfareItem extends Object {
  String? uuid;

  String? name;

  WelfareReferenceType? refType;

  WelfareGettingType? getType;

  String? path;

  String? dateTimeStarted;

  String? dateTimeEnded;

  /// 支持平台
  List<WelfarePlatform>? platform;

  /// 简介、说明
  String? brief;

  /// 详细介绍，详细说明，详情页需要的，列表页一般返回的是个空字符串。
  String? intro;

  bool isSupportedCurrentPlatfrom(int platformNum) {
    var isSupported = false;

    platform?.forEach((element) {
      if (element.platform == platformNum) {
        isSupported = true;
      }
    });
    return isSupported;
  }

  String get displayStartedToEnded {
    if (dateTimeStarted == null && dateTimeEnded == null) {
      return '永久有效';
    } else {
      return '$dateTimeStarted ~ $dateTimeEnded';
    }
  }

  MemberWelfareItem(
      this.uuid,
      this.name,
      this.refType,
      this.getType,
      this.path,
      this.dateTimeStarted,
      this.dateTimeEnded,
      this.platform,
      this.brief,
      this.intro);

  factory MemberWelfareItem.fromJson(Map<String, dynamic> srcJson) =>
      _$WelfareItemFromJson(srcJson);
}

MemberWelfareItem _$WelfareItemFromJson(Map<String, dynamic> json) {
  return MemberWelfareItem(
    json['uuid'],
    json['name'],
    WelfareReferenceType.fromJson(json['ref_type']),
    WelfareGettingType.fromJson(json['get_type']),
    json['path'],
    json['datetime_started'],
    json['datetime_ended'],
    (json['platform'] as List).map((e) => WelfarePlatform.fromJson(e)).toList(),
    json['brief'],
    json['intro'],
  );
}

class WelfareReferenceType extends Object {
  int? value;
  String? display;

  WelfareReferenceType(this.value, this.display);

  factory WelfareReferenceType.fromJson(Map<String, dynamic> srcJson) =>
      _$WelfareReferenceTypeFromJson(srcJson);
}

WelfareReferenceType _$WelfareReferenceTypeFromJson(Map<String, dynamic> json) {
  return WelfareReferenceType(
    json['value'],
    json['display'],
  );
}

class WelfareGettingType extends Object {
  int? value;
  String? display;

  WelfareGettingType(this.value, this.display);

  factory WelfareGettingType.fromJson(Map<String, dynamic> srcJson) =>
      _$WelfareGettingTypeFromJson(srcJson);
}

WelfareGettingType _$WelfareGettingTypeFromJson(Map<String, dynamic> json) {
  return WelfareGettingType(
    json['value'],
    json['display'],
  );
}

class WelfarePlatform extends Object {
  int? platform;

  WelfarePlatform(this.platform);

  factory WelfarePlatform.fromJson(Map<String, dynamic> srcJson) =>
      _$WelfarePlatformFromJson(srcJson);
}

WelfarePlatform _$WelfarePlatformFromJson(Map<String, dynamic> json) {
  return WelfarePlatform(json['platform']);
}
