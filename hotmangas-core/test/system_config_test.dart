// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show system;
import 'package:hotmangasCore/model/model.dart' show SystemConfigEntity;

void main() {
  test('测试系统配置 300', () async {
    SystemConfigEntity? response;
    await system.config.get300(resolve: (data) {
      response = data;
      print(response);
    });
    expect(response, isA<SystemConfigEntity>());
    expect(response!.code, equals(200));
    // expect(response!.message, equals('Success'));
  });

  test('测试系统配置 1000', () async {
    SystemConfigEntity? response;
    await system.config.get1000(resolve: (data) {
      response = data;
      print(response);
    });
    expect(response, isA<SystemConfigEntity>());
    expect(response!.code, equals(200));
    // expect(response!.message, equals('Success'));
  });

  test('测试系统配置 1001', () async {
    SystemConfigEntity? response;
    await system.config.get1001(resolve: (data) {
      response = data;
      print(response);
    });
    expect(response, isA<SystemConfigEntity>());
    expect(response!.code, equals(200));
    // expect(response!.message, equals('Success'));
  });

  test('测试系统配置 2000', () async {
    SystemConfigEntity? response;
    await system.config.get2000(resolve: (data) {
      response = data;
      print(response);
    });
    expect(response, isA<SystemConfigEntity>());
    expect(response!.code, equals(200));
    // expect(response!.message, equals('Success'));
  });

  test('测试系统配置 2020', () async {
    SystemConfigEntity? response;
    await system.config.get2020(resolve: (data) {
      response = data;
      print(response);
    });
    expect(response, isA<SystemConfigEntity>());
    expect(response!.code, equals(200));
    // expect(response!.message, equals('Success'));
  });

  test('测试系统配置 30001', () async {
    SystemConfigEntity? response;
    await system.config.get30001(resolve: (data) {
      response = data;
      print(response);
    });
    expect(response, isA<SystemConfigEntity>());
    expect(response!.code, equals(200));
    // expect(response!.message, equals('Success'));
  });
}
