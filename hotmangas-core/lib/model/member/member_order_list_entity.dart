import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberOrderListEntity extends ResultsEntity {
  Results? results;

  MemberOrderListEntity(code, message, this.results) : super(code, message);

  factory MemberOrderListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberOrderListEntityFromJson(srcJson);
}

MemberOrderListEntity _$MemberOrderListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberOrderListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<MemberOrderItem>? list;

  bool get hasOrders {
    return list!.isNotEmpty;
  }

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List)
        .map((e) => MemberOrderItem.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

class MemberOrderItem extends Object {
  String? no;

  String? goodsName;

  int? goodsNumber;

  OrderStatus? status;

  Way? way;

  String? priceTotal;

  String? pricePay;

  String? dateTimePay;

  String? dateTimeCreated;

  Way? vipType;

  String? ticket;

  String? ticketStr;

  bool get hasPaied {
    return status!.value == 1;
  }

  MemberOrderItem(
    this.no,
    this.goodsName,
    this.goodsNumber,
    this.status,
    this.way,
    this.priceTotal,
    this.pricePay,
    this.dateTimePay,
    this.dateTimeCreated,
    this.ticket,
    this.ticketStr,
    this.vipType,
  );

  factory MemberOrderItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberOrderItemFromJson(srcJson);
}

MemberOrderItem _$MemberOrderItemFromJson(Map<String, dynamic> json) {
  return MemberOrderItem(
    json['no'],
    json['goods_name'],
    json['goods_num'],
    OrderStatus.fromJson(json['status']),
    Way.fromJson(json['way']),
    json['price_total'],
    json['price_pay'],
    json['datetime_pay'],
    json['datetime_created'] ?? json['create_at'],
    json['ticket'] ?? '',
    json['ticket_str'] ?? '',
    Way.fromJson(json['vip_type']),
  );
}

class OrderStatus extends Object {
  int? value;

  String? display;

  OrderStatus(this.value, this.display);

  factory OrderStatus.fromJson(Map<String, dynamic> srcJson) =>
      _$OrderStatusFromJson(srcJson);
}

OrderStatus _$OrderStatusFromJson(Map<String, dynamic> json) {
  return OrderStatus(
    json['value'],
    json['display'],
  );
}

class Way extends Object {
  int? value;

  String? display;

  Way(this.value, this.display);

  factory Way.fromJson(Map<String, dynamic> srcJson) =>
      _$OrderPlatformFromJson(srcJson);
}

Way _$OrderPlatformFromJson(Map<String, dynamic> json) {
  /// way参数变更，已经变更为int。
  return Way(
    json['value'],
    json['display'],
  );
}
