/// 排行榜的类型
enum ContentRankType {
  None,
  FreeComic,
  ChargeComic,
  FreeCartoon,
  ChargeCartoon,

  /// 轻小说
  Book,

  ///写真
  Portrait
}

const ContentRankTypeEnumMap = {
  ContentRankType.None: 0,
  ContentRankType.FreeComic: 1,
  ContentRankType.ChargeComic: 2,
  ContentRankType.FreeCartoon: 3,
  ContentRankType.ChargeCartoon: 4,

  /// 轻小说
  ContentRankType.Book: 5,

  ///写真
  ContentRankType.Portrait: 6,
};

enum ContentRankDateType { Error, Day, Week, Month, Total }

/// 调用接口传值用。
const ContentRankDateTypeEnumMap = {
  ContentRankDateType.Error: 'error',
  ContentRankDateType.Day: 'day',
  ContentRankDateType.Week: 'week',
  ContentRankDateType.Month: 'month',
  ContentRankDateType.Total: 'total',
};

/// JSON反射时用，必须是 int 类型。
const ContentRankDateTypeEnumMapInt = {
  ContentRankDateType.Error: 0,
  ContentRankDateType.Day: 1,
  ContentRankDateType.Week: 2,
  ContentRankDateType.Month: 3,
  ContentRankDateType.Total: 4,
};
