class CountItem extends Object {
  String? name;

  String? pathWord;

  int? count;

  CountItem(this.name, this.pathWord, this.count);

  factory CountItem.fromJson(Map<String, dynamic> srcJson) =>
      _$CountItemFromJson(srcJson);
}

CountItem _$CountItemFromJson(Map<String, dynamic> json) {
  return CountItem(
    json['name'],
    json['path_word'],
    json['count'],
  );
}
