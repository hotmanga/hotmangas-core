import './common_list_results.dart' show CommonListResults;
import './inner_item.dart' show InnerItem;
import '../enmus/content_type.dart' show ContentTypeEnumMap;
import '../enmus/decode.dart' show enumDecodeNullable;
import '../results_entity.dart' show ResultsEntity;

class HomeIndexFilterListEntity extends ResultsEntity {
  Results? results;

  HomeIndexFilterListEntity(
    code,
    message,
    this.results,
  ) : super(code, message);

  factory HomeIndexFilterListEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$HomeIndexFilterListEntityFromJson(srcJson);
}

class Results extends CommonListResults {
  List<InnerItem?>? list;

  Results(total, this.list, limit, offset, type)
      : super(total, limit, offset, type);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

HomeIndexFilterListEntity _$HomeIndexFilterListEntityFromJson(
    Map<String, dynamic> json) {
  return HomeIndexFilterListEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['total'],
      (json['list'] as List)
          .map((e) =>
              e == null ? null : InnerItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      json['limit'],
      json['offset'],
      enumDecodeNullable(ContentTypeEnumMap, json['type']));
}
