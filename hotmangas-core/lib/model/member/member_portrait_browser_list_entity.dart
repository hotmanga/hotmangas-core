import 'package:hotmangasCore/model/core/common/author_or_company.dart'
    show AuthorOrCompany;
import 'package:hotmangasCore/model/core/common/common_list_results.dart'
    show CommonListResults;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberBrowserPortraitListEntity extends ResultsEntity {
  Results? results;

  MemberBrowserPortraitListEntity(code, message, this.results)
      : super(code, message);

  factory MemberBrowserPortraitListEntity.fromJson(
          Map<String, dynamic> srcJson) =>
      _$MemberBrowserPortraitListEntityFromJson(srcJson);
}

MemberBrowserPortraitListEntity _$MemberBrowserPortraitListEntityFromJson(
    Map<String, dynamic> json) {
  return MemberBrowserPortraitListEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends CommonListResults {
  List<MemberBrowserPortraitItem>? list;

  Results(total, limit, offset, this.list) : super(total, limit, offset, null);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['total'],
    json['limit'],
    json['offset'],
    (json['list'] as List)
        .map((e) => MemberBrowserPortraitItem.fromJson(e))
        .toList(),
  );
}

class MemberBrowserPortraitItem extends Object {
  int? id;
  String? lastChapterId;
  String? lastChapterName;
  BrowserPortraitInnerItem? post;
  MemberBrowserPortraitItem(
      this.id, this.lastChapterId, this.lastChapterName, this.post);

  factory MemberBrowserPortraitItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberBrowserItemFromJson(srcJson);
}

MemberBrowserPortraitItem _$MemberBrowserItemFromJson(
    Map<String, dynamic> json) {
  return MemberBrowserPortraitItem(
    json['id'],
    json['last_chapter_id'] == null ? null : json['last_chapter_id'].toString(),
    json['last_chapter_name'],
    json['post'] == null
        ? null
        : BrowserPortraitInnerItem.fromJson(json['post']),
  );
}

class BrowserPortraitInnerItem extends Object {
  String? uuid;
  bool? bDisplay;
  String? name;

  List<AuthorOrCompany?>? mannequins;
  String? cover;
  int? popular;
  String? datetimeUpdated;

  BrowserPortraitInnerItem(
    this.uuid,
    this.bDisplay,
    this.name,
    this.mannequins,
    this.cover,
    this.popular,
    this.datetimeUpdated,
  );

  factory BrowserPortraitInnerItem.fromJson(Map<String, dynamic> srcJson) =>
      _$BrowserInnerItemFromJson(srcJson);
}

BrowserPortraitInnerItem _$BrowserInnerItemFromJson(Map<String, dynamic> json) {
  return BrowserPortraitInnerItem(
    json['uuid'],
    json['b_display'],
    json['name'],
    (json['mannequins'] as List)
        .map((e) => AuthorOrCompany.fromJson(e))
        .toList(),
    json['cover'],
    json['popular'],
    json['datetime_updated'],
  );
}
