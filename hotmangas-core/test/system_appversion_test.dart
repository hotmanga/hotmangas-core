// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试获取最新版本接口', () async {
    SystemAppVersionInfoEntity? entity;
    await system.appversion.getList(resolve: (data) {
      entity = data;
    }, reject: (error) {
      print(error.toString());
    });

    expect(entity!.code, equals(200));
    var android = entity!.results?.android;
    expect(android?.update, isTrue);
    expect(android?.hasNewVersion('1.0.0'), isTrue,
        reason:
            '1.0.0 is current, lastest version is ${android?.version}, has new');
    expect(android?.hasNewVersion('1.0'), isTrue,
        reason:
            '1.0 is current, lastest version is ${android?.version}, has new');
    expect(android?.hasNewVersion('4.0.0'), isFalse,
        reason:
            '4.0.0 is current, lastest version is ${android?.version}, has no new');
    expect(android?.hasNewVersion('4'), isFalse,
        reason:
            '4 is current, lastest version is ${android?.version}, has no new');
    expect(android?.hasNewVersion('4.0.0.0.0.0'), isFalse,
        reason:
            '4 is current, lastest version is ${android?.version}, has no new');
    expect(android?.hasNewVersion(android.version!), isFalse,
        reason:
            '${android?.version} is current, lastest version is ${android?.version}, has no new');
    var ios = entity!.results?.ios;
    expect(ios?.update, isFalse);
  });
}
