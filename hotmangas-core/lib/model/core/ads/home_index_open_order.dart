class OpenOrders extends Object {
  bool? open;

  String? img;

  String? url;

  OpenOrders(
    this.open,
    this.img,
    this.url,
  );

  factory OpenOrders.fromJson(Map<String, dynamic> srcJson) =>
      _$OpenOrdersFromJson(srcJson);
}

OpenOrders _$OpenOrdersFromJson(Map<String, dynamic> json) {
  return OpenOrders(
    json['open'],
    json['img'],
    json['url'],
  );
}
