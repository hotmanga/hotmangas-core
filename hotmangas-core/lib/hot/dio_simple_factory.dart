import 'package:dio/dio.dart'
    show
        Dio,
        DioError,
        DioErrorType,
        ErrorInterceptorHandler,
        InterceptorsWrapper,
        RequestInterceptorHandler,
        RequestOptions,
        Response,
        ResponseInterceptorHandler;
import 'package:hotmangasCore/model/model.dart';
import 'package:meta/meta.dart' show required;

import './options.dart' show SystemNetworkCache, options;

class DioWarpper {
  final dio = Dio(options);

  DioWarpper() {
    // ignore: unnecessary_this
    this.dio.interceptors.add(InterceptorsWrapper(onRequest:
            (RequestOptions _options, RequestInterceptorHandler handler) async {
          _options.queryParameters.addAll({'platform': 3});

          String? ur = '';

          //如果是谷歌渠道的話
          if (dio.options.extra['isGoogle'] == true) {
            ur = SystemNetworkCache.getApiG();
          }

          if (_options.path.toString().contains('/api/v2/adopr/')) {
            // ur = 'https://marketing.mangacopy.com';
            ur = 'https://marketest.mangacopy.com';
          }
          //只要是广告接口全部使用ur域名
          var _options2 = _options.copyWith(
            baseUrl: _options.uri.toString().contains('https')
                ? ''
                : ur == ''
                    ? SystemNetworkCache.getApi()
                    : ur,

            queryParameters: _options.queryParameters,
            // extra: _options.extra,
          );

          _options2.headers.addAll({
            'platform': dio.options.extra['platform'],
            'source': dio.options.extra['source'],
            'version': dio.options.extra['version'],
            'authorization': dio.options.extra['authorization'],
            'webp': dio.options.extra['webp'],
            'region': dio.options.extra['region'],
            'umString': dio.options.extra['umString'],
            'referer': dio.options.extra['referer'],
            'user-agent': dio.options.extra['userAgent'],
            'device': dio.options.extra['androidId'],
            'deviceInfo': dio.options.extra['deviceInfo'],
            'Accept-Language': dio.options.extra['Accept-Language'],
          });

          // if (!_options2.uri.toString().contains('/api/v3/login')) {
          //   print('header:${_options2.path}');
          // print('header:${_options2.headers.toString()}');
          //   print('-----------------------------------');
          //   print('data:${_options2.data.toString()}');
          //   print('-----------------------------------');
          //   print('extra:${_options2.extra.toString()}');
          //   print('----------------------------------');
          //   print('baseUrl:${_options2.baseUrl.toString()}');
          //   print('----------------------------------');
          //   print('contentType:${_options2.contentType}');
          //   print('----------------------------------');
          //   print('cancelToken:${_options2.cancelToken.toString()}');
          //   print('----------------------------------');
          //   print('connectTimeout:${_options2.connectTimeout}');
          //   print('----------------------------------');
          //   print('maxRedirects:${_options2.maxRedirects}');
          //   print('----------------------------------');
          // }

          return handler.next(_options2);
          // return _options2; //continue
          // 如果你想完成请求并返回一些自定义数据，可以返回一个`Response`对象或返回`dio.resolve(data)`。
          // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义数据data.
          //
          // 如果你想终止请求并触发一个错误,你可以返回一个`DioError`对象，或返回`dio.reject(errMsg)`，
          // 这样请求将被中止并触发异常，上层catchError会被调用。
        }, onResponse:
            (Response response, ResponseInterceptorHandler handler) async {
          // 在返回响应数据之前做一些预处理
          // if (!response.realUri.toString().contains('/api/v3/login')) {
          // print('header:${response.headers.toString()}');
          //   print('-----------------------------------');
          //   print('data:${response.data.toString()}');
          //   print('-----------------------------------');
          //   print('extra:${response.extra.toString()}');
          //   print('----------------------------------');
          //   print('realUrl:${response.realUri.toString()}');
          //   print('----------------------------------');
          //   print('statusCode:${response.statusCode.toString()}');
          //   print('----------------------------------');
          //   print('statusMessage:${response.statusMessage}');
          //   print('----------------------------------');
          // }

          return handler.next(response);
          // return response; // continue
        }, onError: (DioError e, ErrorInterceptorHandler handler) async {
          print('onError, Request URI: ${e.requestOptions.uri}');

          print('DioWarpper: ${e.toString()}, e.runtimeType: ${e.runtimeType}');
          return handler.next(e);
          // return e; //continue
        }));

    // dio.httpClientAdapter = Http2Adapter(
    //   ConnectionManager(
    //     idleTimeout: 10000,
    //     //   /// Ignore bad certificate
    //     //   onClientCreate: (_, clientSetting) => clientSetting.onBadCertificate = (_) => true,
    //   ),
    // );
  }

  Future<void> _httpPost(
      {@required String? path,
      dynamic formData,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    await dio
        .post(path!, data: formData, queryParameters: queryParameters)
        .then((Response response) {
      // resolve?.call(response.data);

      if (response.data['code'] == 200) {
        resolve?.call(response.data);
      } else {
        reject?.call(
            HotmangasError(response.data['code'], response.data['message']));
      }
    }).catchError((error) {
      _catchError(error, reject);
    });
  }

  Future<void> uploadPost(
      {@required String? path,
      dynamic formData,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    await _httpPost(
        path: path,
        formData: formData,
        queryParameters: queryParameters,
        resolve: resolve,
        reject: reject);
  }

  Future<void> httpPost(
      {@required String? path,
      Map<String, dynamic>? formData,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    formData?.addAll(options.extra);

    await _httpPost(
        path: path,
        formData: formData,
        queryParameters: queryParameters,
        resolve: resolve,
        reject: reject);
  }

  Future<void> httpGet(
      {@required String? path,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    await dio
        .get(path!, queryParameters: queryParameters)
        .then((Response? response) {
      if (response!.data['code'] == 200) {
        resolve?.call(response.data);
      } else {
        reject?.call(
            HotmangasError(response.data['code'], response.data['message']));
      }
    }).catchError((error) {
      _catchError(error, reject);
    });
  }

  Future<void> httpDelete(
      {String? path,
      Map<String, dynamic>? formData,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    formData?.addAll(options.extra);

    await dio
        .delete(path!, data: formData, queryParameters: queryParameters)
        .then((Response response) {
      if (response.data['code'] == 200) {
        resolve?.call(response.data);
      } else {
        reject?.call(
            HotmangasError(response.data['code'], response.data['message']));
      }
    }).catchError((error) {
      _catchError(error, reject);
    });
  }

  void mergeDioOptionBaseUrl(String baseUrl) {
    dio.options.baseUrl = baseUrl;
  }

  void mergeDioOptionExtra(Map<String, dynamic> extra) {
    dio.options.extra.addAll(extra);
  }

  /// GET, POST 的 catchError 统一处理。
  void _catchError(dynamic error, Function? reject) {
    if (error is DioError) {
      var _ = error;
      print(SystemNetworkCache.lang);

      /// 有返回值
      if (_.type == DioErrorType.response) {
        if (_.response?.statusCode == 404) {
          if (SystemNetworkCache.lang == 'en') {
            reject?.call(HotmangasError(_.response!.statusCode,
                'Request path error or resource no longer exists'));
          } else {
            reject
                ?.call(HotmangasError(_.response!.statusCode, '请求路径错误或资源已不存在'));
          }
        } else if (_.response?.statusCode == 502) {
          if (SystemNetworkCache.lang == 'en') {
            reject?.call(
                HotmangasError(502, 'Service error, please try again later'));
          } else {
            reject?.call(HotmangasError(502, '服务错误，请稍后再试'));
          }
        } else {
          reject?.call(HotmangasError(
              _.response?.data['code'], _.response?.data['message']));
        }
      }

      /// 收取超时
      else if (_.type == DioErrorType.receiveTimeout) {
        if (SystemNetworkCache.lang == 'en') {
          reject?.call(HotmangasError(
              499, 'Connection timeout, the current network may be unstable'));
        } else {
          reject?.call(HotmangasError(499, '連接超時，當前網絡可能不穩定'));
        }
      }

      /// 连接超时
      else if (_.type == DioErrorType.connectTimeout) {
        if (SystemNetworkCache.lang == 'en') {
          reject?.call(HotmangasError(
              408, 'Connection timeout, the current network may be unstable'));
        } else {
          reject?.call(HotmangasError(408, '連接超時，當前網絡可能不穩定'));
        }
      }

      /// 其他错误
      else {
        if (null == _.response) {
          if (SystemNetworkCache.lang == 'en') {
            reject?.call(HotmangasError(408,
                "Not connected to the network, currently only able to browse content in 'My Downloads'."));
          } else {
            reject?.call(HotmangasError(408, '沒有連接到網絡，當前只能瀏覽"我的下载"里内容。'));
          }
        } else {
          if (SystemNetworkCache.lang == 'en') {
            reject?.call(HotmangasError(999,
                'An unknown error has occurred, please contact the administrator'));
          } else {
            reject?.call(HotmangasError(999, '發生了未知的錯誤,請聯系管理員'));
          }
        }
      }
    } else {
      print(
          '_catchError: runtimeType:[${error.runtimeType}], toString():[${error.toString()}]');
    }
  }
}

final httpServer = DioWarpper();
