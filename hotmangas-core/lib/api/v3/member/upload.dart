import 'dart:io' show File;

import 'package:dio/dio.dart' show FormData, MultipartFile;
import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show UploadEntity;
import 'package:meta/meta.dart' show required;

enum _UploadType {
  /// 1 头像
  Avator,

  /// 2 反馈
  FeedBackImage,
}

const _UploadTypeEnumMap = {
  _UploadType.Avator: 1,
  _UploadType.FeedBackImage: 2
};

class Upload extends HttpCore {
  Future<void> _upload(
      {_UploadType? type,
      File? file,
      Function? resolve,
      Function? reject}) async {
    final _formData = FormData.fromMap({
      'file': await MultipartFile.fromFile(file!.path),
    });
    final _type = _UploadTypeEnumMap[type];
    await uploadCore(
        path: '/api/v3/upload/image/$_type',
        queryParameters: null,
        formData: _formData,
        resolve: (data) {
          resolve?.call(UploadEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> uploadAvator(
      {@required File? file, Function? resolve, Function? reject}) async {
    await _upload(
        type: _UploadType.Avator, file: file, resolve: resolve, reject: reject);
  }

  Future<void> uploadFeedBackImage(
      {@required File? file, Function? resolve, Function? reject}) async {
    await _upload(
        type: _UploadType.FeedBackImage,
        file: file,
        resolve: resolve,
        reject: reject);
  }
}
