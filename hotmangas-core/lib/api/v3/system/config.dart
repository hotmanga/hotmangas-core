import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show SystemConfigEntity;

class SystemConfig extends HttpCore {
  Future<void> _getCore({String? path, resolve, reject}) async {
    await getCore(
        path: path,
        resolve: (data) {
          var systemConfig = SystemConfigEntity.fromJson(data);
          resolve(systemConfig);
        },
        reject: reject);
  }

  Future<void> get300({resolve, reject}) async {
    await _getCore(
        path: '/api/v3/system/config/300/3', resolve: resolve, reject: reject);
  }

  Future<void> get1000({resolve, reject}) async {
    await _getCore(
        path: '/api/v3/system/config/1000/3', resolve: resolve, reject: reject);
  }

  Future<void> get1001({resolve, reject}) async {
    await _getCore(
        path: '/api/v3/system/config/1001/3', resolve: resolve, reject: reject);
  }

  Future<void> get2000({resolve, reject}) async {
    await _getCore(
        path: '/api/v3/system/config/2000/3', resolve: resolve, reject: reject);
  }

  Future<void> get2020({resolve, reject}) async {
    await _getCore(
        path: '/api/v3/system/config/2020/3', resolve: resolve, reject: reject);
  }

  Future<void> get6000({resolve, reject}) async {
    await _getCore(
        path: '/api/v3/system/config/6000', resolve: resolve, reject: reject);
  }

  Future<void> get30001({resolve, reject}) async {
    await _getCore(
        path: '/api/v3/system/config/30001/3',
        resolve: resolve,
        reject: reject);
  }

  Future<void> getAdLine({resolve, reject}) async {
    await _getCore(
        path: '/api/v3/system/config/5000/3', resolve: resolve, reject: reject);
  }
}

final systemConfig = SystemConfig();
