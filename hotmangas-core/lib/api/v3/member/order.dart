import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/member/member_order_entity.dart'
    show MemberOrderEntity;
import 'package:hotmangasCore/model/member/member_order_list_entity.dart'
    show MemberOrderListEntity;

final Map<String, dynamic> _queryParameters = {
  'limit': 21,
  'offset': 0,
};

class Order extends HttpCore {
  Future<void> getList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/orders',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberOrderListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 测试用接口 /api/v3/member/orderTest
  /// 正式用接口 /api/v3/member/order
  Future<void> createOrder(
      {Map<String, dynamic>? formData,
      Function? resolve,
      Function? reject}) async {
    await postCore(
        path: '/api/v3/member/order',
        formData: formData,
        queryParameters: null,
        resolve: (data) {
          resolve?.call(MemberOrderEntity.fromJson(data));
        },
        reject: reject);
  }
}
