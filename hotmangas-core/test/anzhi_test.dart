import 'package:hotmangasCore/api/cpaz/api.dart' show azApi;
import 'package:hotmangasCore/api/v3/api.dart' show member;
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('安智comicquery limit=21 offset=0', () async {
    CommonListEntity? response;
    await azApi.getComicQueryList(
        queryParameters: {},
        resolve: (CommonListEntity entity) {
          response = entity;
        },
        reject: () {});
    expect(response?.results?.limit, equals(21), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
  });

  test('安智getRankList limit=21 offset=0 total=10', () async {
    final userName = 'copy002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    RankListEntity? response;
    await azApi.getRankList(
        queryParameters: {
          'type': 1,
          'date_type': 'day',
          'limit': 21,
          'offset': 0,
        },
        resolve: (RankListEntity entity) {
          response = entity;
        },
        reject: () {});

    expect(response?.results?.limit, equals(21), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.total, equals(10), reason: '3');
    expect(response?.results?.list?.first?.sort, equals(0), reason: '4');
  });
  test('安智getRecsList limit=20 offset=0 total=3', () async {
    final userName = 'copy002';
    final passWord = '111111';

    await member.login.login(
        userName: userName,
        passWord: passWord,
        resolve: (MemberInfoEntity entity) {
          print(entity);
        },
        reject: (err) {
          print(err);
        });
    RecommendComicListEntity? response;
    await azApi.getRecsList(
        position: RecommendType.WebH5HomeEditors,
        resolve: (RecommendComicListEntity entity) {
          response = entity;
        },
        reject: () {});
    expect(response?.results?.limit, equals(20), reason: '1');
    expect(response?.results?.offset, equals(0), reason: '2');
    expect(response?.results?.total, equals(3), reason: '3');
  });

  test('安智homeIndex', () async {
    CopyHomeIndexEntity? response;
    await azApi.getHomeIndex(
        resolve: (CopyHomeIndexEntity entity) {
          response = entity;
          print(entity);
        },
        reject: () {});
    expect(response, isA<CopyHomeIndexEntity>(), reason: '1');
    expect(response?.code, equals(200), reason: '2');

    expect(response?.results?.recComics?.list!.length, equals(3), reason: '5');
    expect(response?.results?.rankDayComics?.list!.length, equals(6),
        reason: '6');
    expect(response?.results?.rankWeekComics?.list!.length, equals(6),
        reason: '7');
    expect(response?.results?.hotComics!.length, equals(6), reason: '8');
    expect(response?.results?.newComics!.length, equals(6), reason: '9');
    expect(response?.results?.finishComics!.list!.length, equals(6),
        reason: '10');
  });
}
