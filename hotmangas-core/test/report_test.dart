// Package imports:
// Project imports:

import 'package:hotmangasCore/api/v3/api.dart' show member;
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试获取轻小说举报配置', () async {
    ReportConfigListEntity? response;
    await member.report.getNovelReport(
        resolve: (ReportConfigListEntity entity) {
      response = entity;
      if (response!.results!.reportType.isNotEmpty) {
        for (var element in response!.results!.reportType) {
          print('${element.name}, ${element.type}');
        }
      }
    }, reject: (err) {
      print(err.toString());
    });
    expect(response, isNotNull);
    expect(response?.code, equals(200));
  });

  test('举报某个用户在某个轻小说的评论', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    //举报原因
    dynamic response;
    await member.report.postNovelReport(
        queryParameters: {
          'book_id': 'b8a1967a-6bd5-11ed-acd3-024352452ce0',
          'comment_id': 3, //吐槽ID
          'report_type': 8, //举报类型
          'report_reason': '测试举报',
        },
        resolve: (data) {
          response = data;
        },
        reject: (HotmangasError error) {
          print(error);
        });

    expect(response, isNotNull);
  });
}
