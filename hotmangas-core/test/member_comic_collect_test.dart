// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';

void main() {
  test('测试书架，获取漫画', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberCollectComicListEntity? entity;
    await member.collect.getComicList(
        queryParameters: {'free_type': 1, 'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
          print(entity);
        },
        reject: (error) {
          print(error);
        });

    expect(entity, isA<MemberCollectComicListEntity>());
    expect(entity?.code, equals(200));
    expect(entity?.results?.list, isList);
    expect(entity?.results?.limit, equals(1));
    expect(entity?.results?.offset, equals(0));
  });

  test('测试直接获取', () async {
    var _token = 'e8544fbe0a453d36e3df37e6c0c4f1334a314500';
    member.login.addToken(token: _token);
    await member.collect.getComicList(
        queryParameters: {
          'free_type': 1,
          'limit': 18,
          'offset': 0,
          'ordering': ''
        },
        resolve: (data) {
          print(data);
        },
        reject: (HotmangasError error) {
          print('${error.errorType} ${error.message} ${error.runtimeType}');
        });
  });
}
