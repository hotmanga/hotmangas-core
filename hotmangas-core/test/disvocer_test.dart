// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';

void main() {
  test('测试发现页接口 付费漫画发现页', () async {
    DiscoverListEntity? entity;
    await discoverIndex.getChargeComicDiscoverIndex(
        resolve: (DiscoverListEntity data) {
      entity = data;
    }, reject: (error) {
      print(error);
    });

    expect(entity, isA<DiscoverListEntity>());
    expect(entity?.code, equals(200));
    expect(entity?.results?.banners, isNotNull, reason: 'banner is not null');
    expect(entity?.results?.banners?.first?.item, isA<InnerItem>(),
        reason: 'banner item is innerItem');
    expect(entity?.results?.recs, isNotNull, reason: 'recs is not null');
    expect(entity?.results?.recs?.limit, equals(3), reason: 'recs length is 3');
    expect(entity?.results?.recs?.offset, equals(0),
        reason: 'recs length is 3');
    expect(entity?.results?.recs?.list?.first?.comic, isA<InnerItem>(),
        reason: 'recs item is innetItem');
    expect(entity?.results?.list, isNotNull, reason: 'list is not null');
    expect(entity?.results?.list?.first?.type, ContentType.Theme,
        reason: 'comic is not null');
    expect(entity?.results?.list?.first?.limit, equals(6),
        reason: 'every list length default is 6');
    expect(entity?.results?.list?.first?.offset, equals(0),
        reason: 'every list offset default is 0');
    expect(entity?.results?.list?.first?.list?.first, isA<InnerItem>(),
        reason: 'list item is innerItem');
  });

  test('测试发现页接口 免费漫画发现页', () async {
    DiscoverListEntity? entity;
    await discoverIndex.getFreeComicDiscoverIndex(
        resolve: (DiscoverListEntity data) {
      entity = data;
    }, reject: (error) {
      print(error);
    });

    expect(entity, isA<DiscoverListEntity>());
    expect(entity?.code, equals(200));
    expect(entity?.results?.banners, isNotNull, reason: 'banner is not null');
    expect(entity?.results?.banners?.first?.item, isA<InnerItem>(),
        reason: 'banner item is innerItem');
    expect(entity?.results?.recs, isNotNull, reason: 'recs is not null');
    expect(entity?.results?.recs?.limit, equals(3), reason: 'recs length is 3');
    expect(entity?.results?.recs?.offset, equals(0),
        reason: 'recs length is 3');
    expect(entity?.results?.recs?.list?.first?.comic, isA<InnerItem>(),
        reason: 'recs item is innetItem');
    expect(entity?.results?.list, isNull, reason: 'list is null');
    // expect(entity?.results.list.first.type, ContentType.Theme,
    // reason: 'comic is not null');
    // expect(entity?.results.list.first.limit, equals(6),
    // reason: 'every list length default is 6');
    // expect(entity?.results.list.first.offset, equals(0),
    // reason: 'every list offset default is 0');
    // expect(entity?.results.list.first.list.first, isA<InnerItem>(),
    // reason: 'list item is innerItem');
  });

  test('测试发现页接口 付费动画发现页', () async {
    DiscoverListEntity? entity;
    await discoverIndex.getChargeCartoonDiscoverIndex(
        resolve: (DiscoverListEntity data) {
      entity = data;
    }, reject: (error) {
      print(error);
    });

    expect(entity, isA<DiscoverListEntity>());
    expect(entity?.code, equals(200));
    expect(entity?.results?.banners, isNotNull, reason: 'banner is not null');
    expect(entity?.results?.banners?.first?.item, isA<InnerItem>(),
        reason: 'banner item is innerItem');
    expect(entity?.results?.recs, isNotNull, reason: 'recs is not null');
    expect(entity?.results?.recs?.limit, equals(3), reason: 'recs length is 3');
    expect(entity?.results?.recs?.offset, equals(0),
        reason: 'recs length is 3');
    expect(entity?.results?.recs?.list?.first?.comic, isA<InnerItem>(),
        reason: 'recs item is innetItem');
    expect(entity?.results?.list, isNotNull, reason: 'list is not null');
    expect(entity?.results?.list?.first?.type, ContentType.Theme,
        reason: 'comic is not null');
    expect(entity?.results?.list?.first?.limit, equals(6),
        reason: 'every list length default is 6');
    expect(entity?.results?.list?.first?.offset, equals(0),
        reason: 'every list offset default is 0');
    expect(entity?.results?.list?.first?.list?.first, isA<InnerItem>(),
        reason: 'list item is innerItem');
  });

  test('getFreeCartoonDiscoverIndex', () async {
    DiscoverFreeCartoonListEntity? response;
    await discoverIndex.getFreeCartoonDiscoverIndex(
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list, isNotNull);
    expect(response?.results?.list?.first, isA<CollectCartoonInnerItem>());
  });
}
