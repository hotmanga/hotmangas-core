class StatusItem extends Object {
  int? value;

  String? display;

  StatusItem(this.value, this.display);

  factory StatusItem.fromJson(Map<String, dynamic> srcJson) =>
      _$StatusItemFromJson(srcJson);
}

StatusItem _$StatusItemFromJson(Map<String, dynamic> json) {
  return StatusItem(
    json['value'],
    json['display'],
  );
}
