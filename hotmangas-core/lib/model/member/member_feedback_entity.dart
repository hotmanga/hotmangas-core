import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberFeedBackEntity extends ResultsEntity {
  MemberFeedBackEntity(code, message) : super(code, message);

  factory MemberFeedBackEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberFeedBackEntity(srcJson);
}

MemberFeedBackEntity _$MemberFeedBackEntity(Map<String, dynamic> json) {
  return MemberFeedBackEntity(
    json['code'],
    json['message'],
  );
}
