import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberForgetAnswerSuccessEntity extends ResultsEntity {
  MemberForgetAnswerSuccessEntity(code, message) : super(code, message);

  factory MemberForgetAnswerSuccessEntity.fromJson(
          Map<String, dynamic> srcJson) =>
      _$MemberForgetAnswerSuccessEntityFromJson(srcJson);
}

MemberForgetAnswerSuccessEntity _$MemberForgetAnswerSuccessEntityFromJson(
    Map<String, dynamic> json) {
  return MemberForgetAnswerSuccessEntity(
    json['code'],
    json['message'],
  );
}
