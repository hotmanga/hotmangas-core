import 'package:hotmangasCore/api/v3/api.dart' show member, portrait;
import 'package:hotmangasCore/model/model.dart'
    show
        MemberCollectPortraitListEntity,
        PortraitChapterContentEntity,
        PortraitPuyEntity,
        ResultsEntity;
import 'package:test/test.dart';

void main() {
  test('测试收藏写真', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    //收藏写真
    await member.collect.collectPortraitByUid(
        uid: '1',
        collect: true,
        resolve: (ResultsEntity data) {
          print(data.message);
        },
        reject: (err) {
          print(err);
        });
  });

  test('获取收藏写真列表', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await member.collect.getPortraitList(
        queryParameters: {'limit': '21', 'offset': '0'},
        resolve: (MemberCollectPortraitListEntity data) {
          print(data.results?.list?.length);
        },
        reject: (err) {
          print(err);
        });
  });

  test('下载写真图片接口测试', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await portrait.getChapterContentByDownload(
        portraitUuid: '1',
        portraitChapterId: '1',
        resolve: (data) {
          print(data.toString());
        },
        reject: (err) {
          print(err);
        });
  });

  test('获取写真章节列表', () async {
    await portrait.getChapters(
        portraitUuid: '1',
        queryParameters: {
          'limit': 0,
        },
        resolve: (res) {
          print(res);
        },
        reject: (err) {});
  });

  test('删除收藏写真', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await member.collect.delPortraitList(
        queryParameters: {
          'ids': [1]
        },
        resolve: (data) {
          print(data);
        },
        reject: (err) {
          print(err);
        });
  });

  test('写真浏览记录', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await member.browser.getPortraitList(
        queryParameters: {'limit': '21', 'offset': '0'},
        resolve: (data) {
          print(data);
        },
        reject: (err) {
          print(err);
        });
  });
  test('清空写真浏览记录', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await member.browser.deletePortrait(resolve: (data) {
      print(data);
    }, reject: (err) {
      print(err);
    });
  });
  test('测试购买写真', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    //购买写真
    await portrait.postPuy(
        post_id: '1',
        resolve: (PortraitPuyEntity data) {
          print(data.results?.ticket);
        },
        reject: (err) {
          print(err);
        });
  });
  test('测试查看阅读页', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ///查看阅读页
    await portrait.getChapterContent(
        portraitUuid: '1',
        portraitChapterId: '1',
        resolve: (PortraitChapterContentEntity data) {
          print(data.results?.chapter?.contents);
        },
        reject: (err) {
          print(err);
        });
  });

  test('测试写真详情页个人相关', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ///查看详情页个人相关
    await portrait.getCurrentPortraitCollect(
        uuid: '1',
        resolve: (data) {
          print(data);
        },
        reject: (err) {
          print(err);
        });
  });

  test('测试获取某个写真的下载图片', () async {
    final userName = 'copy004';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ///查看阅读页
    await portrait.getChapterContentByDownload(
        portraitUuid: '1',
        portraitChapterId: '1',
        resolve: (data) {
          print(data);
        },
        reject: (err) {
          print(err);
        });
  });
}
