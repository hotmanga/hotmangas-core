import 'package:hotmangasCore/model/core/common/reward_list_entity.dart';

class AuthorOrCompany extends Object {
  String? name;

  String? pathWord;

  ///仅在模特里面存在 模特打赏列表
  List<RewardList?>? rewardList;
  AuthorOrCompany(
    this.name,
    this.pathWord,
    this.rewardList,
  );

  @override
  String toString() => name!;

  factory AuthorOrCompany.fromJson(Map<String, dynamic> srcJson) =>
      _$AuthorOrCompanyFromJson(srcJson);
}

AuthorOrCompany _$AuthorOrCompanyFromJson(Map<String, dynamic> json) {
  return AuthorOrCompany(
    json['name'] ?? '-',
    json['path_word'],
    json['reward_list'] == null
        ? null
        : (json['reward_list'] as List)
            .map((e) => e == null
                ? null
                : RewardList.fromJson(e as Map<String, dynamic>))
            .toList(),
  );
}
