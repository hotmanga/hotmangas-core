// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show homeIndex;

import 'package:hotmangasCore/model/model.dart'
    show
        HomeIndexFilterListEntity,
        HomeIndexFilterCartoonTypeEnumMap,
        HomeIndexFilterCartoonType,
        HomeIndexFilterComicTypeEnumMap,
        HomeIndexFilterComicType;

void main() {
  test('测试首页获取动画-默认参数', () async {
    HomeIndexFilterListEntity? response;
    await homeIndex.getHomeIndexCartoon(
        queryParameters: {},
        resolve: (HomeIndexFilterListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<HomeIndexFilterListEntity>());
    expect(response?.results?.limit, equals(20));
    expect(response?.results?.offset, isZero);
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.length, greaterThan(0));
  });

  test('测试首页获取动画-limit=6, offset=2', () async {
    HomeIndexFilterListEntity? response;
    await homeIndex.getHomeIndexCartoon(
        queryParameters: {
          'limit': 6,
          'offset': 2,
        },
        resolve: (HomeIndexFilterListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<HomeIndexFilterListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(6));
    expect(response?.results?.offset, equals(2));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.length, equals(6));
  });

  test('测试首页获取动画-top=3d', () async {
    HomeIndexFilterListEntity? response;
    await homeIndex.getHomeIndexCartoon(
        queryParameters: {
          'top': HomeIndexFilterCartoonTypeEnumMap[
                  HomeIndexFilterCartoonType.Cartoon3D]!
              .name
        },
        resolve: (HomeIndexFilterListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<HomeIndexFilterListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(20));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.length, equals(20));
  });

  test('测试首页获取漫画-默认参数', () async {
    HomeIndexFilterListEntity? response;
    await homeIndex.getHomeIndexComic(
        queryParameters: {},
        resolve: (HomeIndexFilterListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<HomeIndexFilterListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(20));
    expect(response?.results?.offset, isZero);
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.length, greaterThan(0));
  });

  test('测试首页获取漫画-limit=6, offset=2', () async {
    HomeIndexFilterListEntity? response;
    await homeIndex.getHomeIndexComic(
        queryParameters: {
          'limit': 6,
          'offset': 2,
        },
        resolve: (HomeIndexFilterListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<HomeIndexFilterListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(6));
    expect(response?.results?.offset, equals(2));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.length, equals(6));
  });

  test('测试首页获取漫画-top=Korea', () async {
    HomeIndexFilterListEntity? response;
    await homeIndex.getHomeIndexComic(
        queryParameters: {
          'top':
              HomeIndexFilterComicTypeEnumMap[HomeIndexFilterComicType.Korea]!
                  .name
        },
        resolve: (HomeIndexFilterListEntity data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });
    expect(response, isA<HomeIndexFilterListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(20));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.length, equals(20));
  });
}
