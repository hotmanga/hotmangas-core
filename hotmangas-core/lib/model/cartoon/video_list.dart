class VideoList extends Object {
  String? name;

  String? key;

  String? format;

  String? url;

  VideoList(this.name, this.key, this.format, this.url);

  factory VideoList.fromJson(Map<String, dynamic> srcJson) =>
      _$VideoListItemFromJson(srcJson);
}

VideoList _$VideoListItemFromJson(Map<String, dynamic> json) {
  return VideoList(json['name'], json['key'], json['format'], json['format']);
}
