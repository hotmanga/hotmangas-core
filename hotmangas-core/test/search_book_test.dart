import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/search/search_book_list_entity.dart'
    show SearchBookListEntity;
import 'package:test/test.dart';

void main() async {
  test('测试搜索轻小说-仅限一个', () async {
    SearchBookListEntity? response;
    await search.searchBook(
      queryParameters: {
        'limit': 1,
        'offset': 0,
        'q': '測試勿動',
      },
      resolve: (SearchBookListEntity data) {
        response = data;
      },
      reject: (error) {
        print(error);
      },
    );

    expect(response, isA<SearchBookListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list?.length, equals(1));
  });
}
