import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        ComicChapterContentSignEntity,
        ComicChapterEntity,
        ComicChapterContentEntity,
        ComicDetailEntity,
        CommonListEntity,
        MemberCollectEntity;
import 'package:meta/meta.dart' show required;

final Map<String, dynamic> _queryParameters = {
  'limit': 21,
  'offset': 0,
};

class Comic extends HttpCore {
  Future<void> getDetail(
      {@required String? pathWord,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/comic2/$pathWord',
        queryParameters: queryParameters,
        resolve: (data) {
          resolve?.call(ComicDetailEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getChapters(
      {@required String? comicPathWord,
      @required String? groupPathWord,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/comic/$comicPathWord/group/$groupPathWord/chapters',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(ComicChapterEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取漫画某个章节的图片
  /// 测试用:
  /// - comicPathword: "chaoshidepiaoliangjiejie"
  /// - comicChapterId: 386abb34-b698-11ea-85ef-024352452ce0
  Future<void> getChapterContent(
      {@required String? comicPathWord,
      @required String? comicChapterId,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/comic/$comicPathWord/chapter/$comicChapterId',
        resolve: (data) {
          resolve?.call(ComicChapterContentEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取漫画某个章节的图片
  /// 测试用:
  /// - comicPathword: "chaoshidepiaoliangjiejie"
  /// - comicChapterId: 386abb34-b698-11ea-85ef-024352452ce0
  Future<void> getChapterContentBySign(
      {@required String? comicPathWord,
      @required String? comicChapterId,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/comic/$comicPathWord/chapter2/$comicChapterId',
        queryParameters: queryParameters,
        resolve: (data) {
          resolve?.call(ComicChapterContentSignEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取漫画某个章节的图片
  /// 测试用:
  /// - comicPathword: "chaoshidepiaoliangjiejie"
  /// - comicChapterId: 386abb34-b698-11ea-85ef-024352452ce0
  Future<void> getChapterContentByDownload(
      {@required String? comicPathWord,
      @required String? comicChapterId,
      Function? resolve,
      Function? reject}) async {
    await getCore(
        path: '/api/v3/comic/$comicPathWord/chapter2download/$comicChapterId',
        resolve: (data) {
          resolve?.call(ComicChapterContentSignEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getCurrentComicCollect(
      {@required String? pathWord, Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/comic2/$pathWord/query',
        resolve: (data) {
          resolve?.call(MemberCollectEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取漫画某个章节的图片
  /// 测试用:
  /// - comicPathword: "chaoshidepiaoliangjiejie"
  /// - comicChapterId: 386abb34-b698-11ea-85ef-024352452ce0
  Future<void> getQueryList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/comics',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(CommonListEntity.fromJson(data));
        },
        reject: reject);
  }
}

final comic = Comic();
