class RewardList extends Object {
  /// 用户头像
  String? userAvatar;

  /// 用户名称
  String? userName;

  String? _ticket;

  ///打赏了多少写真币
  String? get ticket {
    String? _str = _ticket.toString();
    return _str.split('.')[0].toString();
  }

  RewardList(
    this.userAvatar,
    this.userName,
    this._ticket,
  );

  @override
  String toString() => userName!;

  factory RewardList.fromJson(Map<String, dynamic> srcJson) =>
      _$RewardListFromJson(srcJson);
}

RewardList _$RewardListFromJson(Map<String, dynamic> json) {
  return RewardList(
    json['user_avatar'] ?? '',
    json['user_name'] ?? '',
    json['ticket'] ?? '0',
  );
}
