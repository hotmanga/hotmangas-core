import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class MemberUpdateInfoEntity extends ResultsEntity {
  Results? results;
  MemberUpdateInfoEntity(code, message, this.results) : super(code, message);

  factory MemberUpdateInfoEntity.fromJson(Map<String, dynamic> fromJson) =>
      _$MemberUpdateInfoFromJson(fromJson);
}

MemberUpdateInfoEntity _$MemberUpdateInfoFromJson(Map<String, dynamic> json) {
  return MemberUpdateInfoEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends Object {
  MemberUpdateInfoItem? info;

  List<GenderItem>? genders;

  Results(this.info, this.genders);

  factory Results.fromJson(Map<String, dynamic> fromJson) =>
      _$ResultsFromJson(fromJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(MemberUpdateInfoItem.fromJson(json['info']),
      (json['genders'] as List).map((e) => GenderItem.fromJson(e)).toList());
}

class MemberUpdateInfoItem extends Object {
  String? nickname;

  String? avatar;

  String? avatorRp;

  Gender? gender;

  String? username;

  String? mobile;

  String? mobileRegion;

  String? inviteCode;

  MemberUpdateInfoItem(this.nickname, this.avatar, this.avatorRp, this.gender,
      this.username, this.mobile, this.mobileRegion, this.inviteCode);

  factory MemberUpdateInfoItem.fromJson(Map<String, dynamic> fromJson) =>
      _$MemberUpdateInfoItemFromJson(fromJson);
}

MemberUpdateInfoItem _$MemberUpdateInfoItemFromJson(Map<String, dynamic> json) {
  return MemberUpdateInfoItem(
    json['nickname'] ?? '',
    json['avatar'] ?? '',
    json['avatar_rp'] ?? '',
    Gender.fromJson(json['gender']),
    json['username'] ?? '',
    json['mobile'] ?? '',
    json['mobile_region'] ?? '',
    json['invite_code'] ?? '',
  );
}

class Gender extends Object {
  int? value;

  String? display;

  Gender(this.value, this.display);

  factory Gender.fromJson(Map<String, dynamic> fromJson) =>
      _$GenderFromJson(fromJson);
}

Gender _$GenderFromJson(Map<String, dynamic> json) {
  return Gender(
    json['value'],
    json['display'],
  );
}

class GenderItem extends Object {
  int? key;

  String? value;

  GenderItem(this.key, this.value);

  factory GenderItem.fromJson(Map<String, dynamic> fromJson) =>
      _$GenderItemFromJson(fromJson);
}

GenderItem _$GenderItemFromJson(Map<String, dynamic> json) {
  return GenderItem(
    json['key'],
    json['value'],
  );
}
