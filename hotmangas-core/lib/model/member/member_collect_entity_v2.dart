import 'package:hotmangasCore/model/model.dart' show ResultsEntity;

class MemberCollectEntityV2 extends ResultsEntity {
  MemberCollectedItem? results;

  MemberCollectEntityV2(code, message, this.results) : super(code, message);

  factory MemberCollectEntityV2.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberComicCollectEntityV2FromJson(srcJson);
}

MemberCollectEntityV2 _$MemberComicCollectEntityV2FromJson(
    Map<String, dynamic> json) {
  return MemberCollectEntityV2(
    json['code'],
    json['message'],
    MemberCollectedItem.fromJson(json['results']),
  );
}

class MemberCollectedItem extends Object {
  int? collect;

  bool? isLock;

  bool? isLogin;

  bool? isMobileBind;

  bool? isVip;

  bool? get hasCollect {
    return collect != -1;
  }

  QueryBrowseEntity? browse;

  MemberCollectedItem(this.collect, this.isLock, this.isLogin,
      this.isMobileBind, this.isVip, this.browse);

  factory MemberCollectedItem.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberComicCollectedItemFromJson(srcJson);
}

MemberCollectedItem _$MemberComicCollectedItemFromJson(
    Map<String, dynamic> json) {
  return MemberCollectedItem(
    json['collect'] ?? -1,
    json['is_lock'],
    json['is_login'],
    json['is_mobile_bind'],
    json['is_vip'],
    json['browse'] != null ? QueryBrowseEntity.fromJson(json['browse']) : null,
  );
}

class QueryBrowseEntity extends Object {
  QueryBrowseEntity();

  factory QueryBrowseEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$QueryBrowseEntityFromJson(srcJson);
}

QueryBrowseEntity _$QueryBrowseEntityFromJson(Map<String, dynamic> json) {
  return QueryBrowseEntity();
}

class BookQueryBrowseEntity extends Object {
  String? bookId;
  String? pathWord;
  String? chapterId;
  String? chapterName;

  BookQueryBrowseEntity(
      this.bookId, this.pathWord, this.chapterId, this.chapterName);

  factory BookQueryBrowseEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$BookQueryBrowseEntityFromJson(srcJson);
}

BookQueryBrowseEntity _$BookQueryBrowseEntityFromJson(
    Map<String, dynamic> json) {
  return BookQueryBrowseEntity(
    json['book_id'],
    json['path_word'],
    json['chapter_id'],
    json['chapter_name'],
  );
}
