// Package imports:
// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试浏览记录，获取漫画', () async {
    final userName = 'copy002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberCollectComicListEntity? entity;
    await member.browser.getComicList(
        queryParameters: {'free_type': 1, 'limit': 1, 'offset': 0},
        resolve: (data) {
          entity = data;
          print(entity);
        },
        reject: (error) {
          print(error);
        });

    expect(entity, isA<MemberCollectComicListEntity>());
    expect(entity?.code, equals(200));
    expect(entity?.results?.list, isList);
    expect(entity?.results?.limit, equals(1));
    expect(entity?.results?.offset, equals(0));
  });

  test('测试浏览记录，删除漫画', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
        userName: userName,
        passWord: passWord,
        resolve: (data) {
          print(data);
        },
        reject: (error) {
          print(error);
        });

    MemberComicDeleteEntity? entity;
    await member.browser.deleteComics(
        freeType: FreeType.Free,
        resolve: (data) {
          entity = data;
          print(entity);
        },
        reject: (error) {
          print(error);
        });

    expect(entity, isA<MemberComicDeleteEntity>());
    expect(entity?.code, equals(200));
  });

  test('测试浏览记录，删除动画', () async {
    final userName = 'wxceshi002';
    final passWord = '111111';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberCartoonDeleteEntity? entity;
    await member.browser.deleteCartoon(
        freeType: FreeType.Free,
        resolve: (data) {
          entity = data;
          print(entity);
        },
        reject: (error) {
          print(error);
        });

    expect(entity, isA<MemberCartoonDeleteEntity>());
    expect(entity?.code, equals(200));
  });
}
