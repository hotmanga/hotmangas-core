import 'package:hotmangasCore/model/comic/comic_chapter_content.dart'
    show ChapterContent;
import 'package:hotmangasCore/model/core/common/inner_item.dart' show InnerItem;
import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

class PortraitChapterContentEntity extends ResultsEntity {
  Results? results;

  PortraitChapterContentEntity(int code, String message, this.results)
      : super(code, message);

  factory PortraitChapterContentEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$PortraitChapterContentEntityFromJson(srcJson);
}

PortraitChapterContentEntity _$PortraitChapterContentEntityFromJson(
    Map<String, dynamic> json) {
  return PortraitChapterContentEntity(
    json['code'],
    json['message'],
    Results.fromJson(json['results']),
  );
}

class Results extends Object {
  bool? isLock;

  bool? isLogin;

  bool? isMobileBind;

  bool? isVip;

  InnerItem? post;

  Chapter? chapter;

  Results(this.isLock, this.isLogin, this.isMobileBind, this.isVip, this.post,
      this.chapter);

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
      json['is_lock'],
      json['is_login'],
      json['is_mobile_bind'],
      json['is_vip'],
      InnerItem.fromJson(json['post']),
      Chapter.fromJson(json['chapter']));
}

class Chapter extends Object {
  int? index;

  String? id;

  int? count;

  int? sort;

  String? name;

  String? postId;

  String? prev;

  String? next;

  List<ChapterContent?>? contents;

  Chapter(
    this.index,
    this.id,
    this.count,
    this.sort,
    this.name,
    this.postId,
    this.prev,
    this.next,
    this.contents,
  );

  factory Chapter.fromJson(Map<String, dynamic> srcJson) =>
      _$ChapterFromJson(srcJson);
}

Chapter _$ChapterFromJson(Map<String, dynamic> json) {
  return Chapter(
    json['index'],
    json['id'],
    json['count'],
    json['sort'],
    json['name'],
    json['post_id'],
    json['prev'],
    json['next'],
    (json['contents'] as List).map((e) => ChapterContent.fromJson(e)).toList(),
  );
}
