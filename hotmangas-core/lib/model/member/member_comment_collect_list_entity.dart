import 'package:hotmangasCore/model/core/results_entity.dart'
    show ResultsEntity;

import 'member_comment_collect_list.dart' show MemberCommentCollectList;

class MemberCommentCollectListEntity extends ResultsEntity {
  MemberCommentCollectList? results;

  MemberCommentCollectListEntity(code, message, this.results)
      : super(code, message);

  factory MemberCommentCollectListEntity.fromJson(
          Map<String, dynamic> srcJson) =>
      _$MemberRewardEntityFromJson(srcJson);
}

MemberCommentCollectListEntity _$MemberRewardEntityFromJson(
    Map<String, dynamic> json) {
  return MemberCommentCollectListEntity(json['code'], json['message'],
      MemberCommentCollectList.fromJson(json['results']));
}
