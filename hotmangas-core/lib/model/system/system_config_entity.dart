class SystemConfigEntity extends Object {
  int? code;

  String? message;

  Results? results;

  SystemConfigEntity(
    this.code,
    this.message,
    this.results,
  );

  factory SystemConfigEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$SystemConfigEntityFromJson(srcJson);
}

class Results extends Object {
  bool? bActive;

  String? code;

  Results(
    this.bActive,
    this.code,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

SystemConfigEntity _$SystemConfigEntityFromJson(Map<String, dynamic> json) {
  return SystemConfigEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    json['b_active'],
    json['code'],
  );
}
