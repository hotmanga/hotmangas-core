// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show comic;
import 'package:hotmangasCore/api/v3/api.dart' show member;
import 'package:hotmangasCore/model/model.dart';

void main() {
  test('拷贝漫画下载接口 付费账号', () async {
    final userName = 'copy002';
    final passWord = '111111';
    ComicChapterContentSignEntity? response;
    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await comic.getChapterContentByDownload(
        comicPathWord: 'xiaolvlvyuaili',
        comicChapterId: '87c402ba-e5df-11e9-abbb-00163e0ca5bd',
        reject: () {},
        resolve: (ComicChapterContentSignEntity entity) {
          response = entity;
        });
    expect(response, isA<ComicChapterContentSignEntity>(), reason: '1');
    expect(response?.code, equals(200), reason: '2');
    expect(response?.results?.isVip, equals(false), reason: '3');
    expect(response?.results?.dayDownloads, equals(49), reason: '4');
    expect(response?.results?.dayDownloads, equals(123), reason: '5');
  });
  test('拷贝漫画下载接口 免费账号', () async {
    final userName = 'kbceshi002';
    final passWord = '111111';
    ComicChapterContentSignEntity? response;
    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await comic.getChapterContentByDownload(
        comicPathWord: 'xiaolvlvyuaili',
        comicChapterId: '87c402ba-e5df-11e9-abbb-00163e0ca5bd',
        reject: () {},
        resolve: (ComicChapterContentSignEntity entity) {
          response = entity;
        });
    expect(response, isA<ComicChapterContentSignEntity>(), reason: '1');
    expect(response?.code, equals(200), reason: '2');
    expect(response?.results?.isVip, equals(false), reason: '3');
    expect(response?.results?.dayDownloads, equals(49), reason: '4');
    expect(response?.results?.dayDownloads, equals(0), reason: '5');
  });
}
