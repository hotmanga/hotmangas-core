// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:hotmangasCore/model/search/search_cartoon_list_entity.dart';

void main() async {
  SearchCartoonListEntity? entity;
  await search.searchCartoon(
      queryParameters: {
        'limit': 1,
        'offset': 0,
        'q': 'Kidmo',
      },
      resolve: (SearchCartoonListEntity data) {
        entity = data;
      },
      reject: (error) {
        print(error);
      });

  test('漫画查询接口测试 接口基本信息', () {
    expect(entity!.code, equals(200));
    expect(entity!.results, isA<Results>());
    expect(entity!.results.limit, equals(1));
    expect(entity!.results.offset, equals(0));
    expect(entity!.results.list.first, isNotNull);
    expect(entity!.results.list.first.company, isA<AuthorOrCompany>());
  });
}
