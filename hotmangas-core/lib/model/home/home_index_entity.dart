import 'package:hotmangasCore/model/model.dart'
    show
        Banners,
        CommonListCartoon,
        CommonListComic,
        ResultsEntity,
        Topics,
        OpenOrders;

class HomeIndexEntity extends ResultsEntity {
  Results? results;

  HomeIndexEntity(code, message, this.results) : super(code, message);

  factory HomeIndexEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$HomeIndexEntityFromJson(srcJson);
}

HomeIndexEntity _$HomeIndexEntityFromJson(Map<String, dynamic> json) {
  return HomeIndexEntity(
    json['code'],
    json['message'],
    json['results'] == null
        ? null
        : Results.fromJson(json['results'] as Map<String, dynamic>),
  );
}

class Results extends Object {
  List<Banners?>? banners;

  Topics? topics;

  CommonListComic? recComics;

  CommonListComic? recCartoons;

  CommonListComic? rankWeeklyFreeComics;

  CommonListComic? rankWeeklyChargeComics;

  CommonListCartoon? rankWeeklyChargeCartoons;

  CommonListComic? updateWeeklyChargeComics;

  CommonListComic? updateWeeklyFreeComics;

  CommonListCartoon? updateWeeklyChargeCartoons;

  OpenOrders? openOrders;

  Results(
    this.banners,
    this.topics,
    this.recComics,
    this.recCartoons,
    this.rankWeeklyFreeComics,
    this.rankWeeklyChargeComics,
    this.rankWeeklyChargeCartoons,
    this.updateWeeklyChargeComics,
    this.updateWeeklyFreeComics,
    this.updateWeeklyChargeCartoons,
    this.openOrders,
  );

  factory Results.fromJson(Map<String, dynamic> srcJson) =>
      _$ResultsFromJson(srcJson);
}

Results _$ResultsFromJson(Map<String, dynamic> json) {
  return Results(
    (json['banners'] as List)
        .map((e) =>
            e == null ? null : Banners.fromJson(e as Map<String, dynamic>))
        .toList(),
    json['topics'] == null
        ? null
        : Topics.fromJson(json['topics'] as Map<String, dynamic>),
    json['recComics'] == null
        ? null
        : CommonListComic.fromJson(json['recComics'] as Map<String, dynamic>),
    json['recCartoons'] == null
        ? null
        : CommonListComic.fromJson(json['recCartoons'] as Map<String, dynamic>),
    json['rankWeeklyFreeComics'] == null
        ? null
        : CommonListComic.fromJson(
            json['rankWeeklyFreeComics'] as Map<String, dynamic>),
    json['rankWeeklyChargeComics'] == null
        ? null
        : CommonListComic.fromJson(
            json['rankWeeklyChargeComics'] as Map<String, dynamic>),
    json['rankWeeklyChargeCartoons'] == null
        ? null
        : CommonListCartoon.fromJson(
            json['rankWeeklyChargeCartoons'] as Map<String, dynamic>),
    json['updateWeeklyChargeComics'] == null
        ? null
        : CommonListComic.fromJson(
            json['updateWeeklyChargeComics'] as Map<String, dynamic>),
    json['updateWeeklyFreeComics'] == null
        ? null
        : CommonListComic.fromJson(
            json['updateWeeklyFreeComics'] as Map<String, dynamic>),
    json['updateWeeklyChargeCartoons'] == null
        ? null
        : CommonListCartoon.fromJson(
            json['updateWeeklyChargeCartoons'] as Map<String, dynamic>),
    json['openOrders'] == null
        ? null
        : OpenOrders.fromJson(json['openOrders'] as Map<String, dynamic>),
  );
}
