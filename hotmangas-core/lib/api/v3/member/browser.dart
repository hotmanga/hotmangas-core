import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        FreeType,
        FreeTypeEnumMap,
        MemberBookBrowserListEntity,
        MemberBrowserPortraitListEntity,
        MemberCartoonDeleteEntity,
        MemberCollectCartoonListEntity,
        MemberCollectComicListEntity,
        MemberComicDeleteEntity,
        ResultsEntity;

final Map<String, dynamic> _queryParameters = {
  'limit': 21,
  'offset': 0,
};

class Browser extends HttpCore {
  Future<void> getComicList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/browse/comics',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberCollectComicListEntity.fromJson(data));
          // resolve.call(data);
        },
        reject: reject);
  }

  Future<void> getCartoonList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/browse/cartoons',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberCollectCartoonListEntity.fromJson(data));
          // resolve(data);
        },
        reject: reject);
  }

  Future<void> deleteComics(
      {FreeType? freeType, Function? resolve, Function? reject}) async {
    await deleteCore(
        path: '/api/v3/member/browse/comics',
        formData: {
          'free_type': FreeTypeEnumMap[freeType],
        },
        queryParameters: null,
        resolve: (data) {
          resolve?.call(MemberComicDeleteEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> deleteCartoon(
      {FreeType? freeType, Function? resolve, Function? reject}) async {
    var _ = {
      'free_type': FreeTypeEnumMap[freeType],
    };
    await deleteCore(
        path: '/api/v3/member/browse/cartoons',
        formData: _,
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberCartoonDeleteEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取当前登录用户的轻小说浏览记录
  Future<void> getBooksList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/browse/books',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberBookBrowserListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> deleteBooks({Function? resolve, Function? reject}) async {
    await deleteCore(
        path: '/api/v3/member/browse/books',
        formData: {},
        queryParameters: {},
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 获取当前登录用户的写真浏览记录
  Future<void> getPortraitList(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/member/browse/posts',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(MemberBrowserPortraitListEntity.fromJson(data));
          // resolve?.call(data);
        },
        reject: reject);
  }

  /// 清空当前用户的写真浏览记录
  Future<void> deletePortrait({Function? resolve, Function? reject}) async {
    await deleteCore(
        path: '/api/v3/member/browse/posts',
        formData: {},
        queryParameters: {},
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 清空当前用户的写真浏览记录仅使用在google渠道
  Future<void> deletePortraitV2(
      {int? freeType, Function? resolve, Function? reject}) async {
    await deleteCore(
        path: '/api/v3/member/browse/posts',
        formData: {
          'free_type': freeType,
        },
        queryParameters: null,
        resolve: (data) {
          resolve?.call(ResultsEntity.fromJson(data));
        },
        reject: reject);
  }
}
