import 'package:hotmangasCore/model/core/results_entity.dart';

class UploadEntity extends ResultsEntity {
  String? results;

  UploadEntity(code, message, this.results) : super(code, message);

  factory UploadEntity.fromJson(Map<String, dynamic> fromJson) =>
      _$UploadEntityFromJson(fromJson);
}

UploadEntity _$UploadEntityFromJson(Map<String, dynamic> json) {
  return UploadEntity(
    json['code'],
    json['message'],
    json['results'],
  );
}
