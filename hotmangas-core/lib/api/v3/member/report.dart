import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart' show ReportConfigListEntity;

final Map<String, dynamic>? _postReportMap = {
  'comic_id': '', //漫画ID
  'comment_id': '', //评论ID
  'report_type': '', //举报类型
  'report_reason': '', //举报原因
};

final Map<String, dynamic>? _postRoastMap = {
  'chapter_id': '', //章节ID
  'roast_id': '', //吐槽ID
  'report_type': '', //举报类型
  'report_reason': '', //举报原因
};

final Map<String, dynamic>? _postNovelMap = {
  'book_id': '', //章节ID
  'comment_id': '', //吐槽ID
  'report_type': '', //举报类型
  'report_reason': '', //举报原因
};

class Report extends HttpCore {
  /// 评论举报列表配置信息
  Future<void> getReport({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/comment/config',
        resolve: (data) {
          resolve?.call(ReportConfigListEntity.fromJson(data));
        },
        reject: reject);
  }

  Future<void> getRoastReport({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/roast/config',
        resolve: (data) {
          resolve?.call(ReportConfigListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 轻小说评论举报列表配置信息
  Future<void> getNovelReport({Function? resolve, Function? reject}) async {
    await getCore(
        path: '/api/v3/bookcomment/config',
        resolve: (data) {
          resolve?.call(ReportConfigListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 用户轻小说评论举报
  Future<void> postNovelReport(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_postNovelMap!);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v3/member/bookcomment/report',
        formData: _,
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }

  /// 用户评论举报
  Future<void> postReport(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_postReportMap!);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v3/member/comment/report',
        formData: _,
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }

  ///用户吐槽举报
  Future<void> postRoastReport(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_postRoastMap!);
    _.addAll(queryParameters!);
    await postCore(
        path: '/api/v3/member/roast/report',
        formData: _,
        resolve: (data) {
          resolve?.call(data);
        },
        reject: reject);
  }
}
