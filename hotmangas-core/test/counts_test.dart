import 'package:hotmangasCore/api/v3/api.dart' show counts;
import 'package:hotmangasCore/model/model.dart'
    show CountListEntity, CountItem, FreeType;
import 'package:test/test.dart';

void main() {
  test('测试获取轻小说所有主题', () async {
    CountListEntity? response;
    await counts.book.getThemeList(
        queryParameters: {},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response, isA<CountListEntity>());
    expect(response?.code, equals(200));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.length, greaterThan(0));
    expect(response?.results?.list?.first, isA<CountItem>());
  });

  test('Counts Comic getThemeList', () async {
    CountListEntity? response;
    await counts.comic.getThemeList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.first, isA<CountItem>());
  });

  test('Counts Comic getParodyList', () async {
    CountListEntity? response;
    await counts.comic.getParodyList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.first, isA<CountItem>());
  });

  test('Counts Comic getAuthorList', () async {
    CountListEntity? response;
    await counts.comic.getAuthorList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.first, isA<CountItem>());
  });

  test('Counts Comic getFemaleList', () async {
    CountListEntity? response;
    await counts.comic.getFemaleList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.first, isA<CountItem>());
  });

  test('Counts Comic getMaleList', () async {
    CountListEntity? response;
    await counts.comic.getMaleList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.first, isA<CountItem>());
  });

  test('Counts Cartoon getCompanyList', () async {
    CountListEntity? response;
    await counts.cartoon.getCompanyList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.first, isA<CountItem>());
  });

  test('Counts Cartoon getFemaleList', () async {
    CountListEntity? response;
    await counts.cartoon.getFemaleList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.first, isA<CountItem>());
  });

  test('Counts Cartoon getMaleList', () async {
    CountListEntity? response;
    await counts.cartoon.getMaleList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.first, isA<CountItem>());
  });

  test('Counts Cartoon getThemeList', () async {
    CountListEntity? response;
    await counts.cartoon.getThemeList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list, isEmpty);
  });

  test('Counts Cartoon getYearList', () async {
    CountListEntity? response;
    await counts.cartoon.getYearList(
        freeType: FreeType.Free,
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isNotNull);
    expect(response?.code, equals(200));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isList);
    expect(response?.results?.list?.first, isA<CountItem>());
  });
}
