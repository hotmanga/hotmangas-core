/// 仅支持格式化 DateTime 为 yyyy-MM-dd 格式.
String? datetimeFormat(DateTime? datetime) {
  if (datetime == null) return '-';
  final d = datetime;
  return "${d.year}-${d.month.toString().padLeft(2, '0')}-${d.day.toString().padLeft(2, '0')}";
}
