// Package imports:
import 'package:test/test.dart';

// Project imports:
import 'package:hotmangasCore/api/v3/api.dart' show comic;
import 'package:hotmangasCore/model/model.dart';

void main() {
  test('拷贝漫画下载接口', () async {
    ComicChapterContentSignEntity? response;
    await comic.getChapterContentBySign(
        comicPathWord: 'xiaolvlvyuaili',
        comicChapterId: '87c402ba-e5df-11e9-abbb-00163e0ca5bd',
        reject: () {},
        resolve: (ComicChapterContentSignEntity entity) {
          response = entity;
        });
    expect(response, isA<ComicChapterContentSignEntity>(), reason: '1');
    expect(response?.code, equals(200), reason: '2');
    expect(response?.results?.chapter?.words, isA<List<int>>(), reason: '3');
    expect(response?.results?.isLock, equals(false), reason: '4');
    expect(response?.results?.isLogin, equals(false), reason: '5');
    expect(response?.results?.isMobileBind, equals(false), reason: '6');
    expect(response?.results?.isVip, equals(false), reason: '7');
  });
}
