import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

void main() {
  test('测试获取当前用户书架上的轻小说收藏', () async {
    final userName = 'hotman1234567';
    final passWord = 'hotman1234567';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    MemberBookCollectListEntity? response;
    await member.collect.getBookList(
        queryParameters: {'limit': 1, 'offset': 0},
        resolve: (data) {
          response = data;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<MemberBookCollectListEntity>());
    expect(response?.code, equals(200));
    // expect(response?.results?.list?.length, equals(0));
    expect(response?.results?.limit, equals(1));
    expect(response?.results?.offset, equals(0));
    expect(response?.results?.list, isEmpty);
  });

  test('测试用户收藏某本轻小说', () async {
    final userName = 'hotman1234567';
    final passWord = 'hotman1234567';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ResultsEntity? response;
    await member.collect.collectBookById(
        uid: 'be75b54e-58e9-11ed-94c0-024352452ce0',
        collect: true,
        resolve: (entity) {
          response = entity;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<ResultsEntity>());
    expect(response?.code, equals(200));
  });

  test('测试用户取消收藏某本轻小说', () async {
    final userName = 'hotman1234567';
    final passWord = 'hotman1234567';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ResultsEntity? response;
    await member.collect.collectBookById(
        uid: 'be75b54e-58e9-11ed-94c0-024352452ce0',
        collect: false,
        resolve: (entity) {
          response = entity;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<ResultsEntity>());
    expect(response?.code, equals(200));
  });

  test('测试用户删除书架上的轻小说收藏', () async {
    final userName = 'hotman1234567';
    final passWord = 'hotman1234567';

    await member.login.login(
      userName: userName,
      passWord: passWord,
    );

    ResultsEntity? response;
    await member.collect.delBookList(
        queryParameters: {
          'ids': [1],
        },
        resolve: (entity) {
          response = entity;
        },
        reject: (error) {
          print(error);
        });

    expect(response, isA<ResultsEntity>());
    expect(response?.code, equals(200));
  });
}
