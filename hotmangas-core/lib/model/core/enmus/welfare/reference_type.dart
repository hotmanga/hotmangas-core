enum WelfareReferenceTypeEnum { Error, Goods }

const WelfareReferenceTypeEnumMap = {
  WelfareReferenceTypeEnum.Error: 0,
  WelfareReferenceTypeEnum.Goods: 1
};
