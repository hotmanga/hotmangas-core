import 'package:hotmangasCore/model/core/results_entity.dart';

class TopicsDetailEntity extends ResultsEntity {
  TopicsDetailItem? results;

  TopicsDetailEntity(code, message, this.results) : super(code, message);

  factory TopicsDetailEntity.fromJson(Map<String, dynamic> srcJson) =>
      _$TopicsDetailEntityFromJson(srcJson);
}

TopicsDetailEntity _$TopicsDetailEntityFromJson(Map<String, dynamic> json) {
  return TopicsDetailEntity(json['code'], json['message'],
      TopicsDetailItem.fromJson(json['results']));
}

class TopicsDetailItem extends Object {
  String? title;

  String? journal;

  String? cover;

  String? period;

  String? brief;

  int? type;

  String? pathWord;

  String? dateTimeCreated;

  ///优惠后的价格 仅写真专题使用
  String? ticketPay;

  ///总价格
  String? ticketTotal;

  ///打几折
  String get discount {
    try {
      var d = (int.parse(ticketPay!) / int.parse(ticketTotal!))
          .toString()
          .split('.')[1];
      if (d.length == 1) {
        return d;
      } else {
        return d[0] + '.' + d[1];
      }
    } catch (e) {
      return 'null';
    }
  }

  ///是否购买过该专题
  bool? isBuy;

  ///是否登录
  bool? isLogin;

  TopicsDetailItem(
    this.title,
    this.journal,
    this.cover,
    this.period,
    this.brief,
    this.type,
    this.pathWord,
    this.dateTimeCreated,
    this.ticketPay,
    this.ticketTotal,
    this.isBuy,
    this.isLogin,
  );

  factory TopicsDetailItem.fromJson(Map<String, dynamic> srcJson) =>
      _$TopicsDetailFromJson(srcJson);
}

TopicsDetailItem _$TopicsDetailFromJson(Map<String, dynamic> json) {
  return TopicsDetailItem(
    json['title'],
    json['journal'],
    json['cover'],
    json['period'],
    json['brief'],
    json['type'],
    json['path_word'],
    json['datetime_created'],
    json['ticket_pay'] ?? '0',
    json['ticket_total'] ?? '0',
    json['is_buy'],
    json['is_login'],
  );
}
