// Package imports:
// Project imports:
import 'dart:math';

import 'package:hotmangasCore/api/v3/api.dart';
import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart';
import 'package:test/test.dart';

// class ABCRatio {
//   static const int aRatio = 5;
//   static const int bRatio = 3;
//   static const int cRatio = 2;

//   static String getABC() {
//     final random = Random();
//     final num = random.nextInt(10) + 1; // 生成1~10的随机数
//     if (num <= aRatio) {
//       return 'a';
//     } else if (num <= aRatio + bRatio) {
//       return 'b';
//     } else {
//       return 'c';
//     }
//   }
// }
// class ABCRatio {
//   static const int aRatio = 5;
//   static const int bRatio = 3;
//   static const int cRatio = 2;
//   static const int totalRatio = aRatio + bRatio + cRatio;

//   static String getABC() {
//     final Random random = Random();
//     final int num = random.nextInt(totalRatio);
//     if (num < aRatio) {
//       return 'a';
//     } else if (num < aRatio + bRatio) {
//       return 'b';
//     } else {
//       return 'c';
//     }
//   }
// }

void main() {
  test('测试用户登录 接口基本信息', () async {
    final userName = 'kbceshi004';
    final passWord = '111111';
    MemberInfoEntity? entity;
    await member.login.login(
        userName: userName,
        passWord: passWord,
        resolve: (data) {
          entity = data;
        },
        reject: (error) {
          print(error);
        });

    expect(entity?.code, equals(200));
    expect(entity?.results, isA<MemberInfo>());
    expect(entity?.results?.invited, isEmpty);
    expect(entity?.results?.inviteCode, isEmpty);
  });

  test('测试用户登录后的接口基本信息', () async {
    final userName = 'copy004';
    final passWord = '111111';
    await member.login.login(
      userName: userName,
      passWord: passWord,
    );
    await HttpCore().getCore(
        path: '/api/v3/games',
        resolve: (data) {
          // print(data);
        },
        reject: () {});
  });

  test('测试用户未登录的接口基本信息', () async {
    await HttpCore().getCore(
        path: '/api/v3/games',
        resolve: (data) {
          // print(data);
        },
        reject: () {});
  });

  test('测试532', () {
    List<int> generateRatioNumbers(
        int count, int ratioA, int ratioB, int ratioC) {
      assert(ratioA + ratioB + ratioC == 10, 'The ratios must add up to 10.');

      final numbers = <int>[];
      final random = Random();
      // ignore: omit_local_variable_types
      int countA = 0, countB = 0, countC = 0;

      while (numbers.length < count) {
        final rand = random.nextInt(10);

        if (rand < ratioA && countA < (count * ratioA / 10).floor()) {
          numbers.add(5);
          countA++;
        } else if (rand < ratioA + ratioB &&
            countB < (count * ratioB / 10).floor()) {
          numbers.add(3);
          countB++;
        } else if (countC < (count * ratioC / 10).floor()) {
          numbers.add(2);
          countC++;
        }
      }
      return numbers;
    }

    final numbers = generateRatioNumbers(18, 5, 3, 2);
    final countA = numbers.where((n) => n == 5).length;
    final countB = numbers.where((n) => n == 3).length;
    final countC = numbers.where((n) => n == 2).length;

    print('Generated numbers: $numbers');
    print('Count of A: $countA');
    print('Count of B: $countB');
    print('Count of C: $countC');
  });
}