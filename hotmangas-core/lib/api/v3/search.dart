import 'package:hotmangasCore/hot/dio_warpper.dart' show HttpCore;
import 'package:hotmangasCore/model/model.dart'
    show
        SearchCartoonListEntity,
        SearchComicListEntity,
        SearchHotKeyListEntity,
        SearchBookListEntity;

final Map<String, dynamic> _queryParameters = {
  'limit': 21,
  'offset': 0,
  'q_type': '',
  'q': '',
};

class Search extends HttpCore {
  Future<void> searchHotKey(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/search/key',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(SearchHotKeyListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 搜索漫画, 支持类型查询, q_type=[""|"name"|"author"|"local"]
  /// - "", 空(默认), 即搜索漫画中所有符合的内容.
  /// - "name", 指代 q 参数为漫画名称.
  /// - "author", 指代 q 参数为作者.
  /// - "local", 指代 q 参数为汉化组.
  Future<void> searchComic(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);

    await getCore(
        path: '/api/v3/search/comic',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(SearchComicListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 搜索动画, 支持类型查询, q_type=[""|"name"|"company"]
  /// - "", 空(默认), 即搜索动画中所有符合的内容.
  /// - "name", 指代 q 参数为动画名称.
  /// - "company", 指代 q 参数为动画的公司.
  Future<void> searchCartoon(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/search/cartoon',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(SearchCartoonListEntity.fromJson(data));
        },
        reject: reject);
  }

  /// 搜索小说, 支持类型查询, q_type=[''|'name'|'author']
  /// - "", 空(默认), 即搜索轻小说中所有符合的内容.
  /// - "name", 指代 q 参数为轻小说名称.
  /// - "author", 指代 q 参数为轻小说作者.
  Future<void> searchBook(
      {Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    var _ = Map<String, dynamic>.from(_queryParameters);
    _.addAll(queryParameters!);
    await getCore(
        path: '/api/v3/search/books',
        queryParameters: _,
        resolve: (data) {
          resolve?.call(SearchBookListEntity.fromJson(data));
        },
        reject: reject);
  }
}

final search = Search();
