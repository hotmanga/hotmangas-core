class MemberCommentCollectLists extends Object {
  ///  留言ID
  int? id;

  /// 留言时间
  String? createAt;

  /// 用户ID
  String? userId;

  /// 用户名称
  String? userName;

  /// 用户头像地址
  String? userAvatar;

  /// 留言内容
  String? comment;

  /// 顺序
  int? count;

  // 回复对象的ID
  int? parentId;

  // 回复对象的USERID
  String? parentUserId;

  // 回复对象的名字
  String? parentUserName;

  Map<String, dynamic> toJson() {
    return <String, dynamic>{
      'id': id,
      'createAt': createAt,
      'userId': userId,
      'userName': userName,
      'userAvatar': userAvatar,
      'comment': comment,
      'count': count,
      'parentId': parentId,
      'parentUserId': parentUserId,
      'parentUserName': parentUserName,
    };
  }

  MemberCommentCollectLists(
    this.id,
    this.createAt,
    this.userId,
    this.userName,
    this.userAvatar,
    this.comment,
    this.count,
    this.parentId,
    this.parentUserId,
    this.parentUserName,
  );

  factory MemberCommentCollectLists.fromJson(Map<String, dynamic> srcJson) =>
      _$MemberCommentCollectListFromJson(srcJson);
}

MemberCommentCollectLists _$MemberCommentCollectListFromJson(
    Map<String, dynamic> json) {
  return MemberCommentCollectLists(
    json['id'],
    json['create_at'] ?? '-',
    json['user_id'],
    json['user_name'],
    json['user_avatar'],
    json['comment'],
    json['count'],
    json['parent_id'] ?? 0,
    json['parent_user_id'] ?? '',
    json['parent_user_name'] ?? '',
  );
}
