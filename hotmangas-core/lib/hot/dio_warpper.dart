import 'package:dio/dio.dart' show FormData;
import 'package:meta/meta.dart' show required;

import './dio_simple_factory.dart' as dio_simple_factory;

class HttpCore {
  final http = dio_simple_factory.httpServer;

  /// example:
  /// 1，APP 启动时，需要 merge 的信息有
  ///  *， verison
  ///  *， deviceInfo
  ///  *， utmSource
  /// 2，登录成功时，merge Token。
  /// 3，退出时，也需要merge Token，设置为空。
  void mergeDioOptionExtra(Map<String, dynamic> extra) {
    http.mergeDioOptionExtra(extra);
  }

  Future<void> uploadCore(
      {@required String? path,
      FormData? formData,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    await http.uploadPost(
        path: path!,
        formData: formData!,
        queryParameters: queryParameters,
        resolve: resolve!,
        reject: reject!);
  }

  Future<void> postCore(
      {@required String? path,
      Map<String, dynamic>? formData,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    await http.httpPost(
        path: path!,
        formData: formData,
        queryParameters: queryParameters,
        resolve: resolve!,
        reject: reject);
  }

  Future<void> getCore(
      {@required String? path,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    await http.httpGet(
        path: path!,
        queryParameters: queryParameters,
        resolve: resolve!,
        reject: reject);
  }

  Future<void> deleteCore(
      {@required String? path,
      Map<String, dynamic>? formData,
      Map<String, dynamic>? queryParameters,
      Function? resolve,
      Function? reject}) async {
    await http.httpDelete(
        path: path!,
        formData: formData!,
        queryParameters: queryParameters,
        resolve: resolve!,
        reject: reject);
  }
}
